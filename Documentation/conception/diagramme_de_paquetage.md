```plantuml

@startuml

package CoreLibrary <<Rectangle>> {
  package Core <<Rectangle>> {
    class Code
    class Plateau
    struct Jeton
    enum Couleur
    enum Indicateur
  }
  
  package Joueurs <<Rectangle>> {
    class Joueur
  }
  
  
  package Regles <<Rectangle>> {
    interface IRegles
    class ReglesClassiques
  }
  
  package Evenements <<Rectangle>> {
    class AjouterCodeEventArgs
    class AjouterJetonEventArgs
    class AjouterJoueurEventArgs
    class DebutPartieEventArgs
    class DemanderJetonEventArgs
    class DemanderJoueurEventArgs
    class NouveauTourEventArgs
    class PartieTermineeEventArgs
    class PasserMainEventArgs
  }
  
  package Exceptions <<Rectangle>> {
    class CodeCompletException
    class CodeIncompletException
    class CodeInvalideException
    class CodeVideException
    class GrilleCompleteException
    class IndiceCodeException
    class PartieNonCommenceeException
    class TailleCodeException
    class TailleGrilleException
  }
  
  class Partie
}

package ConsoleApp <<Rectangle>> {
  class Program
  class Utils
}

package System <<Rectangle>> {
  class Exception
  class EventArgs
  class Random
}

@enduml

```