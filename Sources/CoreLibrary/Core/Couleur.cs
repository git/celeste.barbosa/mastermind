﻿namespace CoreLibrary.Core
{
    /// <summary>
    /// Enumération des différentes couleurs possibles pour un jeton.
    /// </summary>
    public enum Couleur
    {
        Rouge,
        Vert,
        Bleu,
        Jaune,
        Noir,
        Blanc
    }
}
