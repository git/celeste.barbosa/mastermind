﻿using System.Runtime.Serialization;

namespace CoreLibrary.Core
{
    /// <summary>
    /// Structure représentant un jeton de couleur.
    /// </summary>
    [DataContract]
    public readonly struct Jeton
    {
        /// <summary>
        /// Obtient la couleur du jeton.
        /// </summary>
        [DataMember]
        public Couleur Couleur { get; private init; }

        /// <summary>
        /// Constructeur d'un jeton avec une couleur spécifiée.
        /// </summary>
        /// <param name="couleur">La couleur du jeton.</param>
        public Jeton(Couleur couleur)
        {
            Couleur = couleur;
        }

        /// <summary>
        /// Détermine si l'objet spécifié est égal à l'objet actuel.
        /// </summary>
        /// <param name="obj">L'objet à comparer avec l'objet actuel.</param>
        /// <returns>Renvoie true si l'objet spécifié est égal à l'objet actuel. Sinon, false.</returns>
        public override readonly bool Equals(object? obj)
        {
            if (obj is not Jeton)
                return false;

            return Couleur == ((Jeton)obj).Couleur;
        }

        /// <summary>
        /// Détermine si deux jetons sont égaux.
        /// </summary>
        /// <param name="gauche">Le premier jeton à comparer.</param>
        /// <param name="droite">Le deuxième jeton à comparer.</param>
        /// <returns>Renvoie true si les deux jetons sont égaux. Sinon, false.</returns>
        public static bool operator ==(Jeton gauche, Jeton droite) => gauche.Equals(droite);

        /// <summary>
        /// Détermine si deux jetons ne sont pas égaux.
        /// </summary>
        /// <param name="gauche">Le premier jeton à comparer.</param>
        /// <param name="droite">Le deuxième jeton à comparer.</param>
        /// <returns>Renvoie true si les deux jetons ne sont pas égaux. Sinon, false.</returns>
        public static bool operator !=(Jeton gauche, Jeton droite) => !gauche.Equals(droite);

        /// <summary>
        /// Fonction de hachage.
        /// </summary>
        /// <returns>Renvoie un code de hachage pour une couleur.</returns>
        public override readonly int GetHashCode() => Couleur.GetHashCode();
    }
}
