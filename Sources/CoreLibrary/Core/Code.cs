﻿using CoreLibrary.Exceptions;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;

namespace CoreLibrary.Core
{
    /// <summary>
    /// Classe représentant un code composé de jetons et ses différentes méthodes.
    /// </summary>
    [DataContract]
    public class Code
    {
        /// <summary>
        /// Collection observable de jetons.
        /// </summary>
        [DataMember]
        public ObservableCollection<Jeton> Jetons { get; private init; } = new ObservableCollection<Jeton>();

        /// <summary>
        /// Obtient le nombre actuel de jetons dans le code.
        /// </summary>
        public int Taille => Jetons.Count;

        /// <summary>
        /// Taille maximale de jetons autorisée pour le code.
        /// </summary>
        [DataMember]
        public int TailleMax { get; private init; }

        /// <summary>
        /// Indique si le code est complet.
        /// </summary>
        public bool Complet => Taille == TailleMax;

        /// <summary>
        /// Indique si le code est vide.
        /// </summary>
        public bool Vide => Taille == 0;

        /// <summary>
        /// Constructeur de code avec une taille maximale spécifiée.
        /// </summary>
        /// <param name="taille">La taille maximale de jetons autorisée pour le code.</param>
        /// <exception cref="TailleCodeException">Exception lancée si la taille spécifiée est négative ou nulle.</exception>
        public Code(int taille)
        {
            if (taille < 0)
                throw new TailleCodeException(taille);

            TailleMax = taille;
        }

        /// <summary>
        /// Ajoute un jeton au code.
        /// </summary>
        /// <param name="jeton">Le jeton à ajouter.</param>
        /// <exception cref="CodeCompletException">Lancée si le code est déjà complet.</exception>
        public void AjouterJeton(Jeton jeton)
        {
            if (Complet)
                throw new CodeCompletException();

            Jetons.Add(jeton);
        }

        /// <summary>
        /// Récupère le jeton à l'indice spécifié.
        /// </summary>
        /// <param name="indice">L'indice du jeton à récupérer.</param>
        /// <returns>Renvoie le jeton à l'indice spécifié.</returns>
        /// <exception cref="IndiceCodeException">Lancée si l'indice est en dehors des limites de la collection de jetons.</exception>
        public Jeton RecupererJeton(int indice)
        {
            if (indice < 0 || indice >= Taille)
                throw new IndiceCodeException(indice, Taille - 1);

            return Jetons.ElementAt(indice);
        }

        /// <summary>
        /// Supprime le dernier jeton du code.
        /// </summary>
        /// <exception cref="CodeVideException">Lancée si le code est vide.</exception>
        public void SupprimerDernierJeton()
        {
            if (Vide)
                throw new CodeVideException();

            Jetons.RemoveAt(Taille - 1);
        }

        /// <summary>
        /// Compare le code avec le code secret et retourne une liste d'indicateurs.
        /// </summary>
        /// <param name="code">Le code à comparer.</param>
        /// <returns>Renvoie une liste d'indicateurs représentant les résultats de la comparaison.</returns>
        /// <exception cref="CodeIncompletException">Lancée si le code n'est pas complet.</exception>
        /// <exception cref="CodeInvalideException">Lancée si le code est invalide.</exception>
        public IReadOnlyList<Indicateur> Comparer(Code code)
        {
            // Je suis le bon code

            List<Indicateur> indicateurs = new List<Indicateur>();

            if (!code.Complet)
                throw new CodeIncompletException();

            if (code.TailleMax != TailleMax)
                throw new CodeInvalideException();

            List<Jeton?> mesJetons = Jetons.Select(jeton => (Jeton?)jeton).ToList();
            List<Jeton?> sesJetons = code.Jetons.Select(jeton => (Jeton?)jeton).ToList();

            for (int i = 0; i < mesJetons.Count; ++i)
            {
                if (mesJetons[i]!.Value == sesJetons[i]!.Value)
                {
                    mesJetons[i] = null;
                    sesJetons[i] = null;
                    indicateurs.Add(Indicateur.BonnePlace);
                }
            }

            for (int i = 0; i < sesJetons.Count; ++i)
            {
                if (sesJetons[i].HasValue && mesJetons.Contains(sesJetons[i]))
                {
                    mesJetons[mesJetons.IndexOf(sesJetons[i])] = null;
                    sesJetons[i] = null;
                    indicateurs.Add(Indicateur.BonneCouleur);
                }
            }

            return indicateurs;
        }

        /// <summary>
        /// Retourne la taille du code sous forme de chaîne de caractères.
        /// </summary>
        /// <returns>Renvoie une chaîne de caractères représentant la taille du code.</returns>
        public override string ToString() => $"Code({Taille})";
    }
}
