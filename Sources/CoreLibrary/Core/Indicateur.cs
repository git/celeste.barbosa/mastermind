﻿namespace CoreLibrary.Core
{
    /// <summary>
    /// Enumération des indicateurs de comparaison entre deux codes.
    /// </summary>
    public enum Indicateur
    {
        BonnePlace,
        BonneCouleur
    }
}
