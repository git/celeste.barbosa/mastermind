﻿using System.Runtime.Serialization;

namespace CoreLibrary.Regles
{
    /// <summary>
    /// Classe définissant les règles difficiles du jeu.
    /// </summary>

    [DataContract]
    public class ReglesDifficiles : IRegles
    {
        /// <summary>
        /// Obtient l'indice des règles difficiles.
        /// </summary>
        public int Indice => 2;

        /// <summary>
        /// Obtient le nom des règles difficiles.
        /// </summary>
        public string Nom => "Règles difficiles";

        /// <summary>
        /// Obtient la description des règles difficiles.
        /// </summary>
        public string Description => "Le but du jeu est de découvrir la combinaison. On génère aléatoirement deux combinaisons de 6 couleurs (six couleurs au total : jaune, bleu, rouge, vert, blanc et noir), une combinaison pour chaque joueur. Deux joueurs se battent pour trouver la combinaison en premier, il y a douze tours. Le premier joueur à trouver la combinaison à gagner, chaque joueur a le même nombre de coups à réaliser. Donc si le joueur un à trouvé la solution au bout de huit coups, le joueur deux doit finir son huitième coup. Si le joueur deux trouve la combinaison, les deux joueurs sont à égalité. Sinon, le joueur un gagne. Pour trouver la combinaison, les joueurs disposent de quatre indicateurs. Ces indicateurs sont quatre ronds qui représentent les 6 couleurs sélectionnées par le joueur. Un rond noir signifie qu’une couleur est à la bonne place, un rond blanc correspond à une mauvaise place et s'il n’y a pas d’indicateur aucune des couleurs n’est présentent dans la combinaison.";

        /// <summary>
        /// Obtient le nombre de joueurs autorisé pour les règles difficiles.
        /// </summary>
        public int NbJoueurs => 2;

        /// <summary>
        /// Obtient le nombre de tours maximum pour les règles difficiles.
        /// </summary>
        public int NbTour => 12;

        /// <summary>
        /// Obtient la taille maximale du code pour les règles difficiles.
        /// </summary>
        public int TailleCode => 6;

        /// <summary>
        /// Détermine si les règles difficiles sont égales à un objet spécifié.
        /// </summary>
        /// <param name="obj">L'objet à comparer avec les règles difficiles.</param>
        /// <returns>True si les règles difficiles sont égales à l'objet spécifié, sinon false.</returns>
        public override bool Equals(object? obj) => obj is ReglesDifficiles;

        /// <summary>
        /// Obtient le code de hachage pour les règles difficiles.
        /// </summary>
        /// <returns>Le code de hachage des règles difficiles.</returns>
        public override int GetHashCode() => Nom.GetHashCode();
    }
}
