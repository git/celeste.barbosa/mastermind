﻿namespace CoreLibrary.Regles
{
    /// <summary>
    /// Interface pour créer des règles.
    /// </summary>
    public interface IRegles
    {
        /// <summary>
        /// Obtient l'indice des règles
        /// </summary>
        int Indice { get; }
        /// <summary>
        /// Obtient le nom des règles.
        /// </summary>
        string Nom { get; }
        /// <summary>
        /// Obtient la description des règles.
        /// </summary>
        string Description { get; }

        /// <summary>
        /// Obtient le nombre de joueurs.
        /// </summary>
        int NbJoueurs { get; }

        /// <summary>
        /// Obtient le nombre de tour.
        /// </summary>
        int NbTour { get; }

        /// <summary>
        /// Obtient la taille d'un code.
        /// </summary>
        int TailleCode { get; }

        /// Détermine si les règles sont égales à un objet spécifié.
        /// </summary>
        /// <param name="obj">L'objet à comparer avec les règles.</param>
        /// <returns>True si les règles sont égales à l'objet spécifié, sinon false.</returns>
        bool Equals(object? obj);

        /// <summary>
        /// Obtient le code de hachage pour les règles.
        /// </summary>
        /// <returns>Code de hachage des règles.</returns>
        int GetHashCode();
    }
}
