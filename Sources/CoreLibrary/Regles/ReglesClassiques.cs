﻿using System.Runtime.Serialization;

namespace CoreLibrary.Regles
{
    /// <summary>
    /// Classe pour les règles classiques, implémente IRegles.
    /// </summary>
    [DataContract]
    public class ReglesClassiques : IRegles
    {
        /// <summary>
        /// Obtient l'indice des règles classiques.
        /// </summary>
        public int Indice => 1;

        /// <summary>
        /// Chaîne de caractères pour le nom des règles classiques.
        /// </summary>
        public string Nom => "Règles classiques";

        /// <summary>
        /// Obtient la description des règles classiques.
        /// </summary>
        public string Description => "Le but du jeu est de découvrir la combinaison. On génère aléatoirement deux combinaisons de 4 couleurs (six couleurs au total : jaune, bleu, rouge, vert, blanc et noir), une combinaison pour chaque joueur. Deux joueurs se battent pour trouver la combinaison en premier, il y a douze tours. Le premier joueur à trouver la combinaison à gagner, chaque joueur a le même nombre de coups à réaliser. Donc si le joueur un à trouvé la solution au bout de huit coups, le joueur deux doit finir son huitième coup. Si le joueur deux trouve la combinaison, les deux joueurs sont à égalité. Sinon, le joueur un gagne. Pour trouver la combinaison, les joueurs disposent de quatre indicateurs. Ces indicateurs sont quatre ronds qui représentent les quatre couleurs sélectionnées par le joueur. Un rond noir signifie qu’une couleur est à la bonne place, un rond blanc correspond à une mauvaise place et s'il n’y a pas d’indicateur aucune des couleurs n’est présentent dans la combinaison.";

        /// <summary>
        /// Nombre de joueurs autorisé pour les règles classiques.
        /// </summary>
        public int NbJoueurs => 2;

        /// <summary>
        /// Nombre de tour maximum pour des règles classiques.
        /// </summary>
        public int NbTour => 12;

        /// <summary>
        /// Taille du code maximale pour des règles classiques.
        /// </summary>
        public int TailleCode => 4;

        /// <summary>
        /// Détermine si les règles classiques sont égales à un objet spécifié.
        /// </summary>
        /// <param name="obj">L'objet à comparer avec les règles classiques.</param>
        /// <returns>True si les règles classiques sont égales à l'objet spécifié, sinon false.</returns>
        public override bool Equals(object? obj) => obj is ReglesClassiques;

        /// <summary>
        /// Obtient le code de hachage pour les règles classiques.
        /// </summary>
        /// <returns>Le code de hachage des règles classiques.</returns>
        public override int GetHashCode() => Nom.GetHashCode();
    }
}
