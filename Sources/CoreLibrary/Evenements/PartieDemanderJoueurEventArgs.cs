﻿using CoreLibrary.Joueurs;

namespace CoreLibrary.Evenements
{
    /// <summary>
    /// Classe contenant les arguments passés en paramètres lors de l'événement PartieDemanderJoueur.
    /// </summary>
    /// <param name="">.</param>
    public class PartieDemanderJoueurEventArgs : EventArgs
    {
        /// <summary>
        /// Entier correspondant à l'indice du joueur.
        /// </summary>
        public int Indice { get; private init; }

        /// <summary>
        /// Le joueur qui est demandé.
        /// </summary>
        public Joueur JoueurDemande { get; private init; }

        /// <summary>
        /// Constructeur de PartieDemanderJoueurEventArgs, avec l'indice du joueur et le joueur demandé spécifiés.
        /// </summary>
        /// <param name="indice">Indice du joueur demandé.</param>
        /// <param name="joueurDemande">Le joueur demandé.</param>
        public PartieDemanderJoueurEventArgs(int indice, Joueur joueurDemande) 
        {
            Indice = indice;
            JoueurDemande = joueurDemande;
        }
    }
}
