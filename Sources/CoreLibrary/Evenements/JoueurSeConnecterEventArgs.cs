﻿using CoreLibrary.Joueurs;

namespace CoreLibrary.Evenements
{
    /// <summary>
    /// Classe contenant les arguments passés en paramètres lors de l'événement JoueurSeConnecter.
    /// </summary>
    public class JoueurSeConnecterEventArgs : EventArgs
    {
        /// <summary>
        /// Obtient le joueur qui s'est connecté.
        /// </summary>
        public Joueur Joueur { get; private init; }

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="JoueurSeConnecterEventArgs"/>.
        /// </summary>
        /// <param name="joueur">Le joueur qui s'est connecté.</param>
        public JoueurSeConnecterEventArgs(Joueur joueur)
        {
            Joueur = joueur;
        }
    }
}
