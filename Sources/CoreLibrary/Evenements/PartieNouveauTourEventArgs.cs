﻿using CoreLibrary.Core;

namespace CoreLibrary.Evenements
{
    /// <summary>
    /// Classe contenant les arguments passés en paramètres lors de l'événement PartieNouveauTour.
    /// </summary>
    public class PartieNouveauTourEventArgs : EventArgs
    {
        /// <summary>
        /// Obtient le numéro du tour.
        /// </summary>
        public int Tour { get; private init; }

        /// <summary>
        /// Obtient le nom du joueur.
        /// </summary>
        public string Nom { get; private init; }

        /// <summary>
        /// Obtient le plateau actuel.
        /// </summary>
        public Plateau Plateau { get; private init; }

        /// <summary>
        /// Obtient le code en cours.
        /// </summary>
        public Code Code { get; private init; }

        /// <summary>
        /// Indique si le joueur est un joueur humain.
        /// </summary>
        public bool EstJoueur { get; private init; }

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="PartieNouveauTourEventArgs"/>.
        /// </summary>
        /// <param name="tour">Le numéro du tour.</param>
        /// <param name="nom">Le nom du joueur.</param>
        /// <param name="plateau">Le plateau actuel.</param>
        /// <param name="code">Le code en cours.</param>
        /// <param name="estJoueur">Indique si le joueur est un joueur humain.</param>
        public PartieNouveauTourEventArgs(int tour, string nom, Plateau plateau, Code code, bool estJoueur)
        {
            Tour = tour;
            Nom = nom;
            Plateau = plateau;
            Code = code;
            EstJoueur = estJoueur;
        }
    }
}
