﻿namespace CoreLibrary.Evenements
{
    /// <summary>
    /// Classe contenant les arguments passés en paramètre, lors de l'événement PartiePasserLaMain.
    /// </summary>
    public class PartiePasserLaMainEventArgs
    {
        /// <summary>
        /// Chaîne de caractère pour le joueur courant.
        /// </summary>
        public string Joueur { get; private init; }

        /// <summary>
        /// Constructeur de PartiePasserLaMainEventArgs, avec le joueur spécifié.
        /// </summary>
        /// <param name="joueur">Chaîne de caractères représentant le joueur courant que nous souhaitons obtenir.</param>
        public PartiePasserLaMainEventArgs(string joueur)
        {
            Joueur = joueur;
        }

    }
}
