﻿namespace CoreLibrary.Evenements
{
    /// <summary>
    /// Classe contenant les arguments passés en paramètre lors de l'événement PartiePartieTerminee.
    /// </summary>
    public class PartiePartieTermineeEventArgs : EventArgs
    {
        /// <summary>
        /// Nombre de tours joués
        /// </summary>
        public int Tour { get; private init; }

        /// <summary>
        /// Liste des gagnants.
        /// </summary>
        public IReadOnlyList<string> Gagnants { get; private init; }

        /// <summary>
        /// Liste des perdants.
        /// </summary>
        public IReadOnlyList<string> Perdants { get; private init; }

        /// <summary>
        /// Constructeur de PartiePartieTermineeEventArgs, avec la liste des gagnants et perdants spécifiés.
        /// </summary>
        /// <param name="gagnants">Liste des gagnants.</param>
        /// <param name="perdants">Liste des perdants.</param>
        public PartiePartieTermineeEventArgs(int tour, IReadOnlyList<string> gagnants, IReadOnlyList<string> perdants)
        {
            Tour = tour;
            Gagnants = gagnants;
            Perdants = perdants;
        }
    }
}
