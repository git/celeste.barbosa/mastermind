﻿using CoreLibrary.Core;

namespace CoreLibrary.Evenements
{
    /// <summary>
    /// Classe contenant les arguments passés en paramètre lors de l'événement PlateauAjouterCode.
    /// </summary>
    public class PlateauAjouterCodeEventArgs : EventArgs
    {
        /// <summary>
        /// Le plateau où nous souhaitons ajouté le code.
        /// </summary>
        public Plateau Plateau { get; private init; }

        /// <summary>
        /// Constructeur de PlateauAjouterCodeEventArgs, avec le plateau spécifié.
        /// </summary>
        /// <param name="plateau">Le plateau que nous souhaitons avoir.</param>
        public PlateauAjouterCodeEventArgs(Plateau plateau)
        {
            Plateau = plateau;
        }
    }
}
