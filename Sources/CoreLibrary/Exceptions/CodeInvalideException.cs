﻿using System.Runtime.Serialization;

namespace CoreLibrary.Exceptions
{
    /// <summary>
    /// Exception levée lorsqu'un code avec une taille invalide est ajouté à la grille.
    /// </summary>
    [Serializable]
    public class CodeInvalideException : Exception
    {
        // Message par défaut
        private const string messageDefaut = "Le code que vous essayez d'ajouter est invalide.";

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="CodeInvalideException"/> avec le message par défaut.
        /// </summary>
        public CodeInvalideException() : base(messageDefaut)
        { }

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="CodeInvalideException"/> avec des attributs spécifiés.
        /// </summary>
        /// <param name="tailleCodeAjoute">La taille du code que vous essayez d'ajouter.</param>
        /// <param name="tailleCodePlateau">La taille du code que le plateau attend.</param>
        public CodeInvalideException(int tailleCodeAjoute, int tailleCodePlateau) :
            base($"Le code que vous essayez d'ajouter est un code de taille {tailleCodeAjoute}, or le plateau attend un code de {tailleCodePlateau}.")
        { }

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="CodeInvalideException"/> avec le message spécifié.
        /// </summary>
        public CodeInvalideException(string message) : base(message)
        { }

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="CodeInvalideException"/> avec le message et l'exception parent spécifiés.
        /// </summary>
        public CodeInvalideException(string message, Exception exception) : base(message, exception)
        { }

        [Obsolete("This method is obsolete. Use alternative methods for data retrieval.", DiagnosticId = "SYSLIB0051")]
        protected CodeInvalideException(SerializationInfo info, StreamingContext contexte) : base(info, contexte) { }

        [Obsolete("This method is obsolete. Use alternative methods for data retrieval.", DiagnosticId = "SYSLIB0051")]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}
