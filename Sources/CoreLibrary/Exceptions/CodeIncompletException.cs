﻿using System.Runtime.Serialization;

namespace CoreLibrary.Exceptions
{
    /// <summary>
    /// Exception levée lorsqu'un code incomplet est ajouté à la grille.
    /// </summary>
    [Serializable]
    public class CodeIncompletException : Exception
    {
        // Message par défaut
        private const string messageDefaut = "Le code que vous essayez d'ajouter dans la grille n'est pas complet.";

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="CodeIncompletException"/> avec le message par défaut.
        /// </summary>
        public CodeIncompletException() : base(messageDefaut)
        { }

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="CodeIncompletException"/> avec le message spécifié.
        /// </summary>
        public CodeIncompletException(string message) : base(message)
        { }

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="CodeIncompletException"/> avec le message et l'exception parent spécifié.
        /// </summary>
        public CodeIncompletException(string message, Exception exception) : base(message, exception)
        { }

        [Obsolete("This method is obsolete. Use alternative methods for data retrieval.", DiagnosticId = "SYSLIB0051")]
        protected CodeIncompletException(SerializationInfo info, StreamingContext contexte) : base(info, contexte) { }

        [Obsolete("This method is obsolete. Use alternative methods for data retrieval.", DiagnosticId = "SYSLIB0051")]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}
