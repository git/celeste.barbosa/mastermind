﻿using System.Runtime.Serialization;

namespace CoreLibrary.Exceptions
{
    /// <summary>
    /// Exception levée lorsqu'un joueur rejoint une partie où il se situe déjà.
    /// </summary>
    [Serializable]
    public class JoueurDejaPresentException : Exception
    {
        // Message par défaut
        private const string messageDefaut = "Le joueur que vous souhaitez ajouter est déjà dans la partie.";

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="JoueurDejaPresentException"/> avec le message par défaut.
        /// </summary>
        public JoueurDejaPresentException() : base(messageDefaut)
        {}

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="JoueurDejaPresentException"/> avec le message spécifié.
        /// </summary>
        public JoueurDejaPresentException(string message) : base(message)
        {}

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="JoueurDejaPresentException"/> avec le message et l'exception parent spécifiés.
        /// </summary>
        public JoueurDejaPresentException(string message, Exception exception) : base(message, exception)
        {}

        [Obsolete("This method is obsolete. Use alternative methods for data retrieval.", DiagnosticId = "SYSLIB0051")]
        protected JoueurDejaPresentException(SerializationInfo info, StreamingContext contexte) : base(info, contexte) 
        {}

        [Obsolete("This method is obsolete. Use alternative methods for data retrieval.", DiagnosticId = "SYSLIB0051")]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}
