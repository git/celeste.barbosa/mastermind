﻿using System.Runtime.Serialization;

namespace CoreLibrary.Exceptions
{
    /// <summary>
    /// Exception levée lorsqu'un code est ajouté à une grille qui est déjà complète.
    /// </summary>
    [Serializable]
    public class GrilleCompleteException : Exception
    {
        // Message par défaut
        private const string messageDefaut = "La grille dans laquelle vous essayez d'ajouter un code est déjà complète.";

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="GrilleCompleteException"/> avec le message par défaut.
        /// </summary>
        public GrilleCompleteException() : base(messageDefaut)
        { }

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="GrilleCompleteException"/> avec le message spécifié.
        /// </summary>
        public GrilleCompleteException(string message) : base(message)
        { }

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="GrilleCompleteException"/> avec le message et l'exception parent spécifiés.
        /// </summary>
        public GrilleCompleteException(string message, Exception exception) : base(message, exception)
        { }

        [Obsolete("This method is obsolete. Use alternative methods for data retrieval.", DiagnosticId = "SYSLIB0051")]
        protected GrilleCompleteException(SerializationInfo info, StreamingContext contexte) : base(info, contexte) { }

        [Obsolete("This method is obsolete. Use alternative methods for data retrieval.", DiagnosticId = "SYSLIB0051")]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}
