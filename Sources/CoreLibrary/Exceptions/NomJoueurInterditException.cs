﻿using System.Runtime.Serialization;

namespace CoreLibrary.Exceptions
{
    /// <summary>
    /// Exception levée lorsqu'un utilisateur choisi un nom interdit pour son joueur.
    /// </summary>
    [Serializable]
    public class NomJoueurInterditException : Exception
    {
        // Message par défaut
        private const string messageDefaut = "Le nom choisi pour votre joueur est interdit.";

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="NomJoueurInterditException"/> avec le message par défaut.
        /// </summary>
        public NomJoueurInterditException() : base(messageDefaut)
        {}

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="NomJoueurInterditException"/> avec le message spécifié.
        /// </summary>
        public NomJoueurInterditException(string message) : base(message)
        {}

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="NomJoueurInterditException"/> avec le message et l'exception parent spécifiés.
        /// </summary>
        public NomJoueurInterditException(string message, Exception exception) : base(message, exception)
        {}

        [Obsolete("This method is obsolete. Use alternative methods for data retrieval.", DiagnosticId = "SYSLIB0051")]
        protected NomJoueurInterditException(SerializationInfo info, StreamingContext contexte) : base(info, contexte) 
        {}

        [Obsolete("This method is obsolete. Use alternative methods for data retrieval.", DiagnosticId = "SYSLIB0051")]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}
