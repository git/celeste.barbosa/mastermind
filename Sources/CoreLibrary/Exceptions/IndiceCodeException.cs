﻿using System.Runtime.Serialization;

namespace CoreLibrary.Exceptions
{
    /// <summary>
    /// Exception levée lorsqu'un indice de jeton est invalide.
    /// </summary>
    [Serializable]
    public class IndiceCodeException : Exception
    {
        // Message par défaut
        private const string messageDefaut = "L'indice du jeton que vous essayez de récupérer est hors de la plage valide.";

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="IndiceCodeException"/> avec le message par défaut.
        /// </summary>
        public IndiceCodeException() : base(messageDefaut)
        { }

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="IndiceCodeException"/> avec les attributs spécifiés.
        /// </summary>
        /// <param name="indice">L'indice incorrect qui a été spécifié.</param>
        /// <param name="indiceMax">L'indice maximum permis.</param>
        public IndiceCodeException(int indice, int indiceMax) :
            base($"Vous avez essayé de récupérer le jeton à la place {indice}, mais son indice doit être compris entre 0 et {indiceMax}.")
        { }

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="IndiceCodeException"/> avec le message spécifié.
        /// </summary>
        public IndiceCodeException(string message) : base(message)
        { }

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="IndiceCodeException"/> avec le message et l'exception parent spécifiés.
        /// </summary>
        public IndiceCodeException(string message, Exception exception) : base(message, exception)
        { }

        [Obsolete("This method is obsolete. Use alternative methods for data retrieval.", DiagnosticId = "SYSLIB0051")]
        protected IndiceCodeException(SerializationInfo info, StreamingContext contexte) : base(info, contexte) { }

        [Obsolete("This method is obsolete. Use alternative methods for data retrieval.", DiagnosticId = "SYSLIB0051")]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}
