﻿using System.Runtime.Serialization;

namespace CoreLibrary.Exceptions
{
    /// <summary>
    /// Exception levée lorsqu'un jeton est ajouté à un code déjà complet.
    /// </summary>
    [Serializable]
    public class CodeCompletException : Exception
    {
        // Message par défaut
        private const string messageDefaut = "Le code dans lequel vous essayez d'ajouter un jeton est déjà complet.";

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="CodeCompletException"/> avec le message par défaut.
        /// </summary>
        public CodeCompletException() : base(messageDefaut)
        {}

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="CodeCompletException"/> avec le message spécifié.
        /// </summary>
        public CodeCompletException(string message) : base(message)
        {}

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="CodeCompletException"/> avec le message et l'exception parent spécifiés.
        /// </summary>
        public CodeCompletException(string message, Exception exception) : base(message, exception)
        {}

        [Obsolete("This method is obsolete. Use alternative methods for data retrieval.", DiagnosticId = "SYSLIB0051")]
        protected CodeCompletException(SerializationInfo info, StreamingContext contexte) : base(info, contexte) 
        {}

        [Obsolete("This method is obsolete. Use alternative methods for data retrieval.", DiagnosticId = "SYSLIB0051")]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}
