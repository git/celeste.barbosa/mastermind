﻿using System.Runtime.Serialization;

namespace CoreLibrary.Exceptions
{
    /// <summary>
    /// Exception levée lorsqu'une taille de code invalide est spécifiée.
    /// </summary>
    [Serializable]
    public class TailleCodeException : Exception
    {
        // Message par défaut
        private const string messageDefaut = "Un code doit avoir une taille positive non nulle.";

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="TailleCodeException"/> avec le message par défaut.
        /// </summary>
        public TailleCodeException() : base(messageDefaut)
        { }

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="TailleCodeException"/> avec l'attribut spécifié.
        /// </summary>
        public TailleCodeException(int taille) :
            base($"Un code doit avoir une taille positive non nulle, or il a reçu {taille}.")
        { }

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="TailleCodeException"/> avec le message spécifié.
        /// </summary>
        public TailleCodeException(string message) : base(message)
        { }

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="TailleCodeException"/> avec le message et l'exception parent spécifiés.
        /// </summary>
        public TailleCodeException(string message, Exception exception) : base(message, exception)
        { }

        [Obsolete("This method is obsolete. Use alternative methods for data retrieval.", DiagnosticId = "SYSLIB0051")]
        protected TailleCodeException(SerializationInfo info, StreamingContext contexte) : base(info, contexte) { }

        [Obsolete("This method is obsolete. Use alternative methods for data retrieval.", DiagnosticId = "SYSLIB0051")]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}
