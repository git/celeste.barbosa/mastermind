﻿using System.Runtime.Serialization;

namespace CoreLibrary.Exceptions
{
    /// <summary>
    /// Exception levée lorsqu'une taille de grille invalide est spécifiée.
    /// </summary>
    [Serializable]
    public class TailleGrilleException : Exception
    {
        // Message par défaut
        private const string messageDefaut = "Une grille doit avoir une taille positive non nulle.";

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="TailleGrilleException"/> avec le message par défaut.
        /// </summary>
        public TailleGrilleException() : base(messageDefaut)
        { }

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="TailleGrilleException"/> avec l'attribut spécifié.
        /// <param name="taille">La taille de la grille.</param>
        /// </summary>
        public TailleGrilleException(int taille) :
            base($"Une grille doit avoir une taille positive non nulle, or elle a reçu {taille}.")
        { }

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="TailleGrilleException"/> avec le message spécifié.
        /// </summary>
        public TailleGrilleException(string message) : base(message)
        { }

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="TailleGrilleException"/> avec le message et l'exception parent spécifiés.
        /// </summary>
        public TailleGrilleException(string message, Exception exception) : base(message, exception)
        { }

        [Obsolete("This method is obsolete. Use alternative methods for data retrieval.", DiagnosticId = "SYSLIB0051")]
        protected TailleGrilleException(SerializationInfo info, StreamingContext contexte) : base(info, contexte) { }

        [Obsolete("This method is obsolete. Use alternative methods for data retrieval.", DiagnosticId = "SYSLIB0051")]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}
