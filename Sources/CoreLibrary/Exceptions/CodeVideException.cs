﻿using System.Runtime.Serialization;

namespace CoreLibrary.Exceptions
{
    /// <summary>
    /// Exception levée lorsqu'un jeton est supprimé d'un code qui est déjà vide.
    /// </summary>
    [Serializable]
    public class CodeVideException : Exception
    {
        // Message par défaut
        private const string messageDefaut = "Le code dans lequel vous essayez de supprimer un jeton est déjà vide.";

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="CodeVideException"/> avec le message par défaut.
        /// </summary>
        public CodeVideException() : base(messageDefaut)
        { }

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="CodeVideException"/> avec le message spécifié.
        /// </summary>
        public CodeVideException(string message) : base(message)
        { }

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="CodeVideException"/> avec le message et l'exception parent spécifiés.
        /// </summary>
        public CodeVideException(string message, Exception exception) : base(message, exception)
        { }

        [Obsolete("This method is obsolete. Use alternative methods for data retrieval.", DiagnosticId = "SYSLIB0051")]
        protected CodeVideException(SerializationInfo info, StreamingContext contexte) : base(info, contexte) { }

        [Obsolete("This method is obsolete. Use alternative methods for data retrieval.", DiagnosticId = "SYSLIB0051")]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
        }
    }
}
