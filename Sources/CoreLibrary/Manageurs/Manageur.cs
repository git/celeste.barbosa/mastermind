using CoreLibrary.Persistance;
using CoreLibrary.Joueurs;
using CoreLibrary.Regles;
using CoreLibrary.Statistiques;

namespace CoreLibrary.Manageurs
{
    /// <summary>
    /// Classe représentant un manager.
    /// </summary>
    public class Manageur
    {
        /// <summary>
        /// Attribut privé pour la persistance.
        /// </summary>
        private readonly IPersistance persistance;

        /// <summary>
        /// Liste des joueurs enregistrés.
        /// </summary>
        private readonly List<Joueur> joueurs;

        /// <summary>
        /// Liste des parties enregistrées.
        /// </summary>
        private readonly List<Partie> parties;

        /// <summary>
        /// Obtient la liste des joueurs enregistrés.
        /// </summary>
        public IReadOnlyList<Joueur> Joueurs => joueurs;

        /// <summary>
        /// Obtient la liste des parties enregistrées terminés.
        /// </summary>
        public IReadOnlyList<Partie> Parties => parties;

        /// <summary>
        /// Obtient la liste des parties non terminées.
        /// </summary>
        public IEnumerable<Partie> PartiesNonTerminees => parties.Where(partie => !partie.Termine).Reverse();

        /// <summary>
        /// Constructeur de manager avec la persistance spécifiée.
        /// </summary>
        /// <param name="persistance">Persistance permettant le chargement des joueurs et parties.</param>
        public Manageur(IPersistance persistance)
        {
            this.persistance = persistance;

            joueurs = persistance.Charger<Joueur>().ToList();
            parties = persistance.Charger<Partie>().ToList();
        }

        /// <summary>
        /// Sauvegarde les joueurs et les parties.
        /// </summary>
        private void Sauvegarder()
        {
            persistance.Enregistrer(joueurs.ToArray());
            persistance.Enregistrer(parties.ToArray());
        }

        /// <summary>
        /// Charge une partie avec la partie à charger spécifiée.
        /// </summary>
        /// <param name="partie">La partie que l'on veut charger.</param>
        /// <returns>Renvoie la partie.</returns>
        public Partie ChargerPartie(Partie partie)
        {
            parties.Remove(partie);

            Partie nouvellePartie = new Partie(partie);
            parties.Add(nouvellePartie);

            EcouterPartie(nouvellePartie);

            return nouvellePartie;
        }

        /// <summary>
        /// Créer une nouvelle partie avec les règles spécifiée.
        /// </summary>
        /// <param name="regles">Regle permettant la création de la partie.</param>
        /// <returns>Renvoie la nouvelle partie créée.</returns>
        public Partie NouvellePartie(IRegles regles)
        {
            Partie partie = new Partie(regles);
            parties.Add(partie);

            EcouterPartie(partie);

            return partie;
        }

        /// <summary>
        /// Suivre le cour d'une partie avec la partie spécifiée.
        /// Sauvegarde quand certains événements sont appelés dans la partie.
        /// </summary>
        /// <param name="partie">La partie à écouter.</param>
        private void EcouterPartie(Partie partie)
        {
            partie.PartieDemanderJoueur += (sender, e) => Sauvegarder();
            partie.PartieDebutPartie += (sender, e) => Sauvegarder();
            partie.PartieDemanderJoueurJouer += (sender, e) => Sauvegarder();
            partie.PartiePasserLaMain += (sender, e) => Sauvegarder();

            partie.PartiePartieTerminee += (sender, e) =>
            {
                foreach (string joueur in e.Gagnants.Concat(e.Perdants))
                {
                    Joueur? j = DemanderJoueurExistant(joueur);

                    if (j == null)
                        continue;

                    double coupmoyen = j.Statistique(partie.Regles, Statistique.CoupMoyen);

                    double partiesJouees =
                        j.Statistique(partie.Regles, Statistique.PartieGagnee) +
                        j.Statistique(partie.Regles, Statistique.PartieEgalite) +
                        j.Statistique(partie.Regles, Statistique.PartiePerdue);

                    double difference = (-coupmoyen + e.Tour) / (partiesJouees + 1);

                    j.IncrementerStatistique(partie.Regles, Statistique.CoupMoyen, difference);
                }

                if (e.Gagnants.Count == 1)
                {
                    DemanderJoueurExistant(e.Gagnants[0])?.IncrementerStatistique(partie.Regles, Statistique.PartieGagnee);
                }
                else
                {
                    foreach (string gagnant in e.Gagnants)
                        DemanderJoueurExistant(gagnant)?.IncrementerStatistique(partie.Regles, Statistique.PartieEgalite);
                }

                foreach (string perdant in e.Perdants)
                {
                    DemanderJoueurExistant(perdant)?.IncrementerStatistique(partie.Regles, Statistique.PartiePerdue);
                }
                Sauvegarder();
            };
        }

        /// <summary>
        /// Recherche un joueur existant par son nom.
        /// </summary>
        /// <param name="nom">Le nom du joueur à rechercher.</param>
        /// <returns>Le joueur s'il existe, sinon null.</returns>
        private Joueur? DemanderJoueurExistant(string nom)
        {
            foreach (Joueur joueur in joueurs)
            {
                if (joueur.Nom == nom)
                {
                    return joueur;
                }
            }

            return null;
        }

        /// <summary>
        /// Recherche un joueur par son nom ou le crée s'il n'existe pas.
        /// </summary>
        /// <param name="nom">Le nom du joueur.</param>
        /// <returns>Le joueur trouvé ou créé.</returns>
        public Joueur DemanderJoueur(string nom)
        {
            Joueur? joueur = DemanderJoueurExistant(nom);
            if (joueur == null)
            {
                joueur = new Joueur(nom);
                joueurs.Add(joueur);
            }

            return joueur;
        }
    }
}
