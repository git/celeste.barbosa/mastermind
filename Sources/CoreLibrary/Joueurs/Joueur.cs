﻿using CoreLibrary.Persistance;
using CoreLibrary.Evenements;
using CoreLibrary.Regles;
using CoreLibrary.Statistiques;
using System.Runtime.Serialization;

namespace CoreLibrary.Joueurs
{
    /// <summary>
    /// Représente un joueur.
    /// </summary>
    [DataContract]
    public class Joueur : IEstPersistant
    {
        /// <summary>
        /// Événement déclenché lorsqu'un joueur se connecte.
        /// </summary>
        public event EventHandler<JoueurSeConnecterEventArgs>? JoueurSeConnecter;

        private void QuandJoueurSeConnecter(Joueur joueur) => JoueurSeConnecter?.Invoke(this, new JoueurSeConnecterEventArgs(joueur));
        
        [DataMember]
        private Dictionary<(IRegles, Statistique), double> statistiques = new Dictionary<(IRegles, Statistique), double>();

        /// <summary>
        /// Obtient le nom du joueur.
        /// </summary>
        [DataMember]
        public string Nom { get; private set; } = "";

        /// <summary>
        /// Constructeur de joueur.
        /// </summary>
        public Joueur()
        {
        }

        /// <summary>
        /// Constructeur de joueur avec le nom spécifié.
        /// </summary>
        /// <param name="nom">Le nom du joueur.</param>
        public Joueur(string nom)
        {
            Nom = nom;
        }

        /// <summary>
        /// Connecte le joueur.
        /// </summary>
        /// <param name="joueur">Le joueur à connecter.</param>
        public void SeConnecter(Joueur joueur)
        {
            QuandJoueurSeConnecter(joueur);
        }

        /// <summary>
        /// Permet au joueur de jouer une partie.
        /// </summary>
        /// <param name="partie">La partie à jouer.</param>
        public void JouerPartie(Partie partie)
        {
            partie.PartieDemanderJoueurJouer += QuandDemanderJoueurJouer;
        }

        /// <summary>
        /// Méthode appelée lorsque la partie demande à ce joueur de jouer.
        /// </summary>
        /// <param name="sender">La source de l'événement.</param>
        /// <param name="e">Les arguments de l'événement.</param>
        public virtual void QuandDemanderJoueurJouer(object? sender, PartieDemanderJoueurJouerEventArgs e)
        {
        }

        /// <summary>
        /// Retourne une représentation sous forme de chaîne de caractères.
        /// </summary>
        /// <returns>Renvoie une chaîne de caractères du nom du joueur.</returns>
        public override string ToString() => Nom;

        /// <summary>
        /// Obtient la statistique spécifiée pour les règles données.
        /// </summary>
        /// <param name="regles">Les règles du jeu.</param>
        /// <param name="statistique">Le type de statistique.</param>
        /// <returns>La valeur de la statistique.</returns>
        public double Statistique(IRegles regles, Statistique statistique) => statistiques.GetValueOrDefault((regles, statistique),0);

        /// <summary>
        /// Incrémente la valeur de la statistique spécifiée pour les règles données.
        /// </summary>
        /// <param name="regles">Les règles du jeu.</param>
        /// <param name="statistique">Le type de statistique.</param>
        public void IncrementerStatistique(IRegles regles, Statistique statistique, double valeur = 1) => 
            statistiques[(regles, statistique)] = Statistique(regles, statistique) + valeur;
    }
}
