﻿namespace CoreLibrary.Persistance
{
    /// <summary>
    /// Interface IPersistance permettant de charger et d'enregistrer.
    /// </summary>
    public interface IPersistance
    {
        /// <summary>
        /// Charge les objets de type T.
        /// </summary>
        /// <typeparam name="T">Le type d'objet à charger.</typeparam>
        /// <returns>Un tableau d'objets chargés.</returns>
        public T[] Charger<T>() where T : IEstPersistant;

        /// <summary>
        /// Enregistre les éléments spécifiés.
        /// </summary>
        /// <typeparam name="T">Le type d'objet à enregistrer.</typeparam>
        /// <param name="elements">Le tableau d'objets à enregistrer.</param>
        public void Enregistrer<T>(T[] elements) where T : IEstPersistant;
    }
}
