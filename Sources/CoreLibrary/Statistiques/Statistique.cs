﻿namespace CoreLibrary.Statistiques
{
    /// <summary>
    /// Enumération des statistiques.
    /// </summary>
    public enum Statistique
    {
        CoupMoyen,
        PartieGagnee,
        PartiePerdue,
        PartieEgalite
    }
}
