﻿using CoreLibrary;
using CoreLibrary.Joueurs;
using CoreLibrary.Persistance;
using CoreLibrary.Regles;
using System.Diagnostics.CodeAnalysis;

namespace Persistance.Persistance
{
    /// <summary>
    /// Classe PersistanceStub, implémentant l'interface IPersistance, pour la persistance des données en brut dans le code.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class PersistanceStub : IPersistance
    {
        /// <summary>
        /// En fonction du type T, renvoie une liste d'éléments.
        /// </summary>
        /// <returns>Renvoie un tableau de tous les éléments chargés.</returns>
        public T[] Charger<T>() where T : IEstPersistant
        {
            if (typeof(T).Equals(typeof(Joueur)))
            {
                return [
                    (T)(object)new Joueur("Céleste"),
                    (T)(object)new Joueur("Pauline"),
                    (T)(object)new Joueur("Camille")
                ];
            }

            if (typeof(T).Equals(typeof(Partie)))
            {
                return [
                    (T)(object)new Partie(new ReglesClassiques()),
                    (T)(object)new Partie(new ReglesDifficiles())
                ];
            }

            return Array.Empty<T>();
        }

        /// <summary>
        /// Enregistre les éléments spécifiés dans le fichier de sauvegarde au format JSON.
        /// </summary>
        /// <param name="elements">Elements à enregistrer dans le fichier de sauvegarde.</param>
        [SuppressMessage("CodeQuality", "S1186:Methods should not be empty", Justification = "Cette méthode est laissée vide volontairement.")]
        public void Enregistrer<T>(T[] elements) where T : IEstPersistant
        {
        }
    }
}
