﻿using System.Runtime.Serialization.Json;
using System.Text;
using System.Diagnostics.CodeAnalysis;
using System.Xml;
using CoreLibrary.Persistance;
using CoreLibrary.Regles;

namespace Persistance.Persistance
{
    /// <summary>
    /// Classe PersistanceJson, implémentant l'interface IPersistance, pour la persistance des données au format JSON.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class PersistanceJson : IPersistance
    {
        /// <summary>
        /// Chaîne de caractères représentant le nom du dossier pour sauvegarder et charger.
        /// </summary>
        private readonly string nomDossier = "Fichiers";

        /// <summary>
        /// Chaîne de caractères représentant le nom du dossier.
        /// </summary>
        private readonly string dossier;

        /// <summary>
        /// Constructeur de PersistanceJson.
        /// </summary>
        public PersistanceJson(string dossier)
        {
            if(!Directory.Exists(dossier))
                Directory.CreateDirectory(dossier);

            this.dossier = dossier;
        }

        /// <summary>
        /// Charge les données trouvées dans le fichier de sauvegarde au format JSON.
        /// </summary>
        /// <returns>Renvoie un tableau de tous les éléments chargés.</returns>
        public T[] Charger<T>() where T : IEstPersistant
        {
            string fichier = $"{typeof(T).Name.ToLower()}s.json";

            Directory.SetCurrentDirectory(dossier);

            if (!Directory.Exists(nomDossier))
                return [];

            Directory.SetCurrentDirectory(Path.Combine(Directory.GetCurrentDirectory(), nomDossier));
            
            if (!File.Exists(fichier))
                return [];

            DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(
                typeof(T[]),
                typeof(IRegles).Assembly.GetTypes()
                .Where(type => typeof(IRegles).IsAssignableFrom(type) && type.IsClass)
            );
            T[]? elements;

            using (FileStream s = File.OpenRead(fichier))
            {
                elements = jsonSerializer.ReadObject(s) as T[];
            }

            return elements != null ? elements : Array.Empty<T>();
        }

        /// <summary>
        /// Enregistre les éléments spécifiés dans le fichier de sauvegarde au format JSON.
        /// </summary>
        /// <param name="elements">Elements à enregistrer dans le fichier de sauvegarde.</param>
        public void Enregistrer<T>(T[] elements) where T : IEstPersistant
        {
            string fichier = $"{typeof(T).Name.ToLower()}s.json";

            Directory.SetCurrentDirectory(dossier);

            if (!Directory.Exists(nomDossier))
                Directory.CreateDirectory(nomDossier);

            Directory.SetCurrentDirectory(Path.Combine(Directory.GetCurrentDirectory(), nomDossier));

            DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(
                typeof(T[]),
                typeof(IRegles).Assembly.GetTypes()
                .Where(type => typeof(IRegles).IsAssignableFrom(type) && type.IsClass)
            );

            using (FileStream s = File.Create(fichier))
            {
                using (XmlDictionaryWriter writer = JsonReaderWriterFactory.CreateJsonWriter(
                                        s,
                                        Encoding.UTF8,
                                        false,
                                        true))
                {
                    jsonSerializer.WriteObject(writer, elements);
                }
            }
        }
    }
}
