﻿using CoreLibrary.Core;
using Xunit;

namespace UnitTesting
{
    /// <summary>
    /// Classe de test pour la classe Jeton.
    /// </summary>
    public class JetonUT
    {
        /// <summary>
        /// Test du constructeur de Jeton.
        /// </summary>
        [Fact]
        public void TestConstructeurValide()
        {
            Couleur[] listeCouleurs = (Couleur[])Enum.GetValues(typeof(Couleur));

            for (int i=0; i<listeCouleurs.Length; ++i)
            {
                Jeton jeton = new Jeton(listeCouleurs[i]);
                Assert.Equal(listeCouleurs[i], jeton.Couleur);
            }
        }

        /// <summary>
        /// Test pour verifier que des jetons soient égaux.
        /// </summary>
        [Fact]
        public void TestEquals()
        {
            Jeton j1 = new Jeton();
            j1.GetHashCode();
            int a = 5;

            Assert.False(j1.Equals(a));

            Jeton j2 = new Jeton(Couleur.Rouge);
            Jeton j3 = new Jeton(Couleur.Vert);

            Assert.False(j2.Equals(j3));
            Assert.False(j2 == j3);
            Assert.True(j2 != j3);

            Jeton j4 = new Jeton(Couleur.Jaune);
            Jeton j5 = new Jeton(Couleur.Jaune);

            Assert.True(j4.Equals(j5));
            Assert.True(j4 == j5);
            Assert.False(j4 != j5);
        }
    }
}
