﻿using CoreLibrary.Exceptions;
using System.Reflection;
using System.Runtime.Serialization;
using Xunit;

namespace UnitTesting
{
    /// <summary>
    /// Classe de test de l'exception JoueurDejaPresentException.
    /// </summary>
    public class JoueurDejaPresentExceptionUT
    {
        /// <summary>
        /// Test de l'exception JoueurDejaPresentException par defaut.
        /// </summary>
        [Fact]
        public void ExceptionDefaut()
        {
            Assert.ThrowsAsync<JoueurDejaPresentException>(() => throw new JoueurDejaPresentException());
        }

        /// <summary>
        /// Test du message de l'exception JoueurDejaPresentException.
        /// </summary>
        [Fact]
        public void ExceptionMessage()
        {
            string message = "Mon super gros problème.";

            Assert.ThrowsAsync<JoueurDejaPresentException>(() => throw new JoueurDejaPresentException(message));

            try
            {
                throw new JoueurDejaPresentException(message);
            }
            catch (JoueurDejaPresentException e)
            {
                Assert.Equal(message, e.Message);
            }
        }

        /// <summary>
        /// Test de l'exception JoueurDejaPresentException et ses messages.
        /// </summary>
        [Fact]
        public void ExceptionMessageEtException()
        {
            string message = "Mon super gros problème.";
            string message2 = "Pas de chance...";
            InvalidOperationException parent = new InvalidOperationException(message2);

            Assert.ThrowsAsync<JoueurDejaPresentException>(() => throw new JoueurDejaPresentException(message, parent));

            try
            {
                throw new JoueurDejaPresentException(message, parent);
            }
            catch (JoueurDejaPresentException e)
            {
                Assert.Equal(message, e.Message);
                Assert.NotNull(e.InnerException);
                Assert.IsType<InvalidOperationException>(e.InnerException);
                Assert.Equal(message2, e.InnerException.Message);
            }
        }

        /// <summary>
        /// Test de la serilisation de l'exception JoueurDejaPresentException.
        /// </summary>
        [Fact]
        public void ExceptionSerialisation()
        {
            JoueurDejaPresentException exception = new JoueurDejaPresentException();

#pragma warning disable SYSLIB0050
            SerializationInfo info = new SerializationInfo(typeof(JoueurDejaPresentException), new FormatterConverter());
            StreamingContext contexte = new StreamingContext(StreamingContextStates.All);
#pragma warning restore SYSLIB0050

#pragma warning disable SYSLIB0051
            exception.GetObjectData(info, contexte);
#pragma warning restore SYSLIB0051

            Assert.Equal(exception.Message, info.GetString("Message"));

#pragma warning disable SYSLIB0050
            JoueurDejaPresentException exceptionSerialisee =
                (JoueurDejaPresentException)FormatterServices.GetUninitializedObject(typeof(JoueurDejaPresentException));
#pragma warning restore SYSLIB0050

            ConstructorInfo? constructeur = typeof(JoueurDejaPresentException).GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, [typeof(SerializationInfo), typeof(StreamingContext)], null);
            Assert.NotNull(constructeur);
            constructeur.Invoke(exceptionSerialisee, [info, contexte]);

            Assert.Equal(exception.Message, exceptionSerialisee.Message);
        }
    }
}
