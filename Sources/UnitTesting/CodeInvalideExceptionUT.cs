﻿using CoreLibrary.Exceptions;
using System.Reflection;
using System.Runtime.Serialization;
using Xunit;

namespace UnitTesting
{
    /// <summary>
    /// Classe de test de l'exception CodeInvalideException.
    /// </summary>
    public class CodeInvalideExceptionUT
    {
        /// <summary>
        /// Test de l'exception CodeInvalideException par defaut.
        /// </summary>
        [Fact]
        public void ExceptionDefaut()
        {
            Assert.ThrowsAsync<CodeInvalideException>(() => throw new CodeInvalideException());
        }

        /// <summary>
        /// Test des attributs de l'exception CodeInvalideException.
        /// </summary>
        [Fact]
        public void ExceptionAttributs()
        {
            Assert.ThrowsAsync<CodeInvalideException>(() => throw new CodeInvalideException(3, 4));

            try
            {
                throw new CodeInvalideException(3, 4);
            }
            catch (CodeInvalideException e)
            {
                Assert.Contains("3", e.Message);
                Assert.Contains("4", e.Message);
            }
        }

        /// <summary>
        /// Test du message de l'exception CodeInvalideException.
        /// </summary>
        [Fact]
        public void ExceptionMessage()
        {
            string message = "Mon super gros problème.";

            Assert.ThrowsAsync<CodeInvalideException>(() => throw new CodeInvalideException(message));

            try
            {
                throw new CodeInvalideException(message);
            }
            catch(CodeInvalideException e)
            {
                Assert.Equal(message, e.Message);
            }
        }

        /// <summary>
        /// Test de l'exception CodeInvalideException et de ses messages.
        /// </summary>
        [Fact]
        public void ExceptionMessageEtException()
        {
            string message = "Mon super gros problème.";
            string message2 = "Pas de chance...";
            InvalidOperationException parent = new InvalidOperationException(message2);

            Assert.ThrowsAsync<CodeInvalideException>(() => throw new CodeInvalideException(message, parent));

            try
            {
                throw new CodeInvalideException(message, parent);
            }
            catch (CodeInvalideException e)
            {
                Assert.Equal(message, e.Message);
                Assert.NotNull(e.InnerException);
                Assert.IsType<InvalidOperationException>(e.InnerException);
                Assert.Equal(message2, e.InnerException.Message);
            }
        }

        /// <summary>
        /// Test de la serialisation de l'exception CodeInvalideException.
        /// </summary>
        [Fact]
        public void ExceptionSerialisation()
        {
            CodeInvalideException exception = new CodeInvalideException();

#pragma warning disable SYSLIB0050
            SerializationInfo info = new SerializationInfo(typeof(CodeInvalideException), new FormatterConverter());
            StreamingContext contexte = new StreamingContext(StreamingContextStates.All);
#pragma warning restore SYSLIB0050

#pragma warning disable SYSLIB0051
            exception.GetObjectData(info, contexte);
#pragma warning restore SYSLIB0051

            Assert.Equal(exception.Message, info.GetString("Message"));

#pragma warning disable SYSLIB0050
            CodeInvalideException exceptionSerialisee =
                (CodeInvalideException)FormatterServices.GetUninitializedObject(typeof(CodeInvalideException));
#pragma warning restore SYSLIB0050

            ConstructorInfo? constructeur = typeof(CodeInvalideException).GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, [typeof(SerializationInfo), typeof(StreamingContext)], null);
            Assert.NotNull(constructeur);
            constructeur.Invoke(exceptionSerialisee, [info, contexte]);

            Assert.Equal(exception.Message, exceptionSerialisee.Message);
        }

    }
}
