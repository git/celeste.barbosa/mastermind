﻿using CoreLibrary.Exceptions;
using System.Reflection;
using System.Runtime.Serialization;
using Xunit;

namespace UnitTesting
{
    /// <summary>
    /// Classe de test de l'exception TailleCodeException.
    /// </summary>
    public class TailleCodeExceptionUT
    {
        /// <summary>
        /// Test l'exception par defaut.
        /// </summary>
        [Fact]
        public void ExceptionDefaut()
        {
            Assert.ThrowsAsync<TailleCodeException>(() => throw new TailleCodeException());
        }

        /// <summary>
        /// Test les attributs de l'exception.
        /// </summary>
        [Fact]
        public void ExceptionAttributs()
        {
            Assert.ThrowsAsync<TailleCodeException>(() => throw new TailleCodeException(-3));

            try
            {
                throw new TailleCodeException(-3);
            }
            catch (TailleCodeException e)
            {
                Assert.Contains("-3", e.Message);
            }
        }

        /// <summary>
        /// Test le message de l'exception TailleCodeException.
        /// </summary>
        [Fact]
        public void ExceptionMessage()
        {
            string message = "Mon super gros problème.";

            Assert.ThrowsAsync<TailleCodeException>(() => throw new TailleCodeException(message));

            try
            {
                throw new TailleCodeException(message);
            }
            catch(TailleCodeException e)
            {
                Assert.Equal(message, e.Message);
            }
        }

        /// <summary>
        /// Test l'exception TailleCodeException et ses messages.
        /// </summary>
        [Fact]
        public void ExceptionMessageEtException()
        {
            string message = "Mon super gros problème.";
            string message2 = "Pas de chance...";
            InvalidOperationException parent = new InvalidOperationException(message2);

            Assert.ThrowsAsync<TailleCodeException>(() => throw new TailleCodeException(message, parent));

            try
            {
                throw new TailleCodeException(message, parent);
            }
            catch (TailleCodeException e)
            {
                Assert.Equal(message, e.Message);
                Assert.NotNull(e.InnerException);
                Assert.IsType<InvalidOperationException>(e.InnerException);
                Assert.Equal(message2, e.InnerException.Message);
            }
        }

        /// <summary>
        /// Test la serialisation de l'exception TailleCodeException.
        /// </summary>
        [Fact]
        public void ExceptionSerialisation()
        {
            TailleCodeException exception = new TailleCodeException();

#pragma warning disable SYSLIB0050
            SerializationInfo info = new SerializationInfo(typeof(TailleCodeException), new FormatterConverter());
            StreamingContext contexte = new StreamingContext(StreamingContextStates.All);
#pragma warning restore SYSLIB0050

#pragma warning disable SYSLIB0051
            exception.GetObjectData(info, contexte);
#pragma warning restore SYSLIB0051

            Assert.Equal(exception.Message, info.GetString("Message"));

#pragma warning disable SYSLIB0050
            TailleCodeException exceptionSerialisee =
                (TailleCodeException)FormatterServices.GetUninitializedObject(typeof(TailleCodeException));
#pragma warning restore SYSLIB0050

            ConstructorInfo? constructeur = typeof(TailleCodeException).GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, [typeof(SerializationInfo), typeof(StreamingContext)], null);
            Assert.NotNull(constructeur);
            constructeur.Invoke(exceptionSerialisee, [info, contexte]);

            Assert.Equal(exception.Message, exceptionSerialisee.Message);
        }
    }
}
