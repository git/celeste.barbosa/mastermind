﻿using CoreLibrary.Regles;
using Xunit;

namespace UnitTesting
{
    /// <summary>
    /// Classe de test pour la classe ReglesClassiques.
    /// </summary>
    public class ReglesClassiquesUT
    {
        /// <summary>
        /// Test les attributs de ReglesClassiques et verifie qu'ils soient egaux.
        /// </summary>
        [Fact]
        public void Test()
        {
            Assert.Equal(1, new ReglesClassiques().Indice);
            Assert.NotNull(new ReglesClassiques().Nom);
            Assert.NotNull(new ReglesClassiques().Description);
            Assert.Equal(2, new ReglesClassiques().NbJoueurs);
            Assert.Equal(12, new ReglesClassiques().NbTour);
            Assert.Equal(4, new ReglesClassiques().TailleCode);
            Assert.True(new ReglesClassiques().Equals(new ReglesClassiques()));
            new ReglesClassiques().GetHashCode();
        }
    }
}
