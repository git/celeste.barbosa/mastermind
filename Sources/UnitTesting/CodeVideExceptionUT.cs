﻿using CoreLibrary.Exceptions;
using System.Reflection;
using System.Runtime.Serialization;
using Xunit;

namespace UnitTesting
{
    /// <summary>
    /// Classe de test de l'exception CodeVideException.
    /// </summary>
    public class CodeVideExceptionUT
    {
        /// <summary>
        /// Test de l'exception CodeVideException par defaut.
        /// </summary>
        [Fact]
        public void ExceptionDefaut()
        {
            Assert.ThrowsAsync<CodeVideException>(() => throw new CodeVideException());
        }

        /// <summary>
        /// Test du message de l'exception CodeVideException.
        /// </summary>
        [Fact]
        public void ExceptionMessage()
        {
            string message = "Mon super gros problème.";

            Assert.ThrowsAsync<CodeVideException>(() => throw new CodeVideException(message));

            try
            {
                throw new CodeVideException(message);
            }
            catch(CodeVideException e)
            {
                Assert.Equal(message, e.Message);
            }
        }

        /// <summary>
        /// Test de l'exception CodeVideException et de ses messages.
        /// </summary>
        [Fact]
        public void ExceptionMessageEtException()
        {
            string message = "Mon super gros problème.";
            string message2 = "Pas de chance...";
            InvalidOperationException parent = new InvalidOperationException(message2);

            Assert.ThrowsAsync<CodeVideException>(() => throw new CodeVideException(message, parent));

            try
            {
                throw new CodeVideException(message, parent);
            }
            catch (CodeVideException e)
            {
                Assert.Equal(message, e.Message);
                Assert.NotNull(e.InnerException);
                Assert.IsType<InvalidOperationException>(e.InnerException);
                Assert.Equal(message2, e.InnerException.Message);
            }
        }

        /// <summary>
        /// Test de la serialisation de l'exception CodeVideException.
        /// </summary>
        [Fact]
        public void ExceptionSerialisation()
        {
            CodeVideException exception = new CodeVideException();

#pragma warning disable SYSLIB0050
            SerializationInfo info = new SerializationInfo(typeof(CodeVideException), new FormatterConverter());
            StreamingContext contexte = new StreamingContext(StreamingContextStates.All);
#pragma warning restore SYSLIB0050

#pragma warning disable SYSLIB0051
            exception.GetObjectData(info, contexte);
#pragma warning restore SYSLIB0051

            Assert.Equal(exception.Message, info.GetString("Message"));

#pragma warning disable SYSLIB0050
            CodeVideException exceptionSerialisee =
                (CodeVideException)FormatterServices.GetUninitializedObject(typeof(CodeVideException));
#pragma warning restore SYSLIB0050

            ConstructorInfo? constructeur = typeof(CodeVideException).GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, [typeof(SerializationInfo), typeof(StreamingContext)], null);
            Assert.NotNull(constructeur);
            constructeur.Invoke(exceptionSerialisee, [info, contexte]);

            Assert.Equal(exception.Message, exceptionSerialisee.Message);
        }
    }
}
