﻿using CoreLibrary.Joueurs;
using CoreLibrary.Evenements;
using Xunit;

namespace UnitTesting
{
    /// <summary>
    /// Classe de test de l'evenement PartieDemanderJoueurEventArgs.
    /// </summary>
    public class PartieDemanderJoueurEventArgsUT
    {
        /// <summary>
        /// Test du constructeur de l'evenement valide.
        /// </summary>
        [Fact]
        public void TestConstructeurValide()
        {
            int indice = 0;
            Joueur joueur = new Joueur();

            PartieDemanderJoueurEventArgs evenement = new PartieDemanderJoueurEventArgs(indice, joueur);

            Assert.Equal(indice, evenement.Indice);
            Assert.Equal(joueur, evenement.JoueurDemande);
        }
    }
}
