﻿using CoreLibrary.Exceptions;
using System.Reflection;
using System.Runtime.Serialization;
using Xunit;

namespace UnitTesting
{
    /// <summary>
    /// Classe de test de l'exception TailleGrille.
    /// </summary>
    public class TailleGrilleExceptionUT
    {
        /// <summary>
        /// Test l'exception par defaut.
        /// </summary>
        [Fact]
        public void ExceptionDefaut()
        {
            Assert.ThrowsAsync<TailleGrilleException>(() => throw new TailleGrilleException());
        }

        /// <summary>
        /// Test l'exception avec ces attributs.
        /// </summary>
        [Fact]
        public void ExceptionAttributs()
        {
            Assert.ThrowsAsync<TailleGrilleException>(() => throw new TailleGrilleException(0));

            try
            {
                throw new TailleCodeException(0);
            }
            catch (TailleCodeException e)
            {
                Assert.Contains("0", e.Message);
            }
        }

        /// <summary>
        /// Test l'affichage du message de l'exception TailleGrilleException.
        /// </summary>
        [Fact]
        public void ExceptionMessage()
        {
            string message = "Mon super gros problème.";

            Assert.ThrowsAsync<TailleGrilleException>(() => throw new TailleGrilleException(message));

            try
            {
                throw new TailleGrilleException(message);
            }
            catch(TailleGrilleException e)
            {
                Assert.Equal(message, e.Message);
            }
        }

        /// <summary>
        /// Test l'exception TailleGrilleException et ses messages.
        /// </summary>
        [Fact]
        public void ExceptionMessageEtException()
        {
            string message = "Mon super gros problème.";
            string message2 = "Pas de chance...";
            InvalidOperationException parent = new InvalidOperationException(message2);

            Assert.ThrowsAsync<TailleGrilleException>(() => throw new TailleGrilleException(message, parent));

            try
            {
                throw new TailleGrilleException(message, parent);
            }
            catch (TailleGrilleException e)
            {
                Assert.Equal(message, e.Message);
                Assert.NotNull(e.InnerException);
                Assert.IsType<InvalidOperationException>(e.InnerException);
                Assert.Equal(message2, e.InnerException.Message);
            }
        }

        /// <summary>
        /// Test la serialisation de l'exception TailleGrilleException.
        /// </summary>
        [Fact]
        public void ExceptionSerialisation()
        {
            TailleGrilleException exception = new TailleGrilleException();

#pragma warning disable SYSLIB0050
            SerializationInfo info = new SerializationInfo(typeof(TailleGrilleException), new FormatterConverter());
            StreamingContext contexte = new StreamingContext(StreamingContextStates.All);
#pragma warning restore SYSLIB0050

#pragma warning disable SYSLIB0051
            exception.GetObjectData(info, contexte);
#pragma warning restore SYSLIB0051

            Assert.Equal(exception.Message, info.GetString("Message"));

#pragma warning disable SYSLIB0050
            TailleGrilleException exceptionSerialisee =
                (TailleGrilleException)FormatterServices.GetUninitializedObject(typeof(TailleGrilleException));
#pragma warning restore SYSLIB0050

            ConstructorInfo? constructeur = typeof(TailleGrilleException).GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, [typeof(SerializationInfo), typeof(StreamingContext)], null);
            Assert.NotNull(constructeur);
            constructeur.Invoke(exceptionSerialisee, [info, contexte]);

            Assert.Equal(exception.Message, exceptionSerialisee.Message);
        }
    }
}
