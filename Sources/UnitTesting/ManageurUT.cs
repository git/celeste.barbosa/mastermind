﻿using CoreLibrary;
using CoreLibrary.Evenements;
using CoreLibrary.Joueurs;
using CoreLibrary.Manageurs;
using CoreLibrary.Persistance;
using CoreLibrary.Regles;
using CoreLibrary.Statistiques;
using Persistance.Persistance;
using System.Reflection;
using Xunit;

namespace UnitTesting
{
    /// <summary>
    /// Classe test pour la classe Manageur.
    /// </summary>
    public class ManageurUT
    {
        /// <summary>
        /// Test le constructeur de Manageur.
        /// </summary>
        [Fact]
        public void TestConstruteur()
        {
            IPersistance persistance = new PersistanceStub();
            Manageur manageur = new Manageur(persistance);

            Assert.NotNull(manageur.Joueurs);
            Assert.NotEmpty(manageur.Joueurs);

            Assert.NotNull(manageur.Parties);
            Assert.NotEmpty(manageur.Joueurs);

            Assert.NotNull(manageur.PartiesNonTerminees);
            Assert.NotEmpty(manageur.Joueurs);
        }

        /// <summary>
        /// Test la methode Sauvegarder d'une partie.
        /// </summary>
        [Fact]
        public void TestSauvegarder()
        {
            IPersistance persistance = new PersistanceStub();
            Manageur manageur = new Manageur(persistance);

            MethodInfo? infosMethode = typeof(Manageur).GetMethod("Sauvegarder", BindingFlags.NonPublic | BindingFlags.Instance);
            Assert.NotNull(infosMethode);
            infosMethode.Invoke(manageur, []);
        }

        /// <summary>
        /// Test la methode ChargerPartie qui charge une partie.
        /// </summary>
        [Fact]
        public void TestChargerPartie()
        {
            IPersistance persistance = new PersistanceStub();
            Manageur manageur = new Manageur(persistance);

            Partie partie = manageur.PartiesNonTerminees.First();

            Partie nouvellePartie = manageur.ChargerPartie(partie);

            Assert.DoesNotContain(partie, manageur.Parties);
            Assert.DoesNotContain(partie, manageur.PartiesNonTerminees);

            Assert.Contains(nouvellePartie, manageur.Parties);
            Assert.Contains(nouvellePartie, manageur.PartiesNonTerminees);
        }

        /// <summary>
        /// Test la methode NouvellePartie de Manageur.
        /// </summary>
        [Fact]
        public void TestNouvellePartie()
        {
            IPersistance persistance = new PersistanceStub();
            Manageur manageur = new Manageur(persistance);

            Partie nouvellePartie = manageur.NouvellePartie(new ReglesClassiques());

            Assert.Contains(nouvellePartie, manageur.Parties);
            Assert.Contains(nouvellePartie, manageur.PartiesNonTerminees);
        }

        /// <summary>
        /// Premier test de la methode EcouterPartie de Manageur. 
        /// </summary>
        [Fact]
        public void TestEcouterPartie()
        {
            IPersistance persistance = new PersistanceStub();
            Manageur manageur = new Manageur(persistance);

            MethodInfo? infosMethode = typeof(Manageur).GetMethod("EcouterPartie", BindingFlags.NonPublic | BindingFlags.Instance);
            Assert.NotNull(infosMethode);

            Partie partie = new Partie(new ReglesClassiques());

            FieldInfo? PartieDemanderJoueurInfos = typeof(Partie)
                .GetField("PartieDemanderJoueur", BindingFlags.Instance | BindingFlags.NonPublic);

            Assert.NotNull(PartieDemanderJoueurInfos);

            EventHandler<PartieDemanderJoueurEventArgs>? PartieDemanderJoueur =
                PartieDemanderJoueurInfos.GetValue(partie) as EventHandler<PartieDemanderJoueurEventArgs>;

            FieldInfo? PartieDebutPartieInfos = typeof(Partie)
                .GetField("PartieDebutPartie", BindingFlags.Instance | BindingFlags.NonPublic);

            Assert.NotNull(PartieDebutPartieInfos);

            EventHandler<PartieDebutPartieEventArgs>? PartieDebutPartie =
                PartieDebutPartieInfos.GetValue(partie) as EventHandler<PartieDebutPartieEventArgs>;
            
            FieldInfo? PartieDemanderJoueurJouerInfos = typeof(Partie)
                .GetField("PartieDemanderJoueurJouer", BindingFlags.Instance | BindingFlags.NonPublic);

            Assert.NotNull(PartieDemanderJoueurJouerInfos);

            EventHandler<PartieDemanderJoueurJouerEventArgs>? PartieDemanderJoueurJouer =
                PartieDemanderJoueurJouerInfos.GetValue(partie) as EventHandler<PartieDemanderJoueurJouerEventArgs>;
            
            FieldInfo? PartiePasserLaMainInfos = typeof(Partie)
                .GetField("PartiePasserLaMain", BindingFlags.Instance | BindingFlags.NonPublic);

            Assert.NotNull(PartiePasserLaMainInfos);

            EventHandler<PartiePasserLaMainEventArgs>? PartiePasserLaMain =
                PartiePasserLaMainInfos.GetValue(partie) as EventHandler<PartiePasserLaMainEventArgs>;
            
            FieldInfo? PartiePartieTermineeInfos = typeof(Partie)
                .GetField("PartiePartieTerminee", BindingFlags.Instance | BindingFlags.NonPublic);

            Assert.NotNull(PartiePartieTermineeInfos);

            EventHandler<PartiePartieTermineeEventArgs>? PartiePartieTerminee =
                PartiePartieTermineeInfos.GetValue(partie) as EventHandler<PartiePartieTermineeEventArgs>;

            Assert.Null(PartieDemanderJoueur);
            Assert.Null(PartieDebutPartie);
            Assert.Null(PartieDemanderJoueurJouer);
            Assert.Null(PartiePasserLaMain);
            Assert.Null(PartiePartieTerminee);

            infosMethode.Invoke(manageur, [partie]);

            PartieDemanderJoueur =
                PartieDemanderJoueurInfos.GetValue(partie) as EventHandler<PartieDemanderJoueurEventArgs>;

            PartieDebutPartie =
                PartieDebutPartieInfos.GetValue(partie) as EventHandler<PartieDebutPartieEventArgs>;

            PartieDemanderJoueurJouer =
                PartieDemanderJoueurJouerInfos.GetValue(partie) as EventHandler<PartieDemanderJoueurJouerEventArgs>;

            PartiePasserLaMain =
                PartiePasserLaMainInfos.GetValue(partie) as EventHandler<PartiePasserLaMainEventArgs>;

            PartiePartieTerminee =
                PartiePartieTermineeInfos.GetValue(partie) as EventHandler<PartiePartieTermineeEventArgs>;

            Assert.NotNull(PartieDemanderJoueur);
            Assert.NotNull(PartieDebutPartie);
            Assert.NotNull(PartieDemanderJoueurJouer);
            Assert.NotNull(PartiePasserLaMain);
            Assert.NotNull(PartiePartieTerminee);

            Assert.NotEmpty(PartieDemanderJoueur.GetInvocationList());
            Assert.NotEmpty(PartieDebutPartie.GetInvocationList());
            Assert.NotEmpty(PartieDemanderJoueurJouer.GetInvocationList());
            Assert.NotEmpty(PartiePasserLaMain.GetInvocationList());
            Assert.NotEmpty(PartiePartieTerminee.GetInvocationList());
        }

        /// <summary>
        /// Deuxieme test de la methode EcouterPartie de Manageur. 
        /// </summary>
        [Fact]
        public void TestEcouterPartie2()
        {
            IPersistance persistance = new PersistanceStub();
            Manageur manageur = new Manageur(persistance);

            MethodInfo? infosMethode = typeof(Manageur).GetMethod("EcouterPartie", BindingFlags.NonPublic | BindingFlags.Instance);
            Assert.NotNull(infosMethode);

            Partie partie = new Partie(new ReglesClassiques());

            infosMethode.Invoke(manageur, [partie]);

            FieldInfo? PartiePartieTermineeInfos = typeof(Partie)
                .GetField("PartiePartieTerminee", BindingFlags.Instance | BindingFlags.NonPublic);

            Assert.NotNull(PartiePartieTermineeInfos);

            EventHandler<PartiePartieTermineeEventArgs>? PartiePartieTerminee =
                PartiePartieTermineeInfos.GetValue(partie) as EventHandler<PartiePartieTermineeEventArgs>;

            PartiePartieTerminee =
                PartiePartieTermineeInfos.GetValue(partie) as EventHandler<PartiePartieTermineeEventArgs>;

            Assert.NotNull(PartiePartieTerminee);
            Assert.NotEmpty(PartiePartieTerminee.GetInvocationList());

            Joueur celeste = manageur.DemanderJoueur("Céleste");
            Robot robot = new Robot();

            Assert.Equal(0, celeste.Statistique(new ReglesClassiques(), Statistique.CoupMoyen));
            Assert.Equal(0, celeste.Statistique(new ReglesClassiques(), Statistique.PartieGagnee));
            Assert.Equal(0, celeste.Statistique(new ReglesClassiques(), Statistique.PartieEgalite));
            Assert.Equal(0, celeste.Statistique(new ReglesClassiques(), Statistique.PartiePerdue));

            int tour = 10;
            IReadOnlyList<string> gagnants = [celeste.Nom];
            IReadOnlyList<string> perdants = [robot.Nom];

            PartiePartieTerminee.Invoke(null, new PartiePartieTermineeEventArgs(
                tour, gagnants, perdants
            ));

            Assert.Equal(tour, celeste.Statistique(new ReglesClassiques(), Statistique.CoupMoyen));
            Assert.Equal(1, celeste.Statistique(new ReglesClassiques(), Statistique.PartieGagnee));
            Assert.Equal(0, celeste.Statistique(new ReglesClassiques(), Statistique.PartieEgalite));
            Assert.Equal(0, celeste.Statistique(new ReglesClassiques(), Statistique.PartiePerdue));
        }

        /// <summary>
        /// Troisieme test de la methode EcouterPartie de Manageur. 
        /// </summary>
        [Fact]
        public void TestEcouterPartie3()
        {
            IPersistance persistance = new PersistanceStub();
            Manageur manageur = new Manageur(persistance);

            MethodInfo? infosMethode = typeof(Manageur).GetMethod("EcouterPartie", BindingFlags.NonPublic | BindingFlags.Instance);
            Assert.NotNull(infosMethode);

            Partie partie = new Partie(new ReglesClassiques());

            infosMethode.Invoke(manageur, [partie]);

            FieldInfo? PartiePartieTermineeInfos = typeof(Partie)
                .GetField("PartiePartieTerminee", BindingFlags.Instance | BindingFlags.NonPublic);

            Assert.NotNull(PartiePartieTermineeInfos);

            EventHandler<PartiePartieTermineeEventArgs>? PartiePartieTerminee =
                PartiePartieTermineeInfos.GetValue(partie) as EventHandler<PartiePartieTermineeEventArgs>;

            PartiePartieTerminee =
                PartiePartieTermineeInfos.GetValue(partie) as EventHandler<PartiePartieTermineeEventArgs>;

            Assert.NotNull(PartiePartieTerminee);
            Assert.NotEmpty(PartiePartieTerminee.GetInvocationList());

            Joueur celeste = manageur.DemanderJoueur("Céleste");
            Robot robot = new Robot();

            Assert.Equal(0, celeste.Statistique(new ReglesClassiques(), Statistique.CoupMoyen));
            Assert.Equal(0, celeste.Statistique(new ReglesClassiques(), Statistique.PartieGagnee));
            Assert.Equal(0, celeste.Statistique(new ReglesClassiques(), Statistique.PartieEgalite));
            Assert.Equal(0, celeste.Statistique(new ReglesClassiques(), Statistique.PartiePerdue));

            int tour = 10;
            IReadOnlyList<string> gagnants = [robot.Nom];
            IReadOnlyList<string> perdants = [celeste.Nom];

            PartiePartieTerminee.Invoke(null, new PartiePartieTermineeEventArgs(
                tour, gagnants, perdants
            ));

            Assert.Equal(tour, celeste.Statistique(new ReglesClassiques(), Statistique.CoupMoyen));
            Assert.Equal(0, celeste.Statistique(new ReglesClassiques(), Statistique.PartieGagnee));
            Assert.Equal(0, celeste.Statistique(new ReglesClassiques(), Statistique.PartieEgalite));
            Assert.Equal(1, celeste.Statistique(new ReglesClassiques(), Statistique.PartiePerdue));
        }

        /// <summary>
        /// Quatrieme test de la methode EcouterPartie de Manageur. 
        /// </summary>
        [Fact]
        public void TestEcouterPartie4()
        {
            IPersistance persistance = new PersistanceStub();
            Manageur manageur = new Manageur(persistance);

            MethodInfo? infosMethode = typeof(Manageur).GetMethod("EcouterPartie", BindingFlags.NonPublic | BindingFlags.Instance);
            Assert.NotNull(infosMethode);

            Partie partie = new Partie(new ReglesClassiques());

            infosMethode.Invoke(manageur, [partie]);

            FieldInfo? PartiePartieTermineeInfos = typeof(Partie)
                .GetField("PartiePartieTerminee", BindingFlags.Instance | BindingFlags.NonPublic);

            Assert.NotNull(PartiePartieTermineeInfos);

            EventHandler<PartiePartieTermineeEventArgs>? PartiePartieTerminee =
                PartiePartieTermineeInfos.GetValue(partie) as EventHandler<PartiePartieTermineeEventArgs>;

            PartiePartieTerminee =
                PartiePartieTermineeInfos.GetValue(partie) as EventHandler<PartiePartieTermineeEventArgs>;

            Assert.NotNull(PartiePartieTerminee);
            Assert.NotEmpty(PartiePartieTerminee.GetInvocationList());

            Joueur celeste = manageur.DemanderJoueur("Céleste");
            Robot robot = new Robot();

            Assert.Equal(0, celeste.Statistique(new ReglesClassiques(), Statistique.CoupMoyen));
            Assert.Equal(0, celeste.Statistique(new ReglesClassiques(), Statistique.PartieGagnee));
            Assert.Equal(0, celeste.Statistique(new ReglesClassiques(), Statistique.PartieEgalite));
            Assert.Equal(0, celeste.Statistique(new ReglesClassiques(), Statistique.PartiePerdue));

            int tour = 10;
            IReadOnlyList<string> gagnants = [robot.Nom, celeste.Nom];
            IReadOnlyList<string> perdants = [];

            PartiePartieTerminee.Invoke(null, new PartiePartieTermineeEventArgs(
                tour, gagnants, perdants
            ));

            Assert.Equal(tour, celeste.Statistique(new ReglesClassiques(), Statistique.CoupMoyen));
            Assert.Equal(0, celeste.Statistique(new ReglesClassiques(), Statistique.PartieGagnee));
            Assert.Equal(1, celeste.Statistique(new ReglesClassiques(), Statistique.PartieEgalite));
            Assert.Equal(0, celeste.Statistique(new ReglesClassiques(), Statistique.PartiePerdue));
        }

        /// <summary>
        /// Cinquieme test de la methode EcouterPartie de Manageur. 
        /// </summary>
        [Fact]
        public void TestEcouterPartie5()
        {
            IPersistance persistance = new PersistanceStub();
            Manageur manageur = new Manageur(persistance);

            MethodInfo? infosMethode = typeof(Manageur).GetMethod("EcouterPartie", BindingFlags.NonPublic | BindingFlags.Instance);
            Assert.NotNull(infosMethode);

            Partie partie = new Partie(new ReglesClassiques());

            infosMethode.Invoke(manageur, [partie]);

            FieldInfo? PartiePartieTermineeInfos = typeof(Partie)
                .GetField("PartiePartieTerminee", BindingFlags.Instance | BindingFlags.NonPublic);

            Assert.NotNull(PartiePartieTermineeInfos);

            EventHandler<PartiePartieTermineeEventArgs>? PartiePartieTerminee =
                PartiePartieTermineeInfos.GetValue(partie) as EventHandler<PartiePartieTermineeEventArgs>;

            PartiePartieTerminee =
                PartiePartieTermineeInfos.GetValue(partie) as EventHandler<PartiePartieTermineeEventArgs>;

            Assert.NotNull(PartiePartieTerminee);
            Assert.NotEmpty(PartiePartieTerminee.GetInvocationList());

            Joueur celeste = manageur.DemanderJoueur("Céleste");
            Robot robot = new Robot();

            Assert.Equal(0, celeste.Statistique(new ReglesClassiques(), Statistique.CoupMoyen));
            Assert.Equal(0, celeste.Statistique(new ReglesClassiques(), Statistique.PartieGagnee));
            Assert.Equal(0, celeste.Statistique(new ReglesClassiques(), Statistique.PartieEgalite));
            Assert.Equal(0, celeste.Statistique(new ReglesClassiques(), Statistique.PartiePerdue));

            int tour = 10;
            IReadOnlyList<string> gagnants = [];
            IReadOnlyList<string> perdants = [robot.Nom, celeste.Nom];

            PartiePartieTerminee.Invoke(null, new PartiePartieTermineeEventArgs(
                tour, gagnants, perdants
            ));

            Assert.Equal(tour, celeste.Statistique(new ReglesClassiques(), Statistique.CoupMoyen));
            Assert.Equal(0, celeste.Statistique(new ReglesClassiques(), Statistique.PartieGagnee));
            Assert.Equal(0, celeste.Statistique(new ReglesClassiques(), Statistique.PartieEgalite));
            Assert.Equal(1, celeste.Statistique(new ReglesClassiques(), Statistique.PartiePerdue));
        }

        /// <summary>
        /// Test de la methode DemanderJoueurExistant de Manageur.
        /// </summary>
        [Fact]
        public void TestDemanderJoueurExistant()
        {
            IPersistance persistance = new PersistanceStub();
            Manageur manageur = new Manageur(persistance);

            MethodInfo? infosMethode = typeof(Manageur).GetMethod("DemanderJoueurExistant", BindingFlags.NonPublic | BindingFlags.Instance);
            Assert.NotNull(infosMethode);

            object? celeste = infosMethode.Invoke(manageur, ["Céleste"]);
            Assert.NotNull(celeste);
            Assert.IsType<Joueur>(celeste);

            object? toto = infosMethode.Invoke(manageur, ["Toto"]);
            Assert.Null(toto);
        }

        /// <summary>
        /// Test de la methode DemanderJoueur de Manageur.
        /// </summary>
        [Fact]
        public void TestDemanderJoueur()
        {
            IPersistance persistance = new PersistanceStub();
            Manageur manageur = new Manageur(persistance);

            Joueur joueur = manageur.DemanderJoueur("Céleste");
            Joueur joueur2 = manageur.DemanderJoueur("Toto");

            Assert.Contains(joueur, manageur.Joueurs);
            Assert.Contains(joueur2, manageur.Joueurs);
        }
    }
}
