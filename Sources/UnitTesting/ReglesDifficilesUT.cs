﻿using CoreLibrary.Regles;
using Xunit;

namespace UnitTesting
{
    /// <summary>
    /// Classe de test pour la classe ReglesDifficiles.
    /// </summary>
    public class ReglesDifficilesUT
    {
        /// <summary>
        /// Test les attributs de ReglesDifficiles et verifie qu'ils soient egaux.
        /// </summary>
        [Fact]
        public void Test()
        {
            Assert.Equal(2, new ReglesDifficiles().Indice);
            Assert.NotNull(new ReglesDifficiles().Nom);
            Assert.NotNull(new ReglesDifficiles().Description);
            Assert.Equal(2, new ReglesDifficiles().NbJoueurs);
            Assert.Equal(12, new ReglesDifficiles().NbTour);
            Assert.Equal(6, new ReglesDifficiles().TailleCode);
            Assert.True(new ReglesDifficiles().Equals(new ReglesDifficiles()));
            new ReglesDifficiles().GetHashCode();
        }
    }
}
