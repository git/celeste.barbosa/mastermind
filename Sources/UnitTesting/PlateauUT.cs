﻿using CoreLibrary.Exceptions;
using System.Reflection;
using CoreLibrary.Core;
using Xunit;

namespace UnitTesting
{
    /// <summary>
    /// Classe test pour la classe Plateau.
    /// </summary>
    public class PlateauUT
    {
        /// <summary>
        /// Test que le constructeur de Plateau soit valide.
        /// </summary>
        [Fact]
        public void TestConstructeurValide()
        {
            Plateau plateau = new Plateau(4,12);
            Assert.NotNull(plateau);
            Assert.False(plateau.Victoire);
        }

        /// <summary>
        /// Test que le constructeur de Plateau soit invalide.
        /// </summary>
        [Fact]
        public void TestConstructeurInvalide()
        {
            Assert.Throws<TailleCodeException>(() => new Plateau(-1, 10));
            Assert.Throws<TailleGrilleException>(() => new Plateau(3, -1));
        }

        /// <summary>
        /// Test la methode EstComplet de Plateau et verifie qu'il renvoie True.
        /// </summary>
        [Fact]
        public void TestEstCompletTrue()
        {
            Plateau plateau = new Plateau(4, 3);
            Jeton[] jetons = [new Jeton(Couleur.Rouge), new Jeton(Couleur.Bleu), new Jeton(Couleur.Blanc), new Jeton(Couleur.Jaune)];
            Code code = new Code(4);
            code.AjouterJeton(jetons[0]);
            code.AjouterJeton(jetons[1]);
            code.AjouterJeton(jetons[2]);
            code.AjouterJeton(jetons[3]);
            plateau.AjouterCode(code);
            plateau.AjouterCode(code);
            plateau.AjouterCode(code);

            bool estComplet = plateau.Complet;

            Assert.True(estComplet);
        }

        /// <summary>
        /// Test la methode EstComplet de Plateau et verifie qu'il renvoie False.
        /// </summary>
        [Fact]
        public void TestEstCompletFalse()
        {
            Plateau plateau = new Plateau(4, 3);
            Jeton[] jetons = [new Jeton(Couleur.Rouge), new Jeton(Couleur.Bleu), new Jeton(Couleur.Blanc), new Jeton(Couleur.Jaune)];
            Code code = new Code(4);
            code.AjouterJeton(jetons[0]);
            code.AjouterJeton(jetons[1]);
            code.AjouterJeton(jetons[2]);
            code.AjouterJeton(jetons[3]);
            plateau.AjouterCode(code);
            plateau.AjouterCode(code);

            bool estComplet = plateau.Complet;

            Assert.False(estComplet);
        }

        /// <summary>
        /// Test l'exception CodeCompletException de Plateau avec une taille de code superieure a ce qui est defini.
        /// </summary>
        [Fact]
        public void TestAjouterCodeTailleIncorrecte()
        {
            Plateau plateau = new Plateau(4, 10);
            Jeton[] jetons = new Jeton[]
            {
                new Jeton(Couleur.Rouge),
                new Jeton(Couleur.Bleu),
                new Jeton(Couleur.Blanc),
                new Jeton(Couleur.Jaune)
            };
            Code code = new Code(4);
            code.AjouterJeton(jetons[0]);
            code.AjouterJeton(jetons[1]);
            code.AjouterJeton(jetons[2]);
            code.AjouterJeton(jetons[3]);

            Assert.Throws<CodeCompletException>(() => code.AjouterJeton(new Jeton(Couleur.Bleu)));

        }

        /// <summary>
        /// Test l'exception CodeIncompletException de Plateau avec un code incomplet.
        /// </summary>
        [Fact]
        public void TestAjouterCodeIncomplet()
        {
            Plateau plateau = new Plateau(4, 10);
            Code code = new Code(4);

            Assert.Throws<CodeIncompletException>(() => plateau.AjouterCode(code));
        }

        /// <summary>
        /// Test la methode AjouterCode de Plateau avec un bon code.
        /// </summary>
        [Fact]
        public void TestAjouterCodeBonCode()
        {
            Plateau plateau = new Plateau(4, 10);

            Type type = typeof(Plateau);

            FieldInfo? fieldInfo = type.GetField("codeSecret", BindingFlags.NonPublic | BindingFlags.Instance);
            Assert.NotNull(fieldInfo);

            Code? codeSecret = (Code?)fieldInfo.GetValue(plateau);

            Assert.NotNull(codeSecret);
            plateau.AjouterCode(codeSecret);
            Assert.True(plateau.Victoire);
        }

        /// <summary>
        /// Test la methode AjouterCode de Plateau et verifie la grille et ses composants.
        /// </summary>
        [Fact]
        public void TestGrilleAjouterCode()
        {
            Code code = new Code(4);
            Couleur[] couleurs = Enum.GetValues<Couleur>();
            Plateau plateau = new Plateau(4, 12);
            Jeton[] jetons = new Jeton[]
            {
                new Jeton(Couleur.Rouge),
                new Jeton(Couleur.Bleu),
                new Jeton(Couleur.Blanc),
                new Jeton(Couleur.Jaune)
            };
            code.AjouterJeton(jetons[0]);
            code.AjouterJeton(jetons[1]);
            code.AjouterJeton(jetons[2]);
            code.AjouterJeton(jetons[3]);

            plateau.AjouterCode(code);
            (IEnumerable<IEnumerable<Jeton>>, IEnumerable<IEnumerable<Indicateur>>) grille = plateau.Grille;

            var (jetonsGrille, indicateurs) = grille;

 
            Assert.Single(jetonsGrille);
            Assert.Equal(4, jetonsGrille.First().Count());

            Assert.Single(indicateurs);
        }

        /// <summary>
        /// Test que la grille du plateau est vide.
        /// </summary>
        [Fact]
        public void TestGrilleEstVide()
        {
            Plateau plateau = new Plateau(4, 12);
            (IEnumerable<IEnumerable<Jeton>>, IEnumerable<IEnumerable<Indicateur>>) grille = plateau.Grille;

            var (jetons, indicateurs) = grille;

            Assert.Empty(jetons);
            Assert.Empty(indicateurs);
        }

        /// <summary>
        /// Test l'exception GrilleCompleteException de Plateau.
        /// </summary>
        [Fact]
        public void TestAjouterCode_GrilleComplete_ThrowsGrilleCompleteException()
        {
            Plateau plateau = new Plateau(4, 2);
            Code codeComplet1 = new Code(4);
            Code codeComplet2 = new Code(4);
            Code codeComplet3 = new Code(4);
            Jeton[] jetons = new Jeton[]
            {
                new Jeton(Couleur.Rouge),
                new Jeton(Couleur.Bleu),
                new Jeton(Couleur.Blanc),
                new Jeton(Couleur.Jaune)
            };
            foreach (Jeton jeton in jetons)
            {
                codeComplet1.AjouterJeton(jeton);
                codeComplet2.AjouterJeton(jeton);
                codeComplet3.AjouterJeton(jeton);
            }

            plateau.AjouterCode(codeComplet1);
            plateau.AjouterCode(codeComplet2);

            Assert.Throws<GrilleCompleteException>(() => plateau.AjouterCode(codeComplet3));
        }

        /// <summary>
        /// Test l'evenement QuandPlateauAjouterCode de Plateau.
        /// </summary>
        [Fact]
        public void TestPlateauEcoute()
        {
            Plateau plateau = new Plateau(4, 2);

            MethodInfo? QuandPlateauAjouterCodeInfo = typeof(Plateau).GetMethod("QuandPlateauAjouterCode", BindingFlags.NonPublic | BindingFlags.Instance);
            
            Assert.NotNull(QuandPlateauAjouterCodeInfo);

            QuandPlateauAjouterCodeInfo?.Invoke(plateau, []);

            bool appel = false;
            plateau.PlateauAjouterCode += (sender, e) => appel = true;

            QuandPlateauAjouterCodeInfo?.Invoke(plateau, []);

            Assert.True(appel);
        }

        /// <summary>
        /// Test la methode AjouterCode de Plateau et verifie tous les cas d'arrets. 
        /// </summary>
        [Fact]
        public void TestAjouterCodeVictoire()
        {
            // Cas 1 : Victoire : false, code correct : false
            Code code = new Code(1);
            code.AjouterJeton(new Jeton(Couleur.Rouge));

            Plateau plateau;
            Code? codeSecret;
            do
            {
                plateau = new Plateau(1, 2);
                FieldInfo? codeSecretInfo = typeof(Plateau).GetField("codeSecret", BindingFlags.NonPublic | BindingFlags.Instance);
                Assert.NotNull(codeSecretInfo);
                codeSecret = codeSecretInfo.GetValue(plateau) as Code;

                Assert.NotNull(codeSecret);
            } while (codeSecret.Jetons.ElementAt(0).Equals(code.Jetons.ElementAt(0)));

            Assert.NotNull(codeSecret);

            plateau.AjouterCode(code);

            // Cas 2 : Victoire : false, code correct : true
            Plateau plateau2 = new Plateau(1, 2);
            FieldInfo? codeSecretInfo2 = typeof(Plateau).GetField("codeSecret", BindingFlags.NonPublic | BindingFlags.Instance);
            Assert.NotNull(codeSecretInfo2);
            Code? code2 = codeSecretInfo2.GetValue(plateau2) as Code;
            Assert.NotNull(code2);

            plateau2.AjouterCode(code2);

            // Cas 3 : Victoire : true, code correct : false
            Code code3 = new Code(1);
            code3.AjouterJeton(new Jeton(Couleur.Rouge));

            Plateau plateau3;
            Code? codeSecret3;
            do
            {
                plateau3 = new Plateau(1, 2);
                FieldInfo? codeSecretInfo3 = typeof(Plateau).GetField("codeSecret", BindingFlags.NonPublic | BindingFlags.Instance);
                Assert.NotNull(codeSecretInfo3);
                codeSecret3 = codeSecretInfo3.GetValue(plateau) as Code;

                Assert.NotNull(codeSecret3);
            } while (codeSecret3.Jetons.ElementAt(0).Equals(code3.Jetons.ElementAt(0)));

            Assert.NotNull(codeSecret3);

            PropertyInfo? VictoireInfo3 = typeof(Plateau).GetProperty("Victoire", BindingFlags.Public | BindingFlags.Instance);

            Assert.NotNull(VictoireInfo3);

            VictoireInfo3.SetValue(plateau3, true);

            plateau3.AjouterCode(code3);

            // Cas 4 : Victoire : true, code correct : true
            Plateau plateau4 = new Plateau(1, 2);
            FieldInfo? codeSecretInfo4 = typeof(Plateau).GetField("codeSecret", BindingFlags.NonPublic | BindingFlags.Instance);
            Assert.NotNull(codeSecretInfo4);
            Code? code4 = codeSecretInfo4.GetValue(plateau4) as Code;
            Assert.NotNull(code4);

            PropertyInfo? VictoireInfo4 = typeof(Plateau).GetProperty("Victoire", BindingFlags.Public | BindingFlags.Instance);
            Assert.NotNull(VictoireInfo4);
            VictoireInfo4.SetValue(plateau4, true);

            plateau4.AjouterCode(code4);
        }
    }
}
