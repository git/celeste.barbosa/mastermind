﻿using CoreLibrary.Exceptions;
using System.Reflection;
using System.Runtime.Serialization;
using Xunit;

namespace UnitTesting
{
    /// <summary>
    /// Classe de test pour l'exception NomJoueurInterditException.
    /// </summary>
    public class NomJoueurInterditExceptionUT
    {
        /// <summary>
        /// Test de l'exception NomJoueurInterditException par defaut.
        /// </summary>
        [Fact]
        public void ExceptionDefaut()
        {
            Assert.ThrowsAsync<NomJoueurInterditException>(() => throw new NomJoueurInterditException());
        }

        /// <summary>
        /// Test du message de l'exception NomJoueurInterditException.
        /// </summary>
        [Fact]
        public void ExceptionMessage()
        {
            string message = "Mon super gros problème.";

            Assert.ThrowsAsync<NomJoueurInterditException>(() => throw new NomJoueurInterditException(message));

            try
            {
                throw new NomJoueurInterditException(message);
            }
            catch (NomJoueurInterditException e)
            {
                Assert.Equal(message, e.Message);
            }
        }

        /// <summary>
        /// Test de l'exception NomJoueurInterditException et de ses messages.
        /// </summary>
        [Fact]
        public void ExceptionMessageEtException()
        {
            string message = "Mon super gros problème.";
            string message2 = "Pas de chance...";
            InvalidOperationException parent = new InvalidOperationException(message2);

            Assert.ThrowsAsync<NomJoueurInterditException>(() => throw new NomJoueurInterditException(message, parent));

            try
            {
                throw new NomJoueurInterditException(message, parent);
            }
            catch (NomJoueurInterditException e)
            {
                Assert.Equal(message, e.Message);
                Assert.NotNull(e.InnerException);
                Assert.IsType<InvalidOperationException>(e.InnerException);
                Assert.Equal(message2, e.InnerException.Message);
            }
        }

        /// <summary>
        /// Test de la serilisation de l'exception NomJoueurInterditException.
        /// </summary>
        [Fact]
        public void ExceptionSerialisation()
        {
            NomJoueurInterditException exception = new NomJoueurInterditException();

#pragma warning disable SYSLIB0050
            SerializationInfo info = new SerializationInfo(typeof(NomJoueurInterditException), new FormatterConverter());
            StreamingContext contexte = new StreamingContext(StreamingContextStates.All);
#pragma warning restore SYSLIB0050

#pragma warning disable SYSLIB0051
            exception.GetObjectData(info, contexte);
#pragma warning restore SYSLIB0051

            Assert.Equal(exception.Message, info.GetString("Message"));

#pragma warning disable SYSLIB0050
            NomJoueurInterditException exceptionSerialisee =
                (NomJoueurInterditException)FormatterServices.GetUninitializedObject(typeof(NomJoueurInterditException));
#pragma warning restore SYSLIB0050

            ConstructorInfo? constructeur = typeof(NomJoueurInterditException).GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, [typeof(SerializationInfo), typeof(StreamingContext)], null);
            Assert.NotNull(constructeur);
            constructeur.Invoke(exceptionSerialisee, [info, contexte]);

            Assert.Equal(exception.Message, exceptionSerialisee.Message);
        }
    }
}
