﻿using CoreLibrary.Exceptions;
using System.Reflection;
using System.Runtime.Serialization;
using Xunit;

namespace UnitTesting
{
    /// <summary>
    /// Classe de test de l'exception IndiceCodeException.
    /// </summary>
    public class IndiceCodeExceptionUT
    {
        /// <summary>
        /// Test de l'exception IndiceCodeException par defaut.
        /// </summary>
        [Fact]
        public void ExceptionDefaut()
        {
            Assert.ThrowsAsync<IndiceCodeException>(() => throw new IndiceCodeException());
        }

        /// <summary>
        /// Test des attributs de l'exception IndiceCodeException.
        /// </summary>
        [Fact]
        public void ExceptionAttributs()
        {
            Assert.ThrowsAsync<IndiceCodeException>(() => throw new IndiceCodeException(5, 3));

            try
            {
                throw new IndiceCodeException(5, 3);
            }
            catch (IndiceCodeException e)
            {
                Assert.Contains("5", e.Message);
                Assert.Contains("3", e.Message);
            }
        }

        /// <summary>
        /// Test du message de l'exception IndiceCodeException.
        /// </summary>
        [Fact]
        public void ExceptionMessage()
        {
            string message = "Mon super gros problème.";

            Assert.ThrowsAsync<IndiceCodeException>(() => throw new IndiceCodeException(message));

            try
            {
                throw new IndiceCodeException(message);
            }
            catch(IndiceCodeException e)
            {
                Assert.Equal(message, e.Message);
            }
        }

        /// <summary>
        /// Test de l'exception IndiceCodeException et de ses messages.
        /// </summary>
        [Fact]
        public void ExceptionMessageEtException()
        {
            string message = "Mon super gros problème.";
            string message2 = "Pas de chance...";
            InvalidOperationException parent = new InvalidOperationException(message2);

            Assert.ThrowsAsync<IndiceCodeException>(() => throw new IndiceCodeException(message, parent));

            try
            {
                throw new IndiceCodeException(message, parent);
            }
            catch (IndiceCodeException e)
            {
                Assert.Equal(message, e.Message);
                Assert.NotNull(e.InnerException);
                Assert.IsType<InvalidOperationException>(e.InnerException);
                Assert.Equal(message2, e.InnerException.Message);
            }
        }

        /// <summary>
        /// Test de la serialisation de l'evenement IndiceCodeException.
        /// </summary>
        [Fact]
        public void ExceptionSerialisation()
        {
            IndiceCodeException exception = new IndiceCodeException();

#pragma warning disable SYSLIB0050
            SerializationInfo info = new SerializationInfo(typeof(IndiceCodeException), new FormatterConverter());
            StreamingContext contexte = new StreamingContext(StreamingContextStates.All);
#pragma warning restore SYSLIB0050

#pragma warning disable SYSLIB0051
            exception.GetObjectData(info, contexte);
#pragma warning restore SYSLIB0051

            Assert.Equal(exception.Message, info.GetString("Message"));

#pragma warning disable SYSLIB0050
            IndiceCodeException exceptionSerialisee =
                (IndiceCodeException)FormatterServices.GetUninitializedObject(typeof(IndiceCodeException));
#pragma warning restore SYSLIB0050

            ConstructorInfo? constructeur = typeof(IndiceCodeException).GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, [typeof(SerializationInfo), typeof(StreamingContext)], null);
            Assert.NotNull(constructeur);
            constructeur.Invoke(exceptionSerialisee, [info, contexte]);

            Assert.Equal(exception.Message, exceptionSerialisee.Message);
        }
    }
}
