﻿using CoreLibrary.Exceptions;
using System.Reflection;
using System.Runtime.Serialization;
using Xunit;

namespace UnitTesting
{
    /// <summary>
    /// Classe de test de l'exception GrilleCompleteException.
    /// </summary>
    public class GrilleCompleteExceptionUT
    {
        /// <summary>
        /// Test de l'exception GrilleCompleteException par defaut.
        /// </summary>
        [Fact]
        public void ExceptionDefaut()
        {
            Assert.ThrowsAsync<GrilleCompleteException>(() => throw new GrilleCompleteException());
        }

        /// <summary>
        /// Test du message de l'exception GrilleCompleteException.
        /// </summary>
        [Fact]
        public void ExceptionMessage()
        {
            string message = "Mon super gros problème.";

            Assert.ThrowsAsync<GrilleCompleteException>(() => throw new GrilleCompleteException(message));

            try
            {
                throw new GrilleCompleteException(message);
            }
            catch(GrilleCompleteException e)
            {
                Assert.Equal(message, e.Message);
            }
        }

        /// <summary>
        /// Test de l'exception GrilleCompleteException et de ses messages.
        /// </summary>
        [Fact]
        public void ExceptionMessageEtException()
        {
            string message = "Mon super gros problème.";
            string message2 = "Pas de chance...";
            InvalidOperationException parent = new InvalidOperationException(message2);

            Assert.ThrowsAsync<GrilleCompleteException>(() => throw new GrilleCompleteException(message, parent));

            try
            {
                throw new GrilleCompleteException(message, parent);
            }
            catch (GrilleCompleteException e)
            {
                Assert.Equal(message, e.Message);
                Assert.NotNull(e.InnerException);
                Assert.IsType<InvalidOperationException>(e.InnerException);
                Assert.Equal(message2, e.InnerException.Message);
            }
        }

        /// <summary>
        /// Test de la serialisation de l'evenement GrilleCompleteException.
        /// </summary>
        [Fact]
        public void ExceptionSerialisation()
        {
            GrilleCompleteException exception = new GrilleCompleteException();

#pragma warning disable SYSLIB0050
            SerializationInfo info = new SerializationInfo(typeof(GrilleCompleteException), new FormatterConverter());
            StreamingContext contexte = new StreamingContext(StreamingContextStates.All);
#pragma warning restore SYSLIB0050

#pragma warning disable SYSLIB0051
            exception.GetObjectData(info, contexte);
#pragma warning restore SYSLIB0051

            Assert.Equal(exception.Message, info.GetString("Message"));

#pragma warning disable SYSLIB0050
            GrilleCompleteException exceptionSerialisee =
                (GrilleCompleteException)FormatterServices.GetUninitializedObject(typeof(GrilleCompleteException));
#pragma warning restore SYSLIB0050

            ConstructorInfo? constructeur = typeof(GrilleCompleteException).GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, [typeof(SerializationInfo), typeof(StreamingContext)], null);
            Assert.NotNull(constructeur);
            constructeur.Invoke(exceptionSerialisee, [info, contexte]);

            Assert.Equal(exception.Message, exceptionSerialisee.Message);
        }
    }
}
