﻿using Xunit;
using CoreLibrary.Core;
using CoreLibrary.Evenements;

namespace UnitTesting
{
    /// <summary>
    /// Classe de test de l'evenement JoueurJouerEventArgs.
    /// </summary>
    public class JoueurJouerEventArgsUT
    {
        /// <summary>
        /// Test du constructeur de l'evenement JoueurJouerEventArgs valide.
        /// </summary>
        [Fact]
        public void TestConstructeurValide()
        {
            int tour = 0;
            string nom = "nom";
            Plateau plateau = new Plateau(3,4);
            Code code = new Code(3);
            bool estJoueur = true;


            PartieDemanderJoueurJouerEventArgs evenement = new PartieDemanderJoueurJouerEventArgs(tour, nom, plateau, code, estJoueur);

            Assert.Equal(tour, evenement.Tour);
            Assert.Equal(nom, evenement.Nom);
            Assert.Equal(plateau, evenement.Plateau);
            Assert.Equal(code, evenement.Code);
            Assert.Equal(estJoueur, evenement.EstJoueur);
            
        }
    }
}
