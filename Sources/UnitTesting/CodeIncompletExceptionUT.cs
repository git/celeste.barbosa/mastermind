﻿using CoreLibrary.Exceptions;
using System.Reflection;
using System.Runtime.Serialization;
using Xunit;

namespace UnitTesting
{
    /// <summary>
    /// Classe de test de l'exception CodeIncompletException.
    /// </summary>
    public class CodeIncompletExceptionUT
    {
        /// <summary>
        /// Test de l'exception CodeIncompletException par defaut.
        /// </summary>
        [Fact]
        public void ExceptionDefaut()
        {
            Assert.ThrowsAsync<CodeIncompletException>(() => throw new CodeIncompletException());
        }

        /// <summary>
        /// Test du message de l'exception CodeIncompletException.
        /// </summary>
        [Fact]
        public void ExceptionMessage()
        {
            string message = "Mon super gros problème.";

            Assert.ThrowsAsync<CodeIncompletException>(() => throw new CodeIncompletException(message));

            try
            {
                throw new CodeIncompletException(message);
            }
            catch(CodeIncompletException e)
            {
                Assert.Equal(message, e.Message);
            }
        }

        /// <summary>
        /// Test de l'exception CodeIncompletException et de ses messages.
        /// </summary>
        [Fact]
        public void ExceptionMessageEtException()
        {
            string message = "Mon super gros problème.";
            string message2 = "Pas de chance...";
            InvalidOperationException parent = new InvalidOperationException(message2);

            Assert.ThrowsAsync<CodeIncompletException>(() => throw new CodeIncompletException(message, parent));

            try
            {
                throw new CodeIncompletException(message, parent);
            }
            catch (CodeIncompletException e)
            {
                Assert.Equal(message, e.Message);
                Assert.NotNull(e.InnerException);
                Assert.IsType<InvalidOperationException>(e.InnerException);
                Assert.Equal(message2, e.InnerException.Message);
            }
        }

        /// <summary>
        /// Test de la serialisation de l'exception CodeIncompletException.
        /// </summary>
        [Fact]
        public void ExceptionSerialisation()
        {
            CodeIncompletException exception = new CodeIncompletException();

#pragma warning disable SYSLIB0050
            SerializationInfo info = new SerializationInfo(typeof(CodeIncompletException), new FormatterConverter());
            StreamingContext contexte = new StreamingContext(StreamingContextStates.All);
#pragma warning restore SYSLIB0050

#pragma warning disable SYSLIB0051
            exception.GetObjectData(info, contexte);
#pragma warning restore SYSLIB0051

            Assert.Equal(exception.Message, info.GetString("Message"));

#pragma warning disable SYSLIB0050
            CodeIncompletException exceptionSerialisee =
                (CodeIncompletException)FormatterServices.GetUninitializedObject(typeof(CodeIncompletException));
#pragma warning restore SYSLIB0050

            ConstructorInfo? constructeur = typeof(CodeIncompletException).GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, [typeof(SerializationInfo), typeof(StreamingContext)], null);
            Assert.NotNull(constructeur);
            constructeur.Invoke(exceptionSerialisee, [info, contexte]);

            Assert.Equal(exception.Message, exceptionSerialisee.Message);
        }
    }
}
