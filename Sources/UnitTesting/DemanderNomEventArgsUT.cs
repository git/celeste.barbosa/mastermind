﻿using CoreLibrary.Evenements;
using CoreLibrary.Joueurs;
using Xunit;

namespace UnitTesting
{
    /// <summary>
    /// Classe de test de l'evenement DemanderNomEventArgs.
    /// </summary>
    public class DemanderNomEventArgsUT
    {
        /// <summary>
        /// Test du constructeur de l'evenement DemanderNomEventArgs valide.
        /// </summary>
        [Fact]
        public void TestConstructeurValide()
        {
            Joueur joueur = new Joueur();
            int indice = 15;

            PartieDemanderJoueurEventArgs evenement = new PartieDemanderJoueurEventArgs(indice, joueur);

            Assert.Equal(joueur, evenement.JoueurDemande);
            Assert.Equal(indice, evenement.Indice);
        }

    }
}
