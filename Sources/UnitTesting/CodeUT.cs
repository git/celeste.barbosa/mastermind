﻿using CoreLibrary.Core;
using CoreLibrary.Exceptions;
using System.Collections.ObjectModel;
using Xunit;

namespace UnitTesting
{
    /// <summary>
    /// Classe de test pour la classe Code.
    /// </summary>
    public class CodeUT
    {
        /// <summary>
        /// Test du constructeur de Code valide.
        /// </summary>
        [Fact]
        public void TestConstructeurValide()
        {
            Code code = new Code(4);
            Assert.NotNull(code);
            Assert.Empty(code.Jetons);
            Assert.Equal(0, code.Taille);
            Assert.False(code.Complet);
            Assert.True(code.Vide);
        }

        /// <summary>
        /// Test du constructeur de Code invalide.
        /// </summary>
        [Fact]
        public void TestConstructeurInvalide()
        {
            Assert.Throws<TailleCodeException>(() => new Code(-1));
        }

        /// <summary>
        /// Test de la methode AjouterJeton valide.
        /// </summary>
        [Fact]
        public void TestAjouterJetonValide()
        {
            Jeton jeton = new Jeton(Couleur.Jaune);
            Code code = new Code(3);
            code.AjouterJeton(jeton);
            Assert.Equal(1, code.Taille);
            Assert.Equal(jeton, code.Jetons.ElementAt(0));
        }

        /// <summary>
        /// Test de la methode AjouterJeton invalide.
        /// </summary>
        [Fact]
        public void TestAjouterJetonInvalide()
        {
            Jeton jeton = new Jeton(Couleur.Jaune);
            Code code = new Code(3);
            code.AjouterJeton(jeton);
            code.AjouterJeton(jeton);
            code.AjouterJeton(jeton);
            Assert.Throws<CodeCompletException>(() => code.AjouterJeton(new Jeton(Couleur.Rouge)));
        }

        /// <summary>
        /// Test de la methode SupprimerDernierJeton valide.
        /// </summary>
        [Fact]
        public void TestSupprimerDernierJetonValide()
        {
            Jeton jeton = new Jeton(Couleur.Rouge);
            Code code = new Code(3);
            code.AjouterJeton(jeton);
            code.AjouterJeton(jeton);
            code.AjouterJeton(jeton);
            code.SupprimerDernierJeton();
            Assert.Equal(2, code.Taille);
        }

        /// <summary>
        /// Test de la methode SupprimerDernierJeton invalide.
        /// </summary>
        [Fact]
        public void TestSupprimerDernierJetonInvalide()
        {
            Code code = new Code(4);
            Assert.Throws<CodeVideException>(() => code.SupprimerDernierJeton());
        }

        /// <summary>
        /// Test de la methode RecupereJeton valide.
        /// </summary>
        [Fact]
        public void TestRecupererJetonValide()
        {
            Jeton jeton1 = new Jeton(Couleur.Rouge);
            Jeton jeton2 = new Jeton(Couleur.Bleu);
            Code code = new Code(3);
            code.AjouterJeton(jeton1);
            code.AjouterJeton(jeton2);
            Jeton jetonAttendu = new Jeton(Couleur.Bleu);
            Jeton jeton = code.RecupererJeton(1);
            Assert.Equal(jetonAttendu.Couleur, jeton.Couleur);
        }

        /// <summary>
        /// Test de la methode RecupereJeton invalide.
        /// </summary>
        [Fact]
        public void TestRecupererJetonInvalide()
        {
            Code code = new Code(4);
            Assert.Throws<IndiceCodeException>(() => code.RecupererJeton(-1));
            Assert.Throws<IndiceCodeException>(() => code.RecupererJeton(4));
        }

        /// <summary>
        /// Test de la methode RecupererJeton avec exception.
        /// </summary>
        [Fact]
        public void TestRecupererJetonNull()
        {
            Code code = new Code(4);
            Assert.Throws<IndiceCodeException>(() => code.RecupererJeton(1));
        }

        /// <summary>
        /// Test que les jetons soient les memes que ceux choisit.
        /// </summary>
        [Fact]
        public void TestJetonsValide()
        {
            Jeton[] jetonsAttendus = [new Jeton(Couleur.Rouge), new Jeton(Couleur.Bleu), new Jeton(Couleur.Bleu)];
            Code code = new Code(3);
            code.AjouterJeton(jetonsAttendus[0]);
            code.AjouterJeton(jetonsAttendus[1]);
            code.AjouterJeton(jetonsAttendus[2]);
            ObservableCollection<Jeton> lesJetons = code.Jetons;

            Assert.Equal(jetonsAttendus.Length, lesJetons.Count);

            int index = 0;
            foreach (Jeton jetonAttendu in jetonsAttendus)
            {
                Assert.Equal(jetonAttendu.Couleur, lesJetons.ElementAt(index).Couleur);
                index++;
            }
        }

        /// <summary>
        /// Test de la methode EstComplet valide.
        /// </summary>
        [Fact]
        public void TestEstCompletValide()
        {
            Jeton[] jetons = [new Jeton(Couleur.Rouge), new Jeton(Couleur.Bleu), new Jeton(Couleur.Blanc)];
            Code code = new Code(3);
            code.AjouterJeton(jetons[0]);
            code.AjouterJeton(jetons[1]);
            code.AjouterJeton(jetons[2]);
            bool estComplet = code.Complet;
            Assert.True(estComplet);
        }

        /// <summary>
        /// Test de la methode EstComplet invalide.
        /// </summary>
        [Fact]
        public void TestEstCompletInvalide()
        {
            Code code = new Code(3);
            bool estComplet = code.Complet;
            Assert.False(estComplet);
        }

        /// <summary>
        /// Test de la methode TailleMaximale valide.
        /// </summary>
        [Fact]
        public void TestTailleMaximaleValide()
        {
            Jeton[] jetons = [new Jeton(Couleur.Rouge), new Jeton(Couleur.Bleu), new Jeton(Couleur.Bleu)];
            Code code = new Code(3);
            code.AjouterJeton(jetons[0]);
            code.AjouterJeton(jetons[1]);
            code.AjouterJeton(jetons[2]);
            int tailleMaximale = code.TailleMax;

            Assert.Equal(jetons.Length, tailleMaximale);
        }

        /// <summary>
        /// Test de la methode Comparer valide.
        /// </summary>
        [Fact]
        public void TestComparerValide()
        {
            Jeton[] jetons1 = [new Jeton(Couleur.Rouge), new Jeton(Couleur.Bleu), new Jeton(Couleur.Blanc)];
            Code code1 = new Code(3);
            code1.AjouterJeton(jetons1[0]);
            code1.AjouterJeton(jetons1[1]);
            code1.AjouterJeton(jetons1[2]);

            Jeton[] jetons2 = [new Jeton(Couleur.Rouge), new Jeton(Couleur.Bleu), new Jeton(Couleur.Blanc)];
            Code code2 = new Code(3);
            code2.AjouterJeton(jetons2[0]);
            code2.AjouterJeton(jetons2[1]);
            code2.AjouterJeton(jetons2[2]);

            IReadOnlyList<Indicateur> indicateurs = code1.Comparer(code2);

            Assert.Equal(3, indicateurs.Count);
        }

        /// <summary>
        /// Test de la methode Comparer ou le resultat est different.
        /// </summary>
        [Fact]
        public void TestComparerDifferent()
        {
            Jeton[] jetons1 = [new Jeton(Couleur.Noir), new Jeton(Couleur.Jaune), new Jeton(Couleur.Vert)];
            Code code1 = new Code(3);
            code1.AjouterJeton(jetons1[0]);
            code1.AjouterJeton(jetons1[1]);
            code1.AjouterJeton(jetons1[2]);

            Jeton[] jetons2 = [new Jeton(Couleur.Rouge), new Jeton(Couleur.Bleu), new Jeton(Couleur.Blanc)];
            Code code2 = new Code(3);
            code2.AjouterJeton(jetons2[0]);
            code2.AjouterJeton(jetons2[1]);
            code2.AjouterJeton(jetons2[2]);

            IReadOnlyList<Indicateur> indicateurs = code1.Comparer(code2);

            Assert.Empty(indicateurs);
        }

        /// <summary>
        /// Test de la methode Comparer ou le mon code est incomplet.
        /// </summary>
        [Fact]
        public void TestComparerMonCodeIncomplet()
        {
            Code code1 = new Code(3);

            Jeton[] jetons2 = [new Jeton(Couleur.Rouge), new Jeton(Couleur.Bleu), new Jeton(Couleur.Blanc)];
            Code code2 = new Code(3);
            code2.AjouterJeton(jetons2[0]);
            code2.AjouterJeton(jetons2[1]);
            code2.AjouterJeton(jetons2[2]);

            IEnumerable<Indicateur> indicateurs = code1.Comparer(code2);

            Assert.Empty(indicateurs);
        }

        /// <summary>
        /// Test de la methode Comparer ou le son code est incomplet.
        /// </summary>
        [Fact]
        public void TestComparerSonCodeIncomplet()
        {
            Jeton[] jetons1 = [new Jeton(Couleur.Noir), new Jeton(Couleur.Jaune), new Jeton(Couleur.Vert)];
            Code code1 = new Code(3);
            code1.AjouterJeton(jetons1[0]);
            code1.AjouterJeton(jetons1[1]);
            code1.AjouterJeton(jetons1[2]);

            Code code2 = new Code(3);

            Assert.Throws<CodeIncompletException>(() => code1.Comparer(code2));
        }

        /// <summary>
        /// Test de la methode ToString de la classe Code.
        /// </summary>
        [Fact]
        public void TestToString()
        {
            Code code = new Code(4);

            Assert.Equal("Code(0)", code.ToString());

            code.AjouterJeton(new Jeton());

            Assert.Equal("Code(1)", code.ToString());
        }

        /// <summary>
        /// Test de la methode Comparer ou le code est invalide.
        /// </summary>
        [Fact]
        public void TestComparerCodeInvalide()
        {
            Code code = new Code(1);
            code.AjouterJeton(new Jeton());
            Code code1 = new Code(2);
            code1.AjouterJeton(new Jeton());
            code1.AjouterJeton(new Jeton());

            Assert.Throws<CodeInvalideException>(() => code.Comparer(code1));
        }

        /// <summary>
        /// Test de la methode Comparer ou le code est correct.
        /// </summary>
        [Fact]
        public void TestComparerCodeCorrect()
        {
            Code code = new Code(1);
            code.AjouterJeton(new Jeton());
            
            IReadOnlyList<Indicateur> indicateurs = code.Comparer(code);
            Assert.NotNull(indicateurs);
            Assert.Equal(1, indicateurs.Count);
            Assert.Equal(Indicateur.BonnePlace, indicateurs.ElementAt(0));
        }
    }

}
