﻿using CoreLibrary.Exceptions;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.Json;
using Xunit;

namespace UnitTesting
{
    /// <summary>
    /// Classe de test de l'exception CodeCompletException.
    /// </summary>
    public class CodeCompletExceptionUT
    {
        /// <summary>
        /// Test de l'exception CodeCompletException par defaut.
        /// </summary>
        [Fact]
        public void ExceptionDefaut()
        {
            Assert.ThrowsAsync<CodeCompletException>(() => throw new CodeCompletException());
        }

        /// <summary>
        /// Test du message de l'exception CodeCompletException.
        /// </summary>
        [Fact]
        public void ExceptionMessage()
        {
            string message = "Mon super gros problème.";

            Assert.ThrowsAsync<CodeCompletException>(() => throw new CodeCompletException(message));

            try
            {
                throw new CodeCompletException(message);
            }
            catch (CodeCompletException e)
            {
                Assert.Equal(message, e.Message);
            }
        }

        /// <summary>
        /// Test de l'exception CodeCompletException et de ses messages.
        /// </summary>
        [Fact]
        public void ExceptionMessageEtException()
        {
            string message = "Mon super gros problème.";
            string message2 = "Pas de chance...";
            InvalidOperationException parent = new InvalidOperationException(message2);

            Assert.ThrowsAsync<CodeCompletException>(() => throw new CodeCompletException(message, parent));

            try
            {
                throw new CodeCompletException(message, parent);
            }
            catch (CodeCompletException e)
            {
                Assert.Equal(message, e.Message);
                Assert.NotNull(e.InnerException);
                Assert.IsType<InvalidOperationException>(e.InnerException);
                Assert.Equal(message2, e.InnerException.Message);
            }
        }

        /// <summary>
        /// Test de la serialisation de l'exception CodeCompletException.
        /// </summary>
        [Fact]
        public void ExceptionSerialisation()
        {
            CodeCompletException exception = new CodeCompletException();

#pragma warning disable SYSLIB0050
            SerializationInfo info = new SerializationInfo(typeof(CodeCompletException), new FormatterConverter());
            StreamingContext contexte = new StreamingContext(StreamingContextStates.All);
#pragma warning restore SYSLIB0050

#pragma warning disable SYSLIB0051
            exception.GetObjectData(info, contexte);
#pragma warning restore SYSLIB0051

            Assert.Equal(exception.Message, info.GetString("Message"));

#pragma warning disable SYSLIB0050
            CodeCompletException exceptionSerialisee = 
                (CodeCompletException) FormatterServices.GetUninitializedObject(typeof(CodeCompletException));
#pragma warning restore SYSLIB0050

            ConstructorInfo? constructeur = typeof(CodeCompletException).GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, [typeof(SerializationInfo), typeof(StreamingContext)], null);
            Assert.NotNull(constructeur);
            constructeur.Invoke(exceptionSerialisee, [info, contexte]);

            Assert.Equal(exception.Message, exceptionSerialisee.Message);
        }
    }
}
