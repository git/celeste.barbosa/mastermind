﻿using CoreLibrary.Core;
using CoreLibrary.Evenements;
using CoreLibrary.Joueurs;
using Xunit;

namespace UnitTesting
{
    /// <summary>
    /// Classe de test de l'evenement PartieNouveauTourEventArgs.
    /// </summary>
    public class PartieNouveauTourEventArgsUT
    {
        /// <summary>
        /// Test du constructeur de l'evenement valide.
        /// </summary>
        [Fact]
        public void TestConstructeurValide()
        {
            // Créer des objets nécessaires pour le test
            int tour = 10;
            string nom = "Céleste";
            Plateau plateau = new Plateau(4, 12);
            Code code = new Code(4);
            bool estJoueur = true;

            // Créer un nouvel événement en utilisant le constructeur
            PartieNouveauTourEventArgs evenement = new PartieNouveauTourEventArgs(tour, nom, plateau, code, estJoueur);

            // Vérifier que les propriétés ont été initialisées correctement
            Assert.Equal(tour, evenement.Tour);
            Assert.Equal(nom, evenement.Nom);
            Assert.Equal(plateau, evenement.Plateau);
            Assert.Equal(code, evenement.Code);
            Assert.Equal(estJoueur, evenement.EstJoueur);
        }
    }
}

