﻿using CoreLibrary.Core;
using CoreLibrary.Evenements;
using Xunit;

namespace UnitTesting
{
    /// <summary>
    /// Classe de test de l'evenement PartieDemanderJoueurJouerEventArgs.
    /// </summary>
    public class PartieDemanderJoueurJouerEventArgsUT
    {
        /// <summary>
        /// Test du constructeur de l'evenement valide.
        /// </summary>
        [Fact]
        public void TestConstructeurValide()
        {
            int tour = 2;
            string nom = "MonJoueur";
            Plateau plateau = new Plateau(3, 4);
            Code code = new Code(4);
            bool estJoueur = true;

            PartieDemanderJoueurJouerEventArgs evenement = new PartieDemanderJoueurJouerEventArgs(tour, nom, plateau, code, estJoueur);
            Assert.Equal(tour, evenement.Tour);
            Assert.Equal(nom, evenement.Nom);
            Assert.Equal(plateau, evenement.Plateau);
            Assert.Equal(code, evenement.Code);
            Assert.Equal(estJoueur, evenement.EstJoueur);
        }
    }
}
