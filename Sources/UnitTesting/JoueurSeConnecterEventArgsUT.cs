﻿using CoreLibrary.Joueurs;
using CoreLibrary.Evenements;
using Xunit;

namespace UnitTesting
{
    /// <summary>
    /// Classe de test de l'evenement JoueurSeConnecterEventArgs.
    /// </summary>
    public class JoueurSeConnecterEventArgsUT
    {
        /// <summary>
        /// Test du constructeur de l'evenement valide.
        /// </summary>
        [Fact]
        public void TestConstructeurValide()
        {
            Joueur joueur = new Joueur();

            JoueurSeConnecterEventArgs evenement = new JoueurSeConnecterEventArgs(joueur);

            Assert.Equal(joueur, evenement.Joueur);
        }
    }
}
