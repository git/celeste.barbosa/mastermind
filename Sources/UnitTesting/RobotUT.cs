﻿using CoreLibrary.Core;
using CoreLibrary.Joueurs;
using System.Reflection;
using Xunit;

namespace UnitTesting
{
    /// <summary>
    /// Classe de test pour la classe Robot.
    /// </summary>
    public class RobotUT
    {
        /// <summary>
        /// Test la methode EstCodePossible de la classe Robot.
        /// </summary>
        [Fact]
        public void TestEstCodePossible()
        {
            // Cas 1 - Plateau vide
            Plateau plateau1 = new Plateau(1, 1);
            Code code1 = new Code(1);
            code1.AjouterJeton(new Jeton(Couleur.Rouge));

            MethodInfo? EstCodePossible1 = typeof(Robot).GetMethod("EstCodePossible", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
            Assert.NotNull(EstCodePossible1);
            object? estPossibleObj1 = EstCodePossible1.Invoke(new Robot(), [plateau1, code1]);

            Assert.NotNull(estPossibleObj1);
            Assert.IsType<bool>(estPossibleObj1);
            Assert.True((bool)estPossibleObj1);

            // Cas 2 - Plateau rempli, code juste
            Plateau plateau2 = new Plateau(1, 1);
            
            FieldInfo? codeSecretInfo2 = typeof(Plateau).GetField("codeSecret", BindingFlags.NonPublic | BindingFlags.Instance);
            Assert.NotNull(codeSecretInfo2);

            Code? codeSecret2 = codeSecretInfo2.GetValue(plateau2) as Code;
            Assert.NotNull(codeSecret2);

            plateau2.AjouterCode(codeSecret2);

            MethodInfo? EstCodePossible2 = typeof(Robot).GetMethod("EstCodePossible", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
            Assert.NotNull(EstCodePossible2);
            object? estPossibleObj2 = EstCodePossible2.Invoke(new Robot(), [plateau2, codeSecret2]);
            Assert.NotNull(estPossibleObj2);
            Assert.IsType<bool>(estPossibleObj2);
            Assert.True((bool)estPossibleObj2);

            // Cas 3 - Plateau rempli, code mauvaise couleur
            Code code3 = new Code(1);
            code3.AjouterJeton(new Jeton(Couleur.Rouge));

            Plateau plateau3;
            Code? codeSecret3;
            do
            {
                plateau3 = new Plateau(1, 1);

                FieldInfo? codeSecretInfo3 = typeof(Plateau).GetField("codeSecret", BindingFlags.NonPublic | BindingFlags.Instance);
                Assert.NotNull(codeSecretInfo3);

                codeSecret3 = codeSecretInfo3.GetValue(plateau3) as Code;
                Assert.NotNull(codeSecret3);
            } while (codeSecret3.RecupererJeton(0).Equals(code3.RecupererJeton(0)));

            plateau3.AjouterCode(codeSecret3);

            MethodInfo? EstCodePossible3 = typeof(Robot).GetMethod("EstCodePossible", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
            Assert.NotNull(EstCodePossible3);
            object? estPossibleObj3 = EstCodePossible3.Invoke(new Robot(), [plateau3, code3]);
            Assert.NotNull(estPossibleObj3);
            Assert.IsType<bool>(estPossibleObj3);
            Assert.False((bool)estPossibleObj3);
        }

        /// <summary>
        /// Test la methode SupprimerCodesImpossibles de la classe Robot.
        /// </summary>
        [Fact]
        public void TestSupprimerCodesImpossibles()
        {
            Code codeSecret = new Code(1);
            codeSecret.AjouterJeton(new Jeton(Couleur.Rouge));

            Code code1 = new Code(1);
            code1.AjouterJeton(new Jeton(Couleur.Vert));

            Code code2 = new Code(1);
            code2.AjouterJeton(new Jeton(Couleur.Jaune));

            List<Code> codes = new List<Code>([
                  codeSecret,
                  code1,
                  code2,
            ]);

            Plateau plateau = new Plateau(1, 1);

            FieldInfo? codeSecretInfo = typeof(Plateau).GetField("codeSecret", BindingFlags.NonPublic | BindingFlags.Instance);
            Assert.NotNull(codeSecretInfo);
            codeSecretInfo.SetValue(plateau, codeSecret);

            plateau.AjouterCode(codeSecret);

            Assert.Equal(3, codes.Count);

            MethodInfo? SupprimerCodesImpossiblesInfo = typeof(Robot).GetMethod("SupprimerCodesImpossibles", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
            Assert.NotNull(SupprimerCodesImpossiblesInfo);
            SupprimerCodesImpossiblesInfo.Invoke(new Robot(), [codes, plateau]);

            Assert.Single(codes);
        }
    }
}
