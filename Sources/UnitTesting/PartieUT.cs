﻿using System.Reflection;
using Xunit;
using CoreLibrary;
using CoreLibrary.Core;
using CoreLibrary.Joueurs;
using CoreLibrary.Regles;
using CoreLibrary.Evenements;
using CoreLibrary.Exceptions;


namespace UnitTesting
{
    /// <summary>
    /// Classe test pour la classe Partie.
    /// </summary>
    public class PartieUT
    {
        /// <summary>
        /// Test le premier constructeur de Partie valide.
        /// </summary>
        [Fact]
        public void TestPremierConstructeurValide()
        {
            IRegles regle = new ReglesClassiques();

            Partie partie = new Partie(regle);
            Assert.Equal(regle, partie.Regles);
        }

        /// <summary>
        /// Test le deuxieme constructeur de Partie valide.
        /// </summary>
        [Fact]
        public void TestSecondConstructeurValide()
        {

            IRegles regles = new ReglesClassiques();
            Partie partieOriginale = new Partie(regles);


            FieldInfo? joueursField = typeof(Partie).GetField("joueurs", BindingFlags.NonPublic | BindingFlags.Instance);
            FieldInfo? plateauxField = typeof(Partie).GetField("plateaux", BindingFlags.NonPublic | BindingFlags.Instance);
            FieldInfo? courantField = typeof(Partie).GetField("courant", BindingFlags.NonPublic | BindingFlags.Instance);
            PropertyInfo? tourField = typeof(Partie).GetProperty("Tour", BindingFlags.Public | BindingFlags.Instance);
            PropertyInfo? termineField = typeof(Partie).GetProperty("Termine", BindingFlags.Public | BindingFlags.Instance);


            if (joueursField != null && plateauxField != null)
            {
                Dictionary<string, bool>? joueurs = joueursField.GetValue(partieOriginale) as Dictionary<string, bool>;
                if (joueurs != null)
                {
                    joueurs.Add("Joueur1", false);
                    joueurs.Add("Joueur2", true);
                }

                List<Plateau>? plateaux = plateauxField.GetValue(partieOriginale) as List<Plateau>;
                if (plateaux != null)
                {
                    plateaux.Add(new Plateau(4, 10));
                    plateaux.Add(new Plateau(4, 10));
                }



                if (tourField != null)
                    tourField.SetValue(partieOriginale, 5);

                if (termineField != null)
                    termineField.SetValue(partieOriginale, false);


                var partieCopiee = new Partie(partieOriginale);


                Dictionary<string, bool>? joueursCopie = joueursField.GetValue(partieCopiee) as Dictionary<string, bool>;
                List<Plateau>? plateauxCopie = plateauxField.GetValue(partieCopiee) as List<Plateau>;

                if (courantField != null && tourField != null && termineField != null)
                {
                    int? courantCopie = courantField.GetValue(partieCopiee) as int?;
                    int? tourCopie = tourField.GetValue(partieCopiee) as int?;
                    bool? termineCopie = termineField.GetValue(partieCopiee) as bool?;


                    if (joueursCopie != null && plateauxCopie != null && courantCopie != null && tourCopie != null && termineCopie != null && joueurs != null && plateaux != null)
                    {
                        Assert.Equal(joueurs.Count, joueursCopie.Count);
                        Assert.Equal(plateaux.Count, plateauxCopie.Count);
                        Assert.Equal(5, tourCopie.Value);
                        Assert.False(termineCopie.Value);
                        Assert.Equal(regles, partieCopiee.Regles);
                    }



                    if (joueurs != null && joueursCopie != null)
                    {
                        foreach (string joueur in joueurs.Keys)
                        {
                            Assert.True(joueursCopie.ContainsKey(joueur));
                            Assert.Equal(joueurs[joueur], joueursCopie[joueur]);
                        }
                    }

                    if (plateaux != null && plateauxCopie != null)
                    {
                        foreach (Plateau plateau in plateaux)
                        {
                            Assert.Contains(plateau, plateauxCopie);
                        }
                    }

                }

            }
        }

        /// <summary>
        /// Test la methode JouerDemanderJoueur de Partie.
        /// </summary>
        [Fact]
        public void TestJouerDemanderJouer()
        {
            IRegles regle = new ReglesClassiques();
            Partie partie = new Partie(regle);
            FieldInfo? joueursField = typeof(Partie).GetField("joueurs", BindingFlags.NonPublic | BindingFlags.Instance);

            if (joueursField != null)
            {
                Dictionary<string, bool>? joueurs = joueursField.GetValue(partie) as Dictionary<string, bool>;
                if (joueurs != null)
                {
                    joueurs.Add("Joueur1", false);
                }

                bool demanderJoueurCalled = false;
                partie.PartieDemanderJoueur += (sender, e) => demanderJoueurCalled = true;
                partie.Jouer();
                Assert.True(demanderJoueurCalled);
            }
        }

        /// <summary>
        /// Test la methode JouerDebutPartie de Partie.
        /// </summary>
        [Fact]
        public void TestJouerDebutPartie()
        {
            IRegles regle = new ReglesClassiques();
            Partie partie = new Partie(regle);
            FieldInfo? joueursField = typeof(Partie).GetField("joueurs", BindingFlags.NonPublic | BindingFlags.Instance);

            if (joueursField != null)
            {
                Dictionary<string, bool>? joueurs = joueursField.GetValue(partie) as Dictionary<string, bool>;
                if (joueurs != null)
                {
                    joueurs.Add("Joueur1", false);
                    joueurs.Add("Joueur2", false);
                }

                bool debutPartieCalled = false;
                partie.PartieDebutPartie += (sender, e) => debutPartieCalled = true;
                partie.Jouer();
                Assert.True(debutPartieCalled);
            }
        }

        /// <summary>
        /// Test la methode DemanderJoueur de Partie.
        /// </summary>
        [Fact]
        public void TestDemanderJoueur()
        {
            IRegles regle = new ReglesClassiques();
            Partie partie = new Partie(regle);

            Joueur? joueurDemande = null;

            partie.PartieDemanderJoueur += (sender, e) =>
            {
                joueurDemande = e.JoueurDemande;
            };

            MethodInfo? methodInfo = typeof(Partie).GetMethod("DemanderJoueur", BindingFlags.NonPublic | BindingFlags.Instance);
            methodInfo?.Invoke(partie, null);

            Assert.NotNull(joueurDemande);
            bool joueurConnecteCalled = false;
            if (joueurDemande != null)
            {
                joueurDemande.JoueurSeConnecter += (sender, e) => joueurConnecteCalled = true;
                joueurDemande.SeConnecter(joueurDemande);
                Assert.True(joueurConnecteCalled);
            }
        }

        /// <summary>
        /// Test l'evenement JoueurSeConnecterEventArgs de Partie.
        /// </summary>
        [Fact]
        public void TestJoueurConnecte()
        {
            IRegles regle = new ReglesClassiques();
            Partie partie = new Partie(regle);

            bool demanderJoueurAppelée = false;
            partie.PartieDemanderJoueur += (sender, e) => demanderJoueurAppelée = true;

            Joueur? joueur1 = new Joueur("Joueur1");
            JoueurSeConnecterEventArgs joueur1EventArgs = new JoueurSeConnecterEventArgs(joueur1);


            MethodInfo? methodInfo = typeof(Partie).GetMethod("JoueurConnecte", BindingFlags.NonPublic | BindingFlags.Instance);
            methodInfo?.Invoke(partie, new object?[] { null, joueur1EventArgs });


            Assert.True(demanderJoueurAppelée);

            FieldInfo? joueursField = typeof(Partie).GetField("joueurs", BindingFlags.NonPublic | BindingFlags.Instance);

            if (joueursField != null)
            {
                Dictionary<string, bool>? joueurs = joueursField.GetValue(partie) as Dictionary<string, bool>;
                if (joueurs != null)
                    Assert.True(joueurs.ContainsKey(joueur1.Nom));
            }

            FieldInfo? plateauxField = typeof(Partie).GetField("plateaux", BindingFlags.NonPublic | BindingFlags.Instance);
            if (plateauxField != null)
            {
                List<Plateau>? plateaux = plateauxField.GetValue(partie) as List<Plateau>;
                if (plateaux != null)
                    Assert.Single(plateaux);
            }

            Joueur joueur2 = new Joueur("Joueur2");
            JoueurSeConnecterEventArgs joueur2EventArgs = new JoueurSeConnecterEventArgs(joueur2);

            demanderJoueurAppelée = false;
            methodInfo?.Invoke(partie, new object?[] { null, joueur2EventArgs });

            if (joueursField != null)
            {
                Dictionary<string, bool>? joueurs = joueursField.GetValue(partie) as Dictionary<string, bool>;
                if (joueurs != null)
                {
                    Assert.Equal(2, joueurs.Count);
                    Assert.True(joueurs.ContainsKey(joueur2.Nom));
                }

            }

            if (plateauxField != null)
            {
                List<Plateau>? plateaux = plateauxField.GetValue(partie) as List<Plateau>;
                if (plateaux != null)
                    Assert.Equal(2, plateaux.Count);
            }

            PropertyInfo? tourProperty = typeof(Partie).GetProperty("Tour", BindingFlags.Public | BindingFlags.Instance);
            if (tourProperty != null)
            {
                object? tourValue = tourProperty.GetValue(partie);
                int? tour = tourValue as int?;
                if (tour != null)
                {
                    Assert.Equal(1, tour);
                }
            }
        }

        /// <summary>
        /// Test l'exception JoueurDejaPresentException de Partie.
        /// </summary>
        [Fact]
        public void TestJoueurConnecteDejaPresent()
        {
            IRegles regle = new ReglesClassiques();
            Partie partie = new Partie(regle);

            bool appelee = false;

            partie.PartieDemanderJoueur += (sender, e) =>
            {
                appelee = true;

                if (e.Indice == 1)
                    e.JoueurDemande.SeConnecter(new Joueur("Céleste"));
                else
                    Assert.Throws<JoueurDejaPresentException>(() => e.JoueurDemande.SeConnecter(new Joueur("Céleste")));
            };

            partie.Jouer();

            Assert.True(appelee);
        }

        /// <summary>
        /// Test l'exception NomJoueurInterditException de Partie.
        /// </summary>
        [Fact]
        public void TestJoueurConnecteNomInterdit()
        {
            IRegles regle = new ReglesClassiques();
            Partie partie = new Partie(regle);

            bool appelee = false;

            partie.PartieDemanderJoueur += (sender, e) =>
            {
                appelee = true;

                Assert.Throws<NomJoueurInterditException>(() => e.JoueurDemande.SeConnecter(new Joueur("Robot")));
            };

            partie.Jouer();

            Assert.True(appelee);
        }

        /// <summary>
        /// Test la creation d'un robot.
        /// </summary>
        [Fact]
        public void TestRobot()
        {
            IRegles regle = new ReglesClassiques();
            Partie partie = new Partie(regle);

            Assert.NotNull(partie.Robots);
            Assert.Empty(partie.Robots);

            partie.PartieDemanderJoueur += (sender, e) =>
            {
                e.JoueurDemande.SeConnecter(new Robot());
            };

            partie.Jouer();

            Assert.NotEmpty(partie.Robots);
        }

        /// <summary>
        /// Test la methode DebutPartie de Partie.
        /// </summary>
        [Fact]
        public void TestDebutPartie()
        {

            IRegles regle = new ReglesClassiques();
            Partie partie = new Partie(regle);

            PropertyInfo? plateauxField = typeof(Partie).GetProperty("plateaux", BindingFlags.Public | BindingFlags.Instance);
            if (plateauxField != null)
            {
                List<Plateau>? plateaux = plateauxField.GetValue(partie) as List<Plateau>;
                if (plateaux != null)
                {
                    plateaux.Add(new Plateau(4, 10));
                    plateaux.Add(new Plateau(4, 10));
                }
            }

            MethodInfo? methodInfo = typeof(Partie).GetMethod("DebutPartie", BindingFlags.NonPublic | BindingFlags.Instance);
            methodInfo?.Invoke(partie, null);

            PropertyInfo? tourProperty = typeof(Partie).GetProperty("Tour", BindingFlags.Public | BindingFlags.Instance);
            Assert.NotNull(tourProperty);
            if (tourProperty != null)
            {
                object? tourValue = tourProperty.GetValue(partie);
                int? tour = tourValue as int?;
                if (tour != null)
                {
                    Assert.Equal(1, tour);
                }
            }

            bool plateauAjouterCodeCalled = false;

            if (plateauxField != null)
            {
                List<Plateau>? plateaux = plateauxField.GetValue(partie) as List<Plateau>;
                if (plateaux != null)
                {
                    foreach (Plateau plateau in plateaux)
                    {
                        plateau.PlateauAjouterCode += (sender, e) => plateauAjouterCodeCalled = true;
                        plateau.AjouterCode(new Code(regle.TailleCode));
                        Assert.True(plateauAjouterCodeCalled);
                    }
                }
            }
        }

        /// <summary>
        /// Test la methode NouveauTour de Partie.
        /// </summary>
        [Fact]
        public void TestNouveauTour()
        {
            IRegles regle = new ReglesClassiques();
            Partie partie = new Partie(regle);


            FieldInfo? joueursField = typeof(Partie).GetField("joueurs", BindingFlags.NonPublic | BindingFlags.Instance);
            FieldInfo? plateauxField = typeof(Partie).GetField("plateaux", BindingFlags.NonPublic | BindingFlags.Instance);
            if (joueursField != null && plateauxField != null)
            {
                List<Plateau>? plateaux = plateauxField.GetValue(partie) as List<Plateau>;
                Dictionary<string, bool>? joueurs = joueursField.GetValue(partie) as Dictionary<string, bool>;
                if (joueurs != null && plateaux != null)
                {
                    joueurs.Add("Joueur10", false);
                    joueurs.Add("Joueur50", false);
                    plateaux.Add(new Plateau(regle.TailleCode, regle.NbTour));
                    plateaux.Add(new Plateau(regle.TailleCode, regle.NbTour));
                }
            }

            bool demanderJoueurJouerEventTriggered = false;
            bool nouveauTourEventTriggered = false;


            partie.PartieDemanderJoueurJouer += (sender, e) => demanderJoueurJouerEventTriggered = true;
            partie.PartieNouveauTour += (sender, e) => nouveauTourEventTriggered = true;

            MethodInfo? methodInfo = typeof(Partie).GetMethod("NouveauTour", BindingFlags.NonPublic | BindingFlags.Instance);
            methodInfo?.Invoke(partie, null);



            Assert.True(demanderJoueurJouerEventTriggered);
            Assert.True(nouveauTourEventTriggered);
        }

        /// <summary>
        /// Test la methode AjouterCode de Partie et verifie qu'il y a eu une incrementation pour le nombre de tour.
        /// </summary>
        [Fact]
        public void TestPlateauAjouterCodeIncrementation()
        {
            IRegles regle = new ReglesClassiques();
            Partie partie = new Partie(regle);


            FieldInfo? joueursField = typeof(Partie).GetField("joueurs", BindingFlags.NonPublic | BindingFlags.Instance);
            FieldInfo? plateauxField = typeof(Partie).GetField("plateaux", BindingFlags.NonPublic | BindingFlags.Instance);
            if (joueursField != null && plateauxField != null)
            {
                List<Plateau>? plateaux = plateauxField.GetValue(partie) as List<Plateau>;
                Dictionary<string, bool>? joueurs = joueursField.GetValue(partie) as Dictionary<string, bool>;
                if (joueurs != null && plateaux != null)
                {
                    joueurs.Add("Joueur1", false);
                    joueurs.Add("Joueur2", false);
                    plateaux.Add(new Plateau(regle.TailleCode, regle.NbTour));
                    plateaux.Add(new Plateau(regle.TailleCode, regle.NbTour));
                }
            }


            Plateau plateau = new Plateau(regle.TailleCode, regle.NbTour);
            PlateauAjouterCodeEventArgs eventArgs = new PlateauAjouterCodeEventArgs(plateau);


            MethodInfo? methodInfo = typeof(Partie).GetMethod("PlateauAjouterCode", BindingFlags.NonPublic | BindingFlags.Instance);
            methodInfo?.Invoke(partie, new object?[] { null, eventArgs });

            FieldInfo? courantField = typeof(Partie).GetField("courant", BindingFlags.NonPublic | BindingFlags.Instance);

            if (courantField != null)
            {
                int? courant = ((int?)courantField.GetValue(partie)).GetValueOrDefault();

                Assert.Equal(1, courant);
                Assert.Equal(0, partie.Tour);

            }

            methodInfo?.Invoke(partie, new object?[] { null, eventArgs });

            if (courantField != null)
            {
                int courant2 = ((int?)courantField.GetValue(partie)).GetValueOrDefault();
                Assert.Equal(0, courant2);
                Assert.Equal(1, partie.Tour);
            }

        }

        /// <summary>
        /// Test la methode AjouterCode de Partie et verifie que la partie est terminee.
        /// </summary>
        [Fact]
        public void PlateauAjouterCodeTerminee()
        {
            IRegles regle = new ReglesClassiques();
            Partie partie = new Partie(regle);

            FieldInfo? joueursField = typeof(Partie).GetField("joueurs", BindingFlags.NonPublic | BindingFlags.Instance);
            FieldInfo? plateauxField = typeof(Partie).GetField("plateaux", BindingFlags.NonPublic | BindingFlags.Instance);
            Plateau plateau = new Plateau(regle.TailleCode, regle.NbTour);
            if (joueursField != null && plateauxField != null)
            {
                List<Plateau>? plateaux = plateauxField.GetValue(partie) as List<Plateau>;
                Dictionary<string, bool>? joueurs = joueursField.GetValue(partie) as Dictionary<string, bool>;
                if (joueurs != null && plateaux != null)
                {
                    joueurs.Add("Joueur1", false);
                    joueurs.Add("Joueur2", false);
                    plateaux.Add(plateau);
                    plateaux.Add(plateau);
                    PropertyInfo? victoireProperty = typeof(Plateau).GetProperty("Victoire");
                    if (victoireProperty != null)
                    {
                        victoireProperty.SetValue(plateau, true);
                    }

                    FieldInfo? courantField = typeof(Partie).GetField("courant", BindingFlags.NonPublic | BindingFlags.Instance);
                    if (courantField != null)
                    {
                        courantField.SetValue(partie, 1);
                    }

                    PlateauAjouterCodeEventArgs eventArgs = new PlateauAjouterCodeEventArgs(plateau);
                    MethodInfo? methodInfo = typeof(Partie).GetMethod("PlateauAjouterCode", BindingFlags.NonPublic | BindingFlags.Instance);
                    methodInfo?.Invoke(partie, new object?[] { null, eventArgs });

                    Assert.True(partie.Termine);
                }
            }
        }

        /// <summary>
        /// Test la methode PartieTerminee de Partie et verifie les gagnants et perdants.
        /// </summary>
        [Fact]
        public void TestPartieTerminee()
        {
            IRegles regle = new ReglesClassiques();
            Partie partie = new Partie(regle);

            FieldInfo? joueursField = typeof(Partie).GetField("joueurs", BindingFlags.NonPublic | BindingFlags.Instance);
            FieldInfo? plateauxField = typeof(Partie).GetField("plateaux", BindingFlags.NonPublic | BindingFlags.Instance);
            Plateau plateau1 = new Plateau(regle.TailleCode, regle.NbTour);
            Plateau plateau2 = new Plateau(regle.TailleCode, regle.NbTour);
            if (joueursField != null && plateauxField != null)
            {
                List<Plateau>? plateaux = plateauxField.GetValue(partie) as List<Plateau>;
                Dictionary<string, bool>? joueurs = joueursField.GetValue(partie) as Dictionary<string, bool>;
                if (joueurs != null && plateaux != null)
                {
                    joueurs.Add("Joueur1", false);
                    joueurs.Add("Joueur2", false);
                    plateaux.Add(plateau1);
                    plateaux.Add(plateau2);
                    PropertyInfo? victoireProperty = typeof(Plateau).GetProperty("Victoire");
                    if (victoireProperty != null)
                    {
                        victoireProperty.SetValue(plateau1, true);
                    }


                    MethodInfo? methodInfo = typeof(Partie).GetMethod("PartieTerminee", BindingFlags.NonPublic | BindingFlags.Instance);
                    methodInfo?.Invoke(partie, null);

                    Assert.True(partie.Termine);
                    partie.PartiePartieTerminee += (sender, e) =>
                    {
                        Assert.True(partie.Termine);
                        Assert.Contains("Joueur1", e.Gagnants);
                        Assert.Contains("Joueur2", e.Perdants);
                    };


                }
            }
        }

        /// <summary>
        /// Test tous les evenements de Partie.
        /// </summary>
        [Fact]
        public void TestPartieEcoute()
        {
            Partie partie = new Partie(new ReglesClassiques());

            FieldInfo? joueursInfo = typeof(Partie).GetField("joueurs", BindingFlags.NonPublic | BindingFlags.Instance);
            FieldInfo? plateauxInfo = typeof(Partie).GetField("plateaux", BindingFlags.NonPublic | BindingFlags.Instance);

            Assert.NotNull(joueursInfo);
            Assert.NotNull(plateauxInfo);

            Dictionary<string, bool>? joueurs = joueursInfo.GetValue(partie) as Dictionary<string, bool>;
            List<Plateau>? plateaux = plateauxInfo.GetValue(partie) as List<Plateau>;

            Assert.NotNull(joueurs);
            Assert.NotNull(plateaux);

            joueurs.Add("Céleste", true);
            plateaux.Add(new Plateau(4, 12));

            MethodInfo? QuandPartieDemanderJoueurInfo = typeof(Partie).GetMethod("QuandPartieDemanderJoueur", BindingFlags.NonPublic | BindingFlags.Instance);
            MethodInfo? QuandPartieDebutPartieInfo = typeof(Partie).GetMethod("QuandPartieDebutPartie", BindingFlags.NonPublic | BindingFlags.Instance);
            MethodInfo? QuandPartieDemanderJoueurJouerInfo = typeof(Partie).GetMethod("QuandPartieDemanderJoueurJouer", BindingFlags.NonPublic | BindingFlags.Instance);
            MethodInfo? QuandPartieNouveauTourInfo = typeof(Partie).GetMethod("QuandPartieNouveauTour", BindingFlags.NonPublic | BindingFlags.Instance);
            MethodInfo? QuandPartiePasserLaMainInfo = typeof(Partie).GetMethod("QuandPartiePasserLaMain", BindingFlags.NonPublic | BindingFlags.Instance);
            MethodInfo? QuandPartiePartieTermineeInfo = typeof(Partie).GetMethod("QuandPartiePartieTerminee", BindingFlags.NonPublic | BindingFlags.Instance);

            Assert.NotNull(QuandPartieDemanderJoueurInfo);
            Assert.NotNull(QuandPartieDebutPartieInfo);
            Assert.NotNull(QuandPartieDemanderJoueurJouerInfo);
            Assert.NotNull(QuandPartieNouveauTourInfo);
            Assert.NotNull(QuandPartiePasserLaMainInfo);
            Assert.NotNull(QuandPartiePartieTermineeInfo);

            QuandPartieDemanderJoueurInfo?.Invoke(partie, [new Joueur()]);
            QuandPartieDebutPartieInfo?.Invoke(partie, []);
            QuandPartieDemanderJoueurJouerInfo?.Invoke(partie, [new Code(4)]);
            QuandPartieNouveauTourInfo?.Invoke(partie, [new Code(4)]);
            QuandPartiePasserLaMainInfo?.Invoke(partie, []);
            QuandPartiePartieTermineeInfo?.Invoke(partie, [new List<string>(["Céleste"]), new List<string>(["Robot 1"])]);

            bool appel1 = false;
            partie.PartieDemanderJoueur += (sender, e) => appel1 = true;
            bool appel2 = false;
            partie.PartieDebutPartie += (sender, e) => appel2 = true;
            bool appel3 = false;
            partie.PartieDemanderJoueurJouer += (sender, e) => appel3 = true;
            bool appel4 = false;
            partie.PartieNouveauTour += (sender, e) => appel4 = true;
            bool appel5 = false;
            partie.PartiePasserLaMain += (sender, e) => appel5 = true;
            bool appel6 = false;
            partie.PartiePartieTerminee += (sender, e) => appel6 = true;

            QuandPartieDemanderJoueurInfo?.Invoke(partie, [new Joueur()]);
            QuandPartieDebutPartieInfo?.Invoke(partie, []);
            QuandPartieDemanderJoueurJouerInfo?.Invoke(partie, [new Code(4)]);
            QuandPartieNouveauTourInfo?.Invoke(partie, [new Code(4)]);
            QuandPartiePasserLaMainInfo?.Invoke(partie, []);
            QuandPartiePartieTermineeInfo?.Invoke(partie, [new List<string>(["Céleste"]), new List<string>(["Robot 1"])]);

            Assert.True(appel1);
            Assert.True(appel2);
            Assert.True(appel3);
            Assert.True(appel4);
            Assert.True(appel5);
            Assert.True(appel6);
        }

        /// <summary>
        /// Test la methode AjouterCode de Partie avec toutes ses composantes et les differentes fin.
        /// </summary>
        [Fact]
        public void TestPartiePlateauAjouterCode()
        {
            // Cas 1 : dernier joueur - false, plateau complet - false, victoire - false
            Partie partie1 = new Partie(new ReglesClassiques());

            FieldInfo? joueursInfo1 = typeof(Partie).GetField("joueurs", BindingFlags.NonPublic | BindingFlags.Instance);
            FieldInfo? plateauxInfo1 = typeof(Partie).GetField("plateaux", BindingFlags.NonPublic | BindingFlags.Instance);
            FieldInfo? courantInfo1 = typeof(Partie).GetField("courant", BindingFlags.NonPublic | BindingFlags.Instance);

            Assert.NotNull(joueursInfo1);
            Assert.NotNull(plateauxInfo1);
            Assert.NotNull(courantInfo1);

            Dictionary<string, bool>? joueurs1 = joueursInfo1.GetValue(partie1) as Dictionary<string, bool>;
            List<Plateau>? plateaux1 = plateauxInfo1.GetValue(partie1) as List<Plateau>;

            Assert.NotNull(joueurs1);
            Assert.NotNull(plateaux1);

            courantInfo1.SetValue(partie1, 0);

            joueurs1.Add("Céleste", true);
            joueurs1.Add("Pauline", true);

            Plateau plateau1_j1 = new Plateau(1, 1);
            Plateau plateau1_j2 = new Plateau(1, 1);

            plateaux1.Add(plateau1_j1);
            plateaux1.Add(plateau1_j2);

            PropertyInfo? VictoireInfo1_j1 = typeof(Plateau).GetProperty("Victoire", BindingFlags.Public | BindingFlags.Instance);
            Assert.NotNull(VictoireInfo1_j1);
            VictoireInfo1_j1.SetValue(plateau1_j1, false);

            PropertyInfo? VictoireInfo1_j2 = typeof(Plateau).GetProperty("Victoire", BindingFlags.Public | BindingFlags.Instance);
            Assert.NotNull(VictoireInfo1_j2);
            VictoireInfo1_j2.SetValue(plateau1_j2, false);

            MethodInfo? PlateauAjouterCodeInfo1 = typeof(Partie).GetMethod("PlateauAjouterCode", BindingFlags.NonPublic | BindingFlags.Instance);
            Assert.NotNull(PlateauAjouterCodeInfo1);
            PlateauAjouterCodeInfo1.Invoke(partie1, [null, new PlateauAjouterCodeEventArgs(plateau1_j1)]);

            // Cas 2 : dernier joueur - false, plateau complet - false, victoire - true
            Partie partie2 = new Partie(new ReglesClassiques());

            FieldInfo? joueursInfo2 = typeof(Partie).GetField("joueurs", BindingFlags.NonPublic | BindingFlags.Instance);
            FieldInfo? plateauxInfo2 = typeof(Partie).GetField("plateaux", BindingFlags.NonPublic | BindingFlags.Instance);
            FieldInfo? courantInfo2 = typeof(Partie).GetField("courant", BindingFlags.NonPublic | BindingFlags.Instance);

            Assert.NotNull(joueursInfo2);
            Assert.NotNull(plateauxInfo2);
            Assert.NotNull(courantInfo2);

            Dictionary<string, bool>? joueurs2 = joueursInfo2.GetValue(partie2) as Dictionary<string, bool>;
            List<Plateau>? plateaux2 = plateauxInfo2.GetValue(partie2) as List<Plateau>;

            Assert.NotNull(joueurs2);
            Assert.NotNull(plateaux2);

            courantInfo2.SetValue(partie2, 0);

            joueurs2.Add("Céleste", true);
            joueurs2.Add("Pauline", true);

            Plateau plateau2_j1 = new Plateau(1, 1);
            Plateau plateau2_j2= new Plateau(1, 1);

            plateaux2.Add(plateau2_j1);
            plateaux2.Add(plateau2_j2);

            PropertyInfo? VictoireInfo2_j1 = typeof(Plateau).GetProperty("Victoire", BindingFlags.Public | BindingFlags.Instance);
            Assert.NotNull(VictoireInfo2_j1);
            VictoireInfo2_j1.SetValue(plateau2_j1, true);

            PropertyInfo? VictoireInfo2_j2 = typeof(Plateau).GetProperty("Victoire", BindingFlags.Public | BindingFlags.Instance);
            Assert.NotNull(VictoireInfo2_j2);
            VictoireInfo2_j2.SetValue(plateau2_j2, true); 
            
            MethodInfo? PlateauAjouterCodeInfo2 = typeof(Partie).GetMethod("PlateauAjouterCode", BindingFlags.NonPublic | BindingFlags.Instance);
            Assert.NotNull(PlateauAjouterCodeInfo2);
            PlateauAjouterCodeInfo2.Invoke(partie2, [null, new PlateauAjouterCodeEventArgs(plateau2_j1)]);


            // Cas 3 : dernier joueur - false, plateau complet - true, victoire - false
            Partie partie3 = new Partie(new ReglesClassiques());

            FieldInfo? joueursInfo3 = typeof(Partie).GetField("joueurs", BindingFlags.NonPublic | BindingFlags.Instance);
            FieldInfo? plateauxInfo3 = typeof(Partie).GetField("plateaux", BindingFlags.NonPublic | BindingFlags.Instance);
            FieldInfo? courantInfo3 = typeof(Partie).GetField("courant", BindingFlags.NonPublic | BindingFlags.Instance);

            Assert.NotNull(joueursInfo3);
            Assert.NotNull(plateauxInfo3);
            Assert.NotNull(courantInfo3);

            Dictionary<string, bool>? joueurs3 = joueursInfo3.GetValue(partie3) as Dictionary<string, bool>;
            List<Plateau>? plateaux3 = plateauxInfo3.GetValue(partie3) as List<Plateau>;

            Assert.NotNull(joueurs3);
            Assert.NotNull(plateaux3);

            courantInfo3.SetValue(partie3, 0);

            joueurs3.Add("Céleste", true);
            joueurs3.Add("Pauline", true);

            Plateau plateau3_j1 = new Plateau(1, 1);
            Plateau plateau3_j2 = new Plateau(1, 1);

            Code code3_j1 = new Code(1);
            code3_j1.AjouterJeton(new Jeton(Couleur.Rouge));
            Code code3_j2 = new Code(1);
            code3_j2.AjouterJeton(new Jeton(Couleur.Rouge));

            plateau3_j1.AjouterCode(code3_j1);
            plateau3_j2.AjouterCode(code3_j2);

            plateaux3.Add(plateau3_j1);
            plateaux3.Add(plateau3_j2);

            PropertyInfo? VictoireInfo3_j1 = typeof(Plateau).GetProperty("Victoire", BindingFlags.Public | BindingFlags.Instance);
            Assert.NotNull(VictoireInfo3_j1);
            VictoireInfo3_j1.SetValue(plateau3_j1, false);

            PropertyInfo? VictoireInfo3_j2 = typeof(Plateau).GetProperty("Victoire", BindingFlags.Public | BindingFlags.Instance);
            Assert.NotNull(VictoireInfo3_j2);
            VictoireInfo3_j2.SetValue(plateau3_j2, false);

            MethodInfo? PlateauAjouterCodeInfo3 = typeof(Partie).GetMethod("PlateauAjouterCode", BindingFlags.NonPublic | BindingFlags.Instance);
            Assert.NotNull(PlateauAjouterCodeInfo3);
            PlateauAjouterCodeInfo3.Invoke(partie3, [null, new PlateauAjouterCodeEventArgs(plateau3_j1)]);


            // Cas 4 : dernier joueur - false, plateau complet - true, victoire - true
            Partie partie4 = new Partie(new ReglesClassiques());

            FieldInfo? joueursInfo4 = typeof(Partie).GetField("joueurs", BindingFlags.NonPublic | BindingFlags.Instance);
            FieldInfo? plateauxInfo4 = typeof(Partie).GetField("plateaux", BindingFlags.NonPublic | BindingFlags.Instance);
            FieldInfo? courantInfo4 = typeof(Partie).GetField("courant", BindingFlags.NonPublic | BindingFlags.Instance);

            Assert.NotNull(joueursInfo4);
            Assert.NotNull(plateauxInfo4);
            Assert.NotNull(courantInfo4);

            Dictionary<string, bool>? joueurs4 = joueursInfo4.GetValue(partie4) as Dictionary<string, bool>;
            List<Plateau>? plateaux4 = plateauxInfo4.GetValue(partie4) as List<Plateau>;

            Assert.NotNull(joueurs4);
            Assert.NotNull(plateaux4);

            courantInfo4.SetValue(partie4, 0);

            joueurs4.Add("Céleste", true);
            joueurs4.Add("Pauline", true);

            Plateau plateau4_j1 = new Plateau(1, 1);
            Plateau plateau4_j2 = new Plateau(1, 1);

            Code code4_j1 = new Code(1);
            code4_j1.AjouterJeton(new Jeton(Couleur.Rouge));
            Code code4_j2 = new Code(1);
            code4_j2.AjouterJeton(new Jeton(Couleur.Rouge));

            plateau4_j1.AjouterCode(code4_j1);
            plateau4_j2.AjouterCode(code4_j2);

            plateaux4.Add(plateau4_j1);
            plateaux4.Add(plateau4_j2);

            PropertyInfo? VictoireInfo4_j1 = typeof(Plateau).GetProperty("Victoire", BindingFlags.Public | BindingFlags.Instance);
            Assert.NotNull(VictoireInfo4_j1);
            VictoireInfo4_j1.SetValue(plateau4_j1, true);

            PropertyInfo? VictoireInfo4_j2 = typeof(Plateau).GetProperty("Victoire", BindingFlags.Public | BindingFlags.Instance);
            Assert.NotNull(VictoireInfo4_j2);
            VictoireInfo4_j2.SetValue(plateau4_j2, true);

            MethodInfo? PlateauAjouterCodeInfo4 = typeof(Partie).GetMethod("PlateauAjouterCode", BindingFlags.NonPublic | BindingFlags.Instance);
            Assert.NotNull(PlateauAjouterCodeInfo4);
            PlateauAjouterCodeInfo4.Invoke(partie4, [null, new PlateauAjouterCodeEventArgs(plateau4_j1)]);


            // Cas 5 : dernier joueur - true, plateau complet - false, victoire - false
            Partie partie5 = new Partie(new ReglesClassiques());

            FieldInfo? joueursInfo5 = typeof(Partie).GetField("joueurs", BindingFlags.NonPublic | BindingFlags.Instance);
            FieldInfo? plateauxInfo5 = typeof(Partie).GetField("plateaux", BindingFlags.NonPublic | BindingFlags.Instance);
            FieldInfo? courantInfo5 = typeof(Partie).GetField("courant", BindingFlags.NonPublic | BindingFlags.Instance);

            Assert.NotNull(joueursInfo5);
            Assert.NotNull(plateauxInfo5);
            Assert.NotNull(courantInfo5);

            Dictionary<string, bool>? joueurs5 = joueursInfo5.GetValue(partie5) as Dictionary<string, bool>;
            List<Plateau>? plateaux5 = plateauxInfo5.GetValue(partie5) as List<Plateau>;

            Assert.NotNull(joueurs5);
            Assert.NotNull(plateaux5);

            courantInfo5.SetValue(partie5, 1);

            joueurs5.Add("Céleste", true);
            joueurs5.Add("Pauline", true);

            Plateau plateau5_j1 = new Plateau(1, 1);
            Plateau plateau5_j2 = new Plateau(1, 1);

            plateaux5.Add(plateau5_j1);
            plateaux5.Add(plateau5_j2);

            PropertyInfo? VictoireInfo5_j1 = typeof(Plateau).GetProperty("Victoire", BindingFlags.Public | BindingFlags.Instance);
            Assert.NotNull(VictoireInfo5_j1);
            VictoireInfo5_j1.SetValue(plateau5_j1, false);

            PropertyInfo? VictoireInfo5_j2 = typeof(Plateau).GetProperty("Victoire", BindingFlags.Public | BindingFlags.Instance);
            Assert.NotNull(VictoireInfo5_j2);
            VictoireInfo5_j2.SetValue(plateau5_j2, false);

            MethodInfo? PlateauAjouterCodeInfo5 = typeof(Partie).GetMethod("PlateauAjouterCode", BindingFlags.NonPublic | BindingFlags.Instance);
            Assert.NotNull(PlateauAjouterCodeInfo5);
            PlateauAjouterCodeInfo5.Invoke(partie5, [null, new PlateauAjouterCodeEventArgs(plateau5_j1)]);

            // Cas 6 : dernier joueur - true, plateau complet - false, victoire - true
            Partie partie6 = new Partie(new ReglesClassiques());

            FieldInfo? joueursInfo6 = typeof(Partie).GetField("joueurs", BindingFlags.NonPublic | BindingFlags.Instance);
            FieldInfo? plateauxInfo6 = typeof(Partie).GetField("plateaux", BindingFlags.NonPublic | BindingFlags.Instance);
            FieldInfo? courantInfo6 = typeof(Partie).GetField("courant", BindingFlags.NonPublic | BindingFlags.Instance);

            Assert.NotNull(joueursInfo6);
            Assert.NotNull(plateauxInfo6);
            Assert.NotNull(courantInfo6);

            Dictionary<string, bool>? joueurs6 = joueursInfo6.GetValue(partie6) as Dictionary<string, bool>;
            List<Plateau>? plateaux6 = plateauxInfo6.GetValue(partie6) as List<Plateau>;

            Assert.NotNull(joueurs6);
            Assert.NotNull(plateaux6);

            courantInfo6.SetValue(partie6, 1);

            joueurs6.Add("Céleste", true);
            joueurs6.Add("Pauline", true);

            Plateau plateau6_j1 = new Plateau(1, 1);
            Plateau plateau6_j2 = new Plateau(1, 1);

            plateaux6.Add(plateau6_j1);
            plateaux6.Add(plateau6_j2);

            PropertyInfo? VictoireInfo6_j1 = typeof(Plateau).GetProperty("Victoire", BindingFlags.Public | BindingFlags.Instance);
            Assert.NotNull(VictoireInfo6_j1);
            VictoireInfo6_j1.SetValue(plateau6_j1, true);

            PropertyInfo? VictoireInfo6_j2 = typeof(Plateau).GetProperty("Victoire", BindingFlags.Public | BindingFlags.Instance);
            Assert.NotNull(VictoireInfo6_j2);
            VictoireInfo6_j2.SetValue(plateau6_j2, true);

            MethodInfo? PlateauAjouterCodeInfo6 = typeof(Partie).GetMethod("PlateauAjouterCode", BindingFlags.NonPublic | BindingFlags.Instance);
            Assert.NotNull(PlateauAjouterCodeInfo6);
            PlateauAjouterCodeInfo6.Invoke(partie6, [null, new PlateauAjouterCodeEventArgs(plateau6_j1)]);

            // Cas 7 : dernier joueur - true, plateau complet - true, victoire - false
            Partie partie7 = new Partie(new ReglesClassiques());

            FieldInfo? joueursInfo7 = typeof(Partie).GetField("joueurs", BindingFlags.NonPublic | BindingFlags.Instance);
            FieldInfo? plateauxInfo7 = typeof(Partie).GetField("plateaux", BindingFlags.NonPublic | BindingFlags.Instance);
            FieldInfo? courantInfo7 = typeof(Partie).GetField("courant", BindingFlags.NonPublic | BindingFlags.Instance);

            Assert.NotNull(joueursInfo7);
            Assert.NotNull(plateauxInfo7);
            Assert.NotNull(courantInfo7);

            Dictionary<string, bool>? joueurs7 = joueursInfo7.GetValue(partie7) as Dictionary<string, bool>;
            List<Plateau>? plateaux7 = plateauxInfo7.GetValue(partie7) as List<Plateau>;

            Assert.NotNull(joueurs7);
            Assert.NotNull(plateaux7);

            courantInfo7.SetValue(partie7, 1);

            joueurs7.Add("Céleste", true);
            joueurs7.Add("Pauline", true);

            Plateau plateau7_j1 = new Plateau(1, 1);
            Plateau plateau7_j2 = new Plateau(1, 1);

            Code code7_j1 = new Code(1);
            code7_j1.AjouterJeton(new Jeton(Couleur.Rouge));
            Code code7_j2 = new Code(1);
            code7_j2.AjouterJeton(new Jeton(Couleur.Rouge));

            plateau7_j1.AjouterCode(code7_j1);
            plateau7_j2.AjouterCode(code7_j2);

            plateaux7.Add(plateau7_j1);
            plateaux7.Add(plateau7_j2);

            PropertyInfo? VictoireInfo7_j1 = typeof(Plateau).GetProperty("Victoire", BindingFlags.Public | BindingFlags.Instance);
            Assert.NotNull(VictoireInfo7_j1);
            VictoireInfo7_j1.SetValue(plateau7_j1, false);

            PropertyInfo? VictoireInfo7_j2 = typeof(Plateau).GetProperty("Victoire", BindingFlags.Public | BindingFlags.Instance);
            Assert.NotNull(VictoireInfo7_j2);
            VictoireInfo7_j2.SetValue(plateau7_j2, false);

            MethodInfo? PlateauAjouterCodeInfo7 = typeof(Partie).GetMethod("PlateauAjouterCode", BindingFlags.NonPublic | BindingFlags.Instance);
            Assert.NotNull(PlateauAjouterCodeInfo7);
            PlateauAjouterCodeInfo7.Invoke(partie7, [null, new PlateauAjouterCodeEventArgs(plateau7_j1)]);

            // Cas 8 : dernier joueur - true, plateau complet - true, victoire - true
            Partie partie8 = new Partie(new ReglesClassiques());

            FieldInfo? joueursInfo8 = typeof(Partie).GetField("joueurs", BindingFlags.NonPublic | BindingFlags.Instance);
            FieldInfo? plateauxInfo8 = typeof(Partie).GetField("plateaux", BindingFlags.NonPublic | BindingFlags.Instance);
            FieldInfo? courantInfo8 = typeof(Partie).GetField("courant", BindingFlags.NonPublic | BindingFlags.Instance);

            Assert.NotNull(joueursInfo8);
            Assert.NotNull(plateauxInfo8);
            Assert.NotNull(courantInfo8);

            Dictionary<string, bool>? joueurs8 = joueursInfo8.GetValue(partie8) as Dictionary<string, bool>;
            List<Plateau>? plateaux8 = plateauxInfo8.GetValue(partie8) as List<Plateau>;

            Assert.NotNull(joueurs8);
            Assert.NotNull(plateaux8);

            courantInfo8.SetValue(partie8, 1);

            joueurs8.Add("Céleste", true);
            joueurs8.Add("Pauline", true);

            Plateau plateau8_j1 = new Plateau(1, 1);
            Plateau plateau8_j2 = new Plateau(1, 1);

            Code code8_j1 = new Code(1);
            code8_j1.AjouterJeton(new Jeton(Couleur.Rouge));
            Code code8_j2 = new Code(1);
            code8_j2.AjouterJeton(new Jeton(Couleur.Rouge));

            plateau8_j1.AjouterCode(code8_j1);
            plateau8_j2.AjouterCode(code8_j2);

            plateaux8.Add(plateau8_j1);
            plateaux8.Add(plateau8_j2);

            PropertyInfo? VictoireInfo8_j1 = typeof(Plateau).GetProperty("Victoire", BindingFlags.Public | BindingFlags.Instance);
            Assert.NotNull(VictoireInfo8_j1);
            VictoireInfo8_j1.SetValue(plateau8_j1, true);

            PropertyInfo? VictoireInfo8_j2 = typeof(Plateau).GetProperty("Victoire", BindingFlags.Public | BindingFlags.Instance);
            Assert.NotNull(VictoireInfo8_j2);
            VictoireInfo8_j2.SetValue(plateau8_j2, true);

            MethodInfo? PlateauAjouterCodeInfo8 = typeof(Partie).GetMethod("PlateauAjouterCode", BindingFlags.NonPublic | BindingFlags.Instance);
            Assert.NotNull(PlateauAjouterCodeInfo8);
            PlateauAjouterCodeInfo8.Invoke(partie8, [null, new PlateauAjouterCodeEventArgs(plateau8_j1)]);
        }
    }
}
