﻿using CoreLibrary.Evenements;
using Xunit;

namespace UnitTesting
{
    /// <summary>
    /// Classe de test de l'evenement PartiePartieTermineeEventArgs.
    /// </summary>
    public class PartiePartieTermineeEventArgsUT
    {
        /// <summary>
        /// Test du constructeur de l'evenement valide.
        /// </summary>
        [Fact]
        public void TestConstructeurValide()
        {
            IReadOnlyList<string> gagnants = new List<string>(["joueur1", "joueur2"]);
            IReadOnlyList<string> perdants = new List<string>(["joueur3"]);
            int tour = 10;

            PartiePartieTermineeEventArgs evenement = new PartiePartieTermineeEventArgs(tour, gagnants, perdants);
            Assert.Equal(tour, evenement.Tour);
            Assert.Equal(gagnants, evenement.Gagnants);
            Assert.Equal(perdants, evenement.Perdants);
        }
    }
}
