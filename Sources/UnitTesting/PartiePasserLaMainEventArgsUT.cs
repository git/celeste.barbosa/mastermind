﻿using CoreLibrary.Evenements;
using Xunit;

namespace UnitTesting
{
    /// <summary>
    /// Classe de test de l'evenement PartiePasserLaMainEventArgs.
    /// </summary>
    public class PartiePasserLaMainEventArgsUT
    {
        /// <summary>
        /// Test du constructeur de l'evenement PartiePasserLaMainEventArgs.
        /// </summary>
        [Fact]
        public void TestConstructeurValide()
        {
            string joueur = "MonJoueur";

            PartiePasserLaMainEventArgs evenement = new PartiePasserLaMainEventArgs(joueur);
            Assert.Equal(joueur, evenement.Joueur);
        }
    }
}
