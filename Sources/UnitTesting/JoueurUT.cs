﻿using CoreLibrary.Core;
using CoreLibrary.Evenements;
using CoreLibrary.Exceptions;
using CoreLibrary.Joueurs;
using CoreLibrary.Regles;
using CoreLibrary.Statistiques;
using System.Reflection;
using Xunit;

namespace UnitTesting
{
    /// <summary>
    /// Classe de test de la classe Joueur.
    /// </summary>
    public class JoueurUT
    {
        /// <summary>
        /// Test le premier constructeur d'un joueur.
        /// </summary>
        [Fact]
        public void TestConstructeur1Valide()
        {
            string nom = "";

            Joueur joueur = new Joueur();


            Assert.Equal(nom, joueur.Nom);
        }

        /// <summary>
        /// Test le deuxieme constructeur d'un joueur.
        /// </summary>
        [Fact]
        public void TestConstructeur2Valide()
        {
            string nom = "toto";

            Joueur joueur = new Joueur(nom);


            Assert.Equal(nom, joueur.Nom);
        }

        /// <summary>
        /// Test de l'événement JoueurSeConnecterEventArgs.
        /// </summary>
        [Fact]
        public void TestQuandJoueurSeConnecter()
        {
            Joueur joueur1 = new Joueur("Joueur1");
            Joueur joueur2 = new Joueur("Joueur2");
            bool eventTriggered = false;
            JoueurSeConnecterEventArgs? eventArgs = null;

            joueur1.JoueurSeConnecter += (sender, e) =>
            {
                eventTriggered = true;
                eventArgs = e;
            };

            MethodInfo? methodInfo = typeof(Joueur).GetMethod("QuandJoueurSeConnecter", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            methodInfo?.Invoke(joueur1, new object[] { joueur2 });

            Assert.True(eventTriggered);
            Assert.NotNull(eventArgs);
            Assert.Equal(joueur2, eventArgs?.Joueur);
        }

        /// <summary>
        /// Test de la methode SeConnecter de la classe Joueur.
        /// </summary>
        [Fact]
        public void TestSeConnecterDeclencheEvenement()
        {
            Joueur joueur1 = new Joueur("Joueur1");
            Joueur joueur2 = new Joueur("Joueur2");
            bool eventTriggered = false;
            JoueurSeConnecterEventArgs? eventArgs = null;

            joueur1.JoueurSeConnecter += (sender, e) =>
            {
                eventTriggered = true;
                eventArgs = e;
            };

            joueur1.SeConnecter(joueur2);

            Assert.True(eventTriggered);
            Assert.NotNull(eventArgs);
            Assert.Equal(joueur2, eventArgs?.Joueur);
        }

        /// <summary>
        /// Test la methode ToString valide de la classe Joueur.
        /// </summary>
        [Fact]
        public void TestToStringValide()
        {
            string nom = "Joueur";
            Joueur joueur = new Joueur(nom);
            string result = joueur.ToString();
            Assert.Equal(nom, result);
        }

        /// <summary>
        /// Test de la methode ToString vide, de la classe Joueur.
        /// </summary>
        [Fact]
        public void TestToStringVide()
        {
            Joueur joueur = new Joueur();
            string result = joueur.ToString();

            Assert.Equal("", result);
        }

        /// <summary>
        /// Test la methode Statistique sans incrementation.
        /// </summary>
        [Fact]
        public void TestStatistiqueNonDefinie()
        {
            Joueur joueur = new Joueur("Joueur");
            ReglesClassiques regles = new ReglesClassiques();
            Statistique statistique = new Statistique();

            double result = joueur.Statistique(regles, statistique);

            Assert.Equal(0, result);
        }

        /// <summary>
        /// Test la methode Statistique avec incrementation.
        /// </summary>
        [Fact]
        public void TestStatistiqueDefinie()
        {
            Joueur joueur = new Joueur("Joueur");
            ReglesClassiques regles = new ReglesClassiques();
            Statistique statistique = new Statistique();
            joueur.IncrementerStatistique(regles, statistique);

            double result = joueur.Statistique(regles, statistique);

            Assert.Equal(1, result);
        }

        /// <summary>
        /// Test de la methode IncrementerSStatistique avec une incrementation.
        /// </summary>
        [Fact]
        public void TestIncrementerStatistiqueUn()
        {
            Joueur joueur = new Joueur("Joueur");
            ReglesClassiques regles = new ReglesClassiques();
            Statistique statistique = new Statistique();

            joueur.IncrementerStatistique(regles, statistique);
            double result = joueur.Statistique(regles, statistique);

            Assert.Equal(1, result);
        }

        /// <summary>
        /// Test de la methode IncrementerSStatistique avec deux incrementation.
        /// </summary>
        [Fact]
        public void TestIncrementerStatistiqueDeux()
        {
            Joueur joueur = new Joueur("Joueur");
            ReglesClassiques regles = new ReglesClassiques();
            Statistique statistique = new Statistique();

            joueur.IncrementerStatistique(regles, statistique);
            joueur.IncrementerStatistique(regles, statistique);
            double result = joueur.Statistique(regles, statistique);

            Assert.Equal(2, result);
        }

        /// <summary>
        /// Test de l'evenement QuandJoueurSeConnecter de la classe Joueur.
        /// </summary>
        [Fact]
        public void TestJoueurEcoute()
        {
            Joueur joueur = new Joueur("Joueur1");

            MethodInfo? QuandJoueurSeConnecter = typeof(Joueur).GetMethod("QuandJoueurSeConnecter", BindingFlags.NonPublic | BindingFlags.Instance);

            Assert.NotNull(QuandJoueurSeConnecter);

            QuandJoueurSeConnecter?.Invoke(joueur, [joueur]);

            bool appel = false;
            joueur.JoueurSeConnecter += (sender, e) => appel = true;

            QuandJoueurSeConnecter?.Invoke(joueur, [joueur]);

            Assert.True(appel);
        }
    }
}
