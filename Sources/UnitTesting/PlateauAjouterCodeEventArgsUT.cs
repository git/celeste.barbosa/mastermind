﻿using CoreLibrary.Core;
using CoreLibrary.Evenements;
using Xunit;

namespace UnitTesting
{
    /// <summary>
    /// Classe de test de l'evenement PlateauAjouterCodeEventArgs.
    /// </summary>
    public class PlateauAjouterCodeEventArgsUT
    {
        /// <summary>
        /// Test du constructeur valide.
        /// </summary>
        [Fact]
        public void TestConstructeurValide()
        {
            Plateau plateau = new Plateau(4,3);

            PlateauAjouterCodeEventArgs evenement = new PlateauAjouterCodeEventArgs(plateau);

            Assert.Equal(plateau, evenement.Plateau);
        }
    }
}
