﻿using MauiSpark.Pages;

namespace MauiSpark
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
        }

        protected override Window CreateWindow(IActivationState? activationState)
        {
            Window window = new Window(new NavigationPage(new AccueilPage()));
            window.Title = "Mastermind";
            
            return window;
        }
    }
}
