namespace MauiSpark.Pages;

/// <summary>
/// Page permettant de reprendre les parties en cours.
/// </summary>
public partial class ReprendrePage : ContentPage
{
    /// <summary>
    /// Constructeur de la page de reprise.
    /// Initialise les param�tres de navigation et les composants de la page.
    /// Initialise le contexte de liaison avec les parties non termin�es.
    /// </summary>
    public ReprendrePage()
	{
        NavigationPage.SetHasNavigationBar(this, false);

        BindingContext = MauiProgram.Manageur.PartiesNonTerminees;

        InitializeComponent();
	}
}