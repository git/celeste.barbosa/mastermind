using CoreLibrary.Regles;

namespace MauiSpark.Pages;

/// <summary>
/// Page de s�lection des modes de jeux.
/// </summary>
public partial class ModePage : ContentPage
{
    /// <summary>
    /// Constructeur de la page mode.
    /// Initialise les param�tres de navigation et les composants de la page.
    /// Initialise le binding contexte avec une liste de r�gles.
    /// </summary>
    public ModePage()
	{
        NavigationPage.SetHasNavigationBar(this, false);

        BindingContext = typeof(IRegles).Assembly.GetTypes()
            .Where(type => typeof(IRegles).IsAssignableFrom(type) && type.IsClass)
            .Select(type => (Activator.CreateInstance(type) as IRegles)!)
            .OrderBy(regles => regles.Indice);

        InitializeComponent();
    }
}