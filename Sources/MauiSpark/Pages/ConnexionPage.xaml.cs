using CoreLibrary.Evenements;
using CoreLibrary.Exceptions;
using CoreLibrary.Joueurs;

namespace MauiSpark.Pages;

/// <summary>
/// Page de connexion o� les joueurs de la partie sont entr�s.
/// </summary>
public partial class ConnexionPage : ContentPage
{
    /// <summary>
    /// Indice du joueur � entrer.
    /// </summary>
    private int? indice;

    /// <summary>
    /// Le joueur demand�.
    /// </summary>
    private Joueur? joueurDemande;

    /// <summary>
    /// Constructeur de la page de connexion.
    /// Initialise les param�tres de navigation et les composants de la page.
    /// </summary>
    public ConnexionPage()
	{
		NavigationPage.SetHasNavigationBar(this, false);

        InitializeComponent();
    }

    /// <summary>
    /// M�thode d�clench�e lorsque l'�v�nement DemanderNom de partie est appel�e.
    /// </summary>
    /// <param name="sender">La classe qui appelle l'�v�nement ; ici Partie.</param>
    /// <param name="e">L'instance de l'�v�nement PartieDemanderJoueurEventArgs cr��e par Partie.</param>
    public async void QuandDemanderNom(Object? sender, PartieDemanderJoueurEventArgs e)
	{
		if(Application.Current != null && Application.Current.MainPage != null && ((NavigationPage)Application.Current.MainPage).CurrentPage != this)
			await Application.Current.MainPage.Navigation.PushAsync(this);

		Nom.Text = "";
		indice = e.Indice;
        joueurDemande = e.JoueurDemande;
        BindingContext = $"Joueur {e.Indice}";
	}

    /// <summary>
    /// M�thode d�clench�e lorsque le bouton "Se Connecter" est cliqu�.
    /// </summary>
    /// <param name="sender">L'objet qui appelle l'�v�nement ; ici le bouton SeConnecterPresse.</param>
    /// <param name="e">L'instance de l'�v�nement EventArgs cr��e par le syst�me.</param>
    private void QuandSeConnecterPresse(Object sender, EventArgs e)
	{
        if (joueurDemande == null || indice == null)
            return;

        Joueur joueur;
        if (string.IsNullOrEmpty(Nom.Text))
        {
            joueur = new Joueur($"Joueur {indice.Value}");
        }
        else
        {
            joueur = MauiProgram.Manageur.DemanderJoueur(Nom.Text);
        }

        try
        {
            joueurDemande.SeConnecter(joueur);
        }
        catch(Exception exception)
        when (exception is JoueurDejaPresentException || exception is NomJoueurInterditException)
        {
            DisplayAlert("Attention", exception.Message, "OK");
        }
    }

    /// <summary>
    /// M�thode d�clench�e lorsque le bouton "Robot" est cliqu�.
    /// </summary>
    /// <param name="sender">L'objet qui appelle l'�v�nement ; ici le bouton RobotPresse.</param>
    /// <param name="e">L'instance de l'�v�nement EventArgs cr��e par le syst�me.</param>
    private void QuandRobotPresse(object sender, EventArgs e)
    {
        if (joueurDemande == null || indice == null)
            return;

        Robot robot = new Robot();

        joueurDemande.SeConnecter(robot);
    }
}