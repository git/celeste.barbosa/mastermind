using CoreLibrary.Evenements;
using CoreLibrary.Joueurs;

namespace MauiSpark.Pages;


partial class Message
{
    public string Titre { get; init; } = "";
    public string Texte { get; init; } = "";
    public string Image { get; init; } = "";
}

/// <summary>
/// Page affich�e lorsque la partie est termin�e, montrant le r�sultat de la partie.
/// </summary>

public partial class VictoirePage : ContentPage
{
    /// <summary>
    /// Constructeur de la page de victoire.
    /// Initialise les param�tres de navigation et les composants de la page.
    /// </summary>
    public VictoirePage()
	{
        NavigationPage.SetHasNavigationBar(this, false);

        InitializeComponent();
    }

    /// <summary>
    /// M�thode d�clench�e lorsque la partie est termin�e.
    /// Affiche le r�sultat de la partie en fonction des gagnants et des perdants.
    /// </summary>
    /// <param name="sender">La classe qui appelle l'�v�nement ; ici Partie.</param>
    /// <param name="e">L'instance de l'�v�nement PartiePartieTermineeEventArgs cr��e par Partie.</param>
    public async void QuandPartieTerminee(object? sender, PartiePartieTermineeEventArgs e)
    {
        if (Application.Current == null || Application.Current.MainPage == null)
            return;

        if (((NavigationPage)Application.Current.MainPage).CurrentPage != this)
            await Application.Current.MainPage.Navigation.PushAsync(this);

        IEnumerable<Page> pages = Application.Current.MainPage.Navigation.NavigationStack.Reverse().Skip(1);

        foreach (Page page in pages)
        {
            if (page is AccueilPage)
                break;

            Application.Current.MainPage.Navigation.RemovePage(page);
        }

        IReadOnlyList<string> gagnants = e.Gagnants;
        IReadOnlyList<string> perdants = e.Perdants;

        // Affichage du message en fonction du r�sultat de la partie
        if (gagnants.Count == 1)
            BindingContext = new Message()
            {
                Titre = "Victoire",
                Texte = $"Le joueur {gagnants[0]} a gagn�",
                Image = "victoire.png"
            };
        else if (gagnants.Count > 1)
            BindingContext = new Message()
            {
                Titre = "Egalit�",
                Texte = $"Les joueurs {string.Join(", ", gagnants)} ont gagn�",
                Image = "egalite.png"
            };
        else
            BindingContext = new Message()
            {
                Titre = "D�faite",
                Texte = "Personne n'a trouv� le code...",
                Image = "defaite.png"
            };
    }

    /// <summary>
    /// M�thode d�clench�e lorsque le bouton de retour au menu est press�.
    /// Retourne � la page pr�c�dente.
    /// </summary>
    /// <param name="sender">L'objet qui appelle l'�v�nement ; ici le bouton de retour au menu.</param>
    /// <param name="e">L'instance de l'�v�nement EventArgs cr��e par le syst�me.</param>
    public async void QuandMenuPresse(object sender, EventArgs e)
    {
        await Navigation.PopAsync();
    }
}