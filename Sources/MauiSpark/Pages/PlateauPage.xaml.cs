using CoreLibrary.Core;
using CoreLibrary.Evenements;
using CoreLibrary.Exceptions;

namespace MauiSpark.Pages;

/// <summary>
/// Repr�sente un tour de jeu, avec le plateau actuel, le joueur en cours, le num�ro de tour et le code.
/// </summary>
internal class Tour
{

    /// <summary>
    /// Obtient ou d�finit le plateau de jeu pour ce tour.
    /// </summary>
    public IEnumerable<(IEnumerable<Jeton>, IEnumerable<Indicateur>)> Plateau { get; private init; }

    /// <summary>
    /// Obtient ou d�finit le nom du joueur pour ce tour.
    /// </summary>
    public string Joueur { get; private init; }

    /// <summary>
    /// Obtient ou d�finit le num�ro de tour.
    /// </summary>
    public string Numero { get; private init; }

    /// <summary>
    /// Obtient ou d�finit le code pour ce tour.
    /// </summary>
    public Code Code { get; private init; }
    public bool EstJoueur { get; private init; }

    /// <summary>
    /// Initialise un tour � partir des arguments de l'�v�nement de nouveau tour de jeu.
    /// </summary>
    /// <param name="e">Les arguments de l'�v�nement PartieNouveauTourEventArgs.</param>
    public Tour(PartieNouveauTourEventArgs e)
    {
        Numero = $"Tour {e.Tour}";
        Joueur = e.Nom;
        Code = e.Code;
        EstJoueur = e.EstJoueur;

        (IEnumerable<IEnumerable<Jeton>> jetons, IEnumerable<IEnumerable<Indicateur>> indicateurs) = e.Plateau.Grille;
        
        List<(IEnumerable<Jeton>, IEnumerable<Indicateur>)> plateau = new List<(IEnumerable<Jeton>, IEnumerable<Indicateur>)>();
        for (int i = 0; i < e.Plateau.TailleMax; ++i)
        {
            if(i >= jetons.Count())
            {
                plateau.Add(([], []));
                continue;
            }

            plateau.Add((jetons.ElementAt(i), indicateurs.ElementAt(i)));
        }
            

        Plateau = plateau;
    }
}

/// <summary>
/// Page affichant le plateau de jeu et permettant aux joueurs d'interagir avec celui-ci.
/// </summary>
public partial class PlateauPage : ContentPage
{
    private Code? code;
    private Plateau? plateau;
    private bool? estJoueur;

    /// <summary>
    /// Constructeur de la page du plateau de jeu.
    /// Initialise les param�tres de navigation et les composants de la page.
    /// </summary>
    public PlateauPage()
    {
        NavigationPage.SetHasNavigationBar(this, false);

        InitializeComponent();
    }

    /// <summary>
    /// M�thode appel�e lorsqu'un nouveau tour de jeu commence.
    /// Affiche le nouveau tour de jeu sur le plateau.
    /// </summary>
    /// <param name="sender">La source de l'�v�nement ; dans ce cas, la partie.</param>
    /// <param name="e">Les arguments de l'�v�nement PartieNouveauTourEventArgs.</param>
    public async void QuandNouveauTour(object? sender, PartieNouveauTourEventArgs e)
    {
        if (Application.Current == null || Application.Current.MainPage == null)
            return;

        if (((NavigationPage)Application.Current!.MainPage).CurrentPage != this)
            await Application.Current.MainPage.Navigation.PushAsync(this);

        IEnumerable<Page> pages = Application.Current.MainPage.Navigation.NavigationStack.Reverse().Skip(1);

        foreach (Page page in pages)
        {
            if (page is AccueilPage)
                break;

            Application.Current.MainPage.Navigation.RemovePage(page);
        }

        code = e.Code;
        plateau = e.Plateau;
        estJoueur = e.EstJoueur;

        BindingContext = new Tour(e);
    }

    /// <summary>
    /// Supprime le dernier jeton ajout� au code en cours.
    /// Affiche une alerte si le code est vide.
    /// </summary>
    /// <param name="sender">La source de l'�v�nement ; ici, le bouton de suppression de jeton.</param>
    /// <param name="e">Les arguments de l'�v�nement EventArgs.</param>
    private void SupprimerDernierJeton(Object sender, EventArgs e)
    {
        try
        {
            if(code != null && estJoueur.HasValue && estJoueur.Value)
                code.SupprimerDernierJeton();
        }
        catch(CodeVideException exception)
        {
            DisplayAlert("Attention", exception.Message, "OK");
        }
    }

    /// <summary>
    /// Valide le code actuel et l'ajoute au plateau de jeu.
    /// Affiche une alerte si le code est incomplet.
    /// </summary>
    /// <param name="sender">La source de l'�v�nement ; ici, le bouton de validation de code.</param>
    /// <param name="e">Les arguments de l'�v�nement EventArgs.</param>
    private void ValiderCode(Object sender, EventArgs e)
    {
        try
        {
            if (plateau != null && code != null)
                plateau.AjouterCode(code);
        }
        catch (CodeIncompletException exception)
        {
            DisplayAlert("Attention", exception.Message, "OK");
        }
    }


    /// <summary>
    /// Ouvre la page des r�gles du jeu.
    /// </summary>
    /// <param name="sender">La source de l'�v�nement ; ici, le bouton de r�gles du jeu.</param>
    /// <param name="e">Les arguments de l'�v�nement EventArgs.</param>
    private async void QuandReglesClique(object sender, EventArgs e)
    {
        await Navigation.PushAsync(new ReglesPage());
    }
}