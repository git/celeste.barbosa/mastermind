using CoreLibrary.Regles;

namespace MauiSpark.Pages;

/// <summary>
/// Page affichant les r�gles disponibles.
/// </summary>
public partial class ReglesPage : ContentPage
{
    /// <summary>
    /// Constructeur de la page des r�gles.
    /// Initialise les param�tres de navigation et les composants de la page.
    /// Initialise le contexte de liaison avec une liste de r�gles disponibles.
    /// </summary>
    public ReglesPage()
	{
		NavigationPage.SetHasNavigationBar(this, false);

		BindingContext = typeof(IRegles).Assembly.GetTypes()
            .Where(type => typeof(IRegles).IsAssignableFrom(type) && type.IsClass)
            .Select(type => (Activator.CreateInstance(type) as IRegles)!)
            .OrderBy(regles => regles.Indice);

        InitializeComponent();
	}
}