namespace MauiSpark.Pages;

/// <summary>
/// Page d'accueil de l'application.
/// </summary>
public partial class AccueilPage : ContentPage
{

    /// <summary>
    /// Constructeur de la page d'accueil.
    /// Initialise les param�tres de navigation et les composants de la page.
    /// </summary>
    public AccueilPage()
	{
		NavigationPage.SetHasBackButton(this, false);
        NavigationPage.SetHasNavigationBar(this, false);

        InitializeComponent();
	}

    /// <summary>
    /// M�thode d�clench�e lorsque le bouton "Jouer" est cliqu�.
    /// Navigue vers la page Mode.
    /// </summary>
    /// <param name="sender">L'objet qui appelle l'�v�nement ; dans ce cas, le bouton "Jouer".</param>
    /// <param name="e">L'instance de l'�v�nement EventArgs cr��e par le syst�me.</param>
    private void QuandJouerClique(Object? sender, EventArgs e)
	{
		Navigation.PushAsync(new ModePage());
	}

    /// <summary>
    /// M�thode d�clench�e lorsque le bouton "Reprendre" est cliqu�.
    /// Navigue vers la page Reprendre.
    /// </summary>
    /// <param name="sender">L'objet qui appelle l'�v�nement ; dans ce cas, le bouton "Reprendre".</param>
    /// <param name="e">L'instance de l'�v�nement EventArgs cr��e par le syst�me.</param>
    private void QuandReprendreClique(Object? sender, EventArgs e)
    {
        Navigation.PushAsync(new ReprendrePage());
    }
}