using CoreLibrary.Joueurs;
using CoreLibrary.Statistiques;
using CoreLibrary.Regles;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace MauiSpark.Pages
{
    /// <summary>
    /// Repr�sente un joueur dans le classement.
    /// </summary>
    partial class Enfant
    {
        private readonly Classement classement;

        /// <summary>
        /// Obtient ou d�finit le joueur associ� � cet enfant.
        /// </summary>
        public Joueur Joueur { get; set; }

        /// <summary>
        /// Obtient la place du joueur dans le classement.
        /// </summary>
        public int Place => classement.Enfants.ToList().IndexOf(this) + 1;

        /// <summary>
        /// Obtient les statistiques du joueur dans le classement.
        /// </summary>
        public IEnumerable<int> Statistiques => Enum.GetValues<Statistique>()
            .Select(statistique => (int)Joueur.Statistique(classement.Regles, statistique));

        /// <summary>
        /// Obtient une liste des objets de cet enfant dans le classement (Son nom et sa place).
        /// </summary>
        public IEnumerable<string> Objets => new List<string>([$"{Place}", Joueur.Nom])
            .Concat(Statistiques.Select(statistique => $"{statistique}"));

        /// <summary>
        /// Initialise une nouvelle instance de la classe <see cref="Enfant"/>.
        /// </summary>
        /// <param name="joueur">Le joueur associ� � cet enfant.</param>
        /// <param name="classement">Le classement auquel cet enfant appartient.</param>
        public Enfant(Joueur joueur, Classement classement)
        {
            this.classement = classement;
            Joueur = joueur;
        }

        /// <summary>
        /// D�termine si l'objet sp�cifi� est �gal � l'objet actuel.
        /// </summary>
        public override bool Equals(object? obj)
        {
            if (obj == null || obj is not Enfant)
                return false;

            return Joueur == ((Enfant)obj).Joueur;
        }

        /// <summary>
        /// Retourne le code de hachage de cet objet.
        /// </summary>
        public override int GetHashCode() => Joueur.GetHashCode();
    }

    /// <summary>
    /// Repr�sente le classement des joueurs.
    /// </summary>
    partial class Classement : INotifyPropertyChanged
    {

        /// <summary>
        /// Se produit lorsque la valeur d'une propri�t� de l'objet change.
        /// </summary>
        public event PropertyChangedEventHandler? PropertyChanged;

        /// <summary>
        /// D�clenche l'�v�nement <see cref="PropertyChanged"/> pour notifier les modifications de propri�t�.
        /// </summary>
        /// <param name="propriete">Le nom de la propri�t� qui a chang�.</param>
        public void QuandProprieteChangee([CallerMemberName] string? propriete = null) => 
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propriete));

        private int indiceRegles = 0;

        /// <summary>
        /// Obtient les r�gles actuelles du classement.
        /// </summary>
        public IRegles Regles => ClassementPage.ToutesRegles.ElementAt(indiceRegles);

        private Statistique statistique = Enum.GetValues<Statistique>().First();

        /// <summary>
        /// Obtient ou d�finit la statistique utilis�e pour classer les joueurs.
        /// </summary>
        public Statistique Statistique
        {
            get => statistique;
            set
            {
                statistique = value;
                QuandProprieteChangee(nameof(Enfants));
            }
        }

        private bool inverser = true;

        /// <summary>
        /// Obtient ou d�finit une valeur indiquant si le classement est invers�.
        /// </summary>
        public bool Inverser { 
            get => inverser; 
            set 
            {
                inverser = value;
                QuandProprieteChangee(nameof(Enfants));
            } 
        }

        /// <summary>
        /// Obtient les titres des colonnes du classement.
        /// </summary>
        public IEnumerable<string> Titres => new List<string>(["Place", "Nom"]).Concat(
            Enum.GetValues<Statistique>().Select(
                statistique => string.Concat((Enum.GetName(typeof(Statistique), statistique) ?? "").Select(
                    (lettre, indice) => indice > 0 && char.IsUpper(lettre) ? $" {lettre}" : $"{lettre}")
                )
            )
        );

        /// <summary>
        /// Obtient la liste des enfants (joueurs) dans le classement, tri�e en fonction de la statistique et des r�gles actuelles.
        /// </summary>
        public IEnumerable<Enfant> Enfants => Inverser ?
            MauiProgram.Manageur.Joueurs
                .OrderBy(joueur => joueur.Statistique(Regles, Statistique))
                .Select(joueur => new Enfant(joueur, this)).Reverse() :
            MauiProgram.Manageur.Joueurs
                .OrderBy(joueur => joueur.Statistique(Regles, Statistique))
                .Select(joueur => new Enfant(joueur, this));

        /// <summary>
        /// Incr�mente l'indice des r�gles du classement selon une valeur sp�cifi�e.
        /// </summary>
        /// <param name="valeur">La valeur � ajouter � l'indice des r�gles.</param>
        public void IncrementerRegles(int valeur)
        {
            if ((indiceRegles += valeur) < 0) 
                indiceRegles = ClassementPage.ToutesRegles.Count() - 1;
            else if (indiceRegles >= ClassementPage.ToutesRegles.Count()) 
                indiceRegles = 0;

            QuandProprieteChangee(nameof(Regles));
            QuandProprieteChangee(nameof(Enfants));
        }
    }

    /// <summary>
    /// Partie de la logique de l'interface utilisateur de la page de classement.
    /// G�re les calculs de classement, l'affichage des statistiques et le changement des r�gles de jeu.
    /// </summary>
    public partial class ClassementPage : ContentPage
    {
        /// <summary>
        /// Obtient toutes les r�gles de jeu disponibles.
        /// </summary>
        public static IEnumerable<IRegles> ToutesRegles => typeof(IRegles).Assembly.GetTypes()
            .Where(type => typeof(IRegles).IsAssignableFrom(type) && type.IsClass)
            .Select(type => (Activator.CreateInstance(type) as IRegles)!)
            .OrderBy(regles => regles.Indice);

        /// <summary>
        /// Constructeur de la page de classement des joueurs.
        /// Initialise les param�tres de navigation et les composants de la page.
        /// </summary>
        public ClassementPage()
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);

            BindingContext = new Classement();
        }

        /// <summary>
        /// M�thode appel�e lorsqu'un bouton de changement de r�gles est press�.
        /// Augmente ou diminue l'indice des r�gles selon le bouton press�.
        /// </summary>
        public void ChangerReglesPresse(object sender, EventArgs e)
        {
            ((Classement)BindingContext).IncrementerRegles(sender == DiminuerRegles ? -1 : 1);
        }

        /// <summary>
        /// M�thode appel�e lorsqu'un bouton statistique est press�e.
        /// Modifie la statistique de classement ou inverse l'ordre du classement si la m�me statistique est d�j� s�lectionn�e.
        /// </summary>
        public void StatistiquePressee(object sender, EventArgs e)
        {
            if (sender is not Label || !Enum.IsDefined(typeof(Statistique), ((Label)sender).Text.Replace(" ", "")))
                return;

            Statistique statistique = Enum.Parse<Statistique>(((Label)sender).Text.Replace(" ", ""));
            Classement classement = (Classement)BindingContext;

            if (classement.Statistique == statistique)
            {
                classement.Inverser = !classement.Inverser;
                return;
            }

            classement.Inverser = true;
            classement.Statistique = statistique;
        }
    }
}