﻿using CoreLibrary.Manageurs;
using Microsoft.Extensions.Logging;
using Persistance.Persistance;

namespace MauiSpark
{
    public static class MauiProgram
    {
        public static Manageur Manageur { get; private set; } = new Manageur(new PersistanceJson(FileSystem.Current.AppDataDirectory));

        public static MauiApp CreateMauiApp()
        {
            var builder = MauiApp.CreateBuilder();
            builder
                .UseMauiApp<App>()
                .ConfigureFonts(fonts =>
                {
                    fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
                    fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
                });

#if DEBUG
    		builder.Logging.AddDebug();
#endif

            return builder.Build();
        }
    }
}
