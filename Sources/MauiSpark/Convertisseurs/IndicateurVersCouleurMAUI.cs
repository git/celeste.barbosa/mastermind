﻿using CoreLibrary.Core;
using System.Globalization;

namespace MauiSpark.Convertisseurs
{
    /// <summary>
    /// Convertisseur d'indicateur en couleur qui implémente l'interface IValueConverter.
    /// Cette classe permet de convertir des valeurs de type Indicateur en valeurs de type Color.
    /// </summary>
    public class IndicateurVersCouleurMAUI : IValueConverter
    {
        /// <summary>
        /// Obtient la couleur noire.
        /// </summary>
        public static Color Noir { get; private set; } = Color.FromArgb("#000000");

        /// <summary>
        /// Obtient la couleur blanche.
        /// </summary>
        public static Color Blanc { get; private set; } = Color.FromArgb("#FFFFFF");

        /// <summary>
        /// Convertit une valeur de type Indicateur en une valeur de type Color.
        /// </summary>
        /// <param name="value">La valeur à convertir.</param>
        /// <param name="targetType">Le type cible de la conversion.</param>
        /// <param name="parameter">Un paramètre facultatif utilisé pour la conversion.</param>
        /// <param name="culture">La culture à utiliser pour la conversion.</param>
        /// <returns>Une valeur de type Color correspondant à la valeur de type Indicateur.</returns>
        public object Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
        {
            if (value is not Indicateur) return Noir;

            switch (value)
            {
                case Indicateur.BonnePlace:
                    return Noir;
                case Indicateur.BonneCouleur:
                    return Blanc;
                default:
                    return Noir;
            }
        }

        /// <summary>
        /// Convertit une valeur de type Color en une valeur de type Indicateur.
        /// </summary>
        /// <param name="value">La valeur à convertir.</param>
        /// <param name="targetType">Le type cible de la conversion.</param>
        /// <param name="parameter">Un paramètre facultatif utilisé pour la conversion.</param>
        /// <param name="culture">La culture à utiliser pour la conversion.</param>
        /// <returns>Non implémenté. Lance une NotImplementedException.</returns>
        public object ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
