﻿using CoreLibrary.Core;
using System.Globalization;

namespace MauiSpark.Convertisseurs
{
    /// <summary>
    /// Convertisseur de couleur qui implémente l'interface IValueConverter.
    /// Cette classe permet de convertir des valeurs de type Couleur en valeurs de type Color (et vice versa).
    /// </summary>
    public class CouleurVersCouleurMAUI : IValueConverter
    {
        /// <summary>
        /// Obtient la couleur rouge.
        /// </summary>
        public static Color Rouge { get; private set; } = Color.FromArgb("#F75353");

        /// <summary>
        /// Obtient la couleur verte.
        /// </summary>
        public static Color Vert { get; private set; } = Color.FromArgb("#53F769");

        /// <summary>
        /// Obtient la couleur bleue.
        /// </summary>
        public static Color Bleu { get; private set; } = Color.FromArgb("#535AF3");

        /// <summary>
        /// Obtient la couleur jaune.
        /// </summary>
        public static Color Jaune { get; private set; } = Color.FromArgb("#E9FE67");

        /// <summary>
        /// Obtient la couleur noire.
        /// </summary>
        public static Color Noir { get; private set; } = Color.FromArgb("#241E1E");

        /// <summary>
        /// Obtient la couleur blanche.
        /// </summary>
        public static Color Blanc { get; private set; } = Color.FromArgb("#FFFFFF");

        /// <summary>
        /// Convertit une valeur de type Couleur en une valeur de type Color.
        /// </summary>
        /// <param name="value">La valeur à convertir.</param>
        /// <param name="targetType">Le type cible de la conversion.</param>
        /// <param name="parameter">Un paramètre facultatif utilisé pour la conversion.</param>
        /// <param name="culture">La culture à utiliser pour la conversion.</param>
        /// <returns>Une valeur de type Color correspondant à la valeur de type Couleur.</returns>
        public object Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
        {
            if (value is not Couleur) 
                return Noir;

            switch (value)
            {
                case Couleur.Blanc:
                    return Blanc;
                case Couleur.Bleu:
                    return Bleu;
                case Couleur.Vert:
                    return Vert;
                case Couleur.Rouge:
                    return Rouge;
                case Couleur.Noir:
                    return Noir;
                case Couleur.Jaune:
                    return Jaune;
                default:
                    return Noir;
            }
        }

        /// <summary>
        /// Convertit une valeur de type Color en une valeur de type Couleur.
        /// </summary>
        /// <param name="value">La valeur à convertir.</param>
        /// <param name="targetType">Le type cible de la conversion.</param>
        /// <param name="parameter">Un paramètre facultatif utilisé pour la conversion.</param>
        /// <param name="culture">La culture à utiliser pour la conversion.</param>
        /// <returns>Une valeur de type Couleur correspondant à la valeur de type Color.</returns>
        public object ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
        {
            if (value is not Color) return Couleur.Noir;

            if (value.Equals(Rouge))
                return Couleur.Rouge;
            if (value.Equals(Vert))
                return Couleur.Vert;
            if (value.Equals(Bleu))
                return Couleur.Bleu;
            if (value.Equals(Jaune))
                return Couleur.Jaune;
            if (value.Equals(Noir))
                return Couleur.Noir;
            if (value.Equals(Blanc))
                return Couleur.Blanc;

            return Couleur.Noir;
        }
    }
}
