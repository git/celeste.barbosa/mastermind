﻿using System.Globalization;

namespace MauiSpark.Convertisseurs
{
    /// <summary>
    /// Convertisseur de texte en texte en majuscules qui implémente l'interface IValueConverter.
    /// Cette classe permet de convertir une chaîne de caractères en majuscules.
    /// </summary>
    public class TexteVersTexteMajuscule : IValueConverter
    {

        /// <summary>
        /// Convertit une chaîne de caractères en majuscules.
        /// </summary>
        /// <param name="value">La valeur à convertir.</param>
        /// <param name="targetType">Le type cible de la conversion.</param>
        /// <param name="parameter">Un paramètre facultatif utilisé pour la conversion.</param>
        /// <param name="culture">La culture à utiliser pour la conversion.</param>
        /// <returns>Une chaîne de caractères en majuscules.</returns>
        public object Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
        {
            if (value is not string) 
                return "";

            return ((string)value).ToUpper();
        }

        /// <summary>
        /// Non implémenté. Lance une NotImplementedException.
        /// </summary>
        /// <param name="value">La valeur à convertir.</param>
        /// <param name="targetType">Le type cible de la conversion.</param>
        /// <param name="parameter">Un paramètre facultatif utilisé pour la conversion.</param>
        /// <param name="culture">La culture à utiliser pour la conversion.</param>
        /// <returns>Non implémenté. Lance une NotImplementedException.</returns>
        public object ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
