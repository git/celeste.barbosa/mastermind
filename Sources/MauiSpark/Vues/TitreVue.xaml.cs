namespace MauiSpark.Vues;

/// <summary>
/// Vue pour afficher un titre.
/// </summary>
public partial class TitreVue : ContentView
{
    /// <summary>
    /// Identifie la propri�t� de d�pendance pour le texte du titre.
    /// </summary>
    public static readonly BindableProperty TexteProperty = BindableProperty.Create(nameof(Texte), typeof(string), typeof(TitreVue), default(string));

    /// <summary>
    /// Obtient ou d�finit le texte du titre.
    /// </summary>
    public string Texte
    {
        get => (string)GetValue(TexteProperty);
        set => SetValue(TexteProperty, value);
    }

    /// <summary>
    /// Initialise une nouvelle instance de la classe <see cref="TitreVue"/>.
    /// </summary>
    public TitreVue()
    {
        InitializeComponent();
        BindingContext = this;
    }
}