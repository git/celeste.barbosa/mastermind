namespace MauiSpark.Vues;

/// <summary>
/// Vue repr�sentant une image de r�sultat.
/// </summary>
public partial class ImageResultatVue : ContentView
{
    /// <summary>
    /// Propri�t� attach�e � l'image affich�e dans la vue.
    /// </summary>
    public static readonly BindableProperty ImageProperty = BindableProperty.Create(nameof(Image), typeof(ImageSource), typeof(ImageResultatVue), null);

    /// <summary>
    /// Obtient ou d�finit l'image affich�e dans la vue.
    /// </summary>
    public ImageSource Image
    {
        get => (ImageSource)GetValue(ImageProperty);
        set => SetValue(ImageProperty, value);
    }

    /// <summary>
    /// Constructeur de la vue de l'image de r�sultat.
    /// Initialise les composants de la vue et lie le contexte de liaison � cette instance.
    /// </summary>
    public ImageResultatVue()
    {
        InitializeComponent();
        BindingContext = this;
    }
}