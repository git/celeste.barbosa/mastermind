using CoreLibrary.Core;
using CoreLibrary.Exceptions;
using MauiSpark.Convertisseurs;
using System.Globalization;

namespace MauiSpark.Vues;

/// <summary>
/// Vue repr�sentant un jeton color�.
/// </summary>
public partial class JetonVue : ContentView
{
    /// <summary>
    /// Propri�t� attach�e � la couleur du jeton.
    /// </summary>
    public static readonly BindableProperty CouleurProperty = BindableProperty.Create(nameof(Couleur), typeof(Color), typeof(JetonVue), default(Color));

    /// <summary>
    /// Propri�t� attach�e au code associ� au jeton.
    /// </summary>
    public static readonly BindableProperty CodeProperty = BindableProperty.Create(nameof(Code), typeof(Code), typeof(JetonVue), null);

    /// <summary>
    /// Propri�t� attach�e indiquant si le jeton est associ� � un joueur.
    /// </summary>
    public static readonly BindableProperty EstJoueurProperty = BindableProperty.Create(nameof(EstJoueur), typeof(bool), typeof(JetonVue), true);

    /// <summary>
    /// Obtient ou d�finit la couleur du jeton.
    /// </summary>
    public Color Couleur
    {
        get => (Color)GetValue(CouleurProperty);
        set => SetValue(CouleurProperty, value);
    }

    /// <summary>
    /// Obtient ou d�finit le code associ� au jeton.
    /// </summary>
    public Code? Code
    {
        get => (Code?)GetValue(CodeProperty);
        set => SetValue(CodeProperty, value);
    }

    /// <summary>
    /// Obtient ou d�finit une valeur indiquant si le jeton est associ� � un joueur.
    /// </summary>
    public bool EstJoueur
    {
        get => (bool)GetValue(EstJoueurProperty);
        set => SetValue(EstJoueurProperty, value);
    }

    /// <summary>
    /// Constructeur de la vue du jeton.
    /// Initialise les composants de la vue et lie le contexte de liaison � cette instance.
    /// </summary>
    public JetonVue()
    {
        InitializeComponent();
        BindingContext = this;
    }

    /// <summary>
    /// M�thode d�clench�e lorsque la taille du jeton est modifi�e.
    /// R�ajuste la taille du cercle repr�sentant le jeton pour qu'il soit toujours circulaire.
    /// </summary>
    /// <param name="sender">L'objet qui a d�clench� l'�v�nement ; ici, le cercle repr�sentant le jeton.</param>
    /// <param name="e">Les arguments de l'�v�nement, s'il y en a.</param>
    private void QuandTailleChangee(object sender, EventArgs e)
    {
        double taille = Math.Min(Grid.Width, Grid.Height) / 2;
        Cercle.WidthRequest = Cercle.HeightRequest = taille;
    }

    /// <summary>
    /// M�thode d�clench�e lorsque le jeton est press�.
    /// Ajoute un jeton � un code si celui-ci est associ� � un joueur.
    /// Affiche une alerte si le code est d�j� complet.
    /// </summary>
    /// <param name="sender">L'objet qui a d�clench� l'�v�nement ; ici, le cercle repr�sentant le jeton.</param>
    /// <param name="e">Les arguments de l'�v�nement, s'il y en a.</param>
    private void JetonPresse(object sender, EventArgs e)
    {
        if (Cercle == null || !sender.Equals(Cercle) || Code == null || Application.Current == null || Application.Current.MainPage == null || !EstJoueur)
            return;

        Couleur couleur = (Couleur)new CouleurVersCouleurMAUI().ConvertBack(((SolidColorBrush)Cercle.Fill).Color, typeof(Couleur), null, CultureInfo.InvariantCulture);

        try
        {
            Code.AjouterJeton(new Jeton(couleur));
        }
        catch (CodeCompletException exception)
        {
            Application.Current.MainPage.DisplayAlert("Attention", exception.Message, "OK");
        }
    }
}