using CoreLibrary;
using CoreLibrary.Regles;
using MauiSpark.Pages;

namespace MauiSpark.Vues;

/// <summary>
/// Vue permettant de s�lectionner un mode de jeu.
/// </summary>
public partial class ModeVue : ContentView
{
    /// <summary>
    /// Propri�t� de d�pendance pour les r�gles du jeu.
    /// </summary>
    public static readonly BindableProperty ReglesProperty = BindableProperty.Create(nameof(Regles), typeof(IRegles), typeof(ModeVue), null);

    /// <summary>
    /// Les r�gles du jeu s�lectionn�es.
    /// </summary>
    public IRegles Regles
    {
        get => (IRegles)GetValue(ReglesProperty);
        set => SetValue(ReglesProperty, value);
    }

    /// <summary>
    /// Constructeur par d�faut de la vue du mode de jeu.
    /// </summary>
    public ModeVue()
	{
		InitializeComponent();
	}

    /// <summary>
    /// M�thode d�clench�e lorsque le bouton de s�lection des r�gles est press�.
    /// </summary>
    /// <param name="sender">L'objet qui a d�clench� l'�v�nement ; ici, le bouton de s�lection des r�gles.</param>
    /// <param name="e">Les arguments de l'�v�nement EventArgs.</param>
    private void QuandReglesPresse(Object sender, EventArgs e)
    {
        Partie partie = MauiProgram.Manageur.NouvellePartie(Regles);

        partie.PartieDemanderJoueur += new ConnexionPage().QuandDemanderNom;
        partie.PartiePartieTerminee += new VictoirePage().QuandPartieTerminee;
        partie.PartieNouveauTour += new PlateauPage().QuandNouveauTour;

        partie.Jouer();
    }
}