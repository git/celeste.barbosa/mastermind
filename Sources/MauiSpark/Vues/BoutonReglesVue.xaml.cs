using MauiSpark.Pages;

namespace MauiSpark.Vues;

/// <summary>
/// Vue repr�sentant un bouton pour acc�der � la page des r�gles.
/// </summary>
public partial class BoutonReglesVue : ContentView
{
    /// <summary>
    /// Constructeur de la vue du bouton des r�gles.
    /// Initialise les composants de la vue.
    /// </summary>
    public BoutonReglesVue()
    {
		InitializeComponent();
    }

    /// <summary>
    /// M�thode d�clench�e lorsque le bouton des r�gles est cliqu�.
    /// Navigue vers la page des r�gles.
    /// </summary>
    /// <param name="sender">L'objet qui appelle l'�v�nement ; ici le bouton des r�gles.</param>
    /// <param name="e">L'instance de l'�v�nement EventArgs cr��e par le syst�me.</param>
    private void QuandReglesClique(Object? sender, EventArgs e)
    {
        Navigation.PushAsync(new ReglesPage());
    }
}