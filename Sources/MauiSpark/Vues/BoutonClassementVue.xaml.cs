using MauiSpark.Pages;

namespace MauiSpark.Vues;

/// <summary>
/// Vue repr�sentant un bouton pour acc�der � la page de classement.
/// </summary>
public partial class BoutonClassementVue : ContentView
{
    /// <summary>
    /// Constructeur de la vue du bouton de classement.
    /// Initialise les composants de la vue.
    /// </summary>
    public BoutonClassementVue()
	{
		InitializeComponent();
	}

    /// <summary>
    /// M�thode d�clench�e lorsque le bouton de classement est cliqu�.
    /// Navigue vers la page de classement.
    /// </summary>
    /// <param name="sender">L'objet qui appelle l'�v�nement ; ici le bouton de classement.</param>
    /// <param name="e">L'instance de l'�v�nement EventArgs cr��e par le syst�me.</param>
    private void QuandClassementClique(Object? sender, EventArgs e)
    {
        Navigation.PushAsync(new ClassementPage());
    }
}