using CoreLibrary.Core;
using MauiSpark.Convertisseurs;
using System.Globalization;

namespace MauiSpark.Vues;

/// <summary>
/// Vue repr�sentant un indicateur color�.
/// </summary>
public partial class IndicateurVue : ContentView
{
    /// <summary>
    /// Propri�t� attach�e � la couleur de l'indicateur.
    /// </summary>
    public static readonly BindableProperty CouleurProperty = BindableProperty.Create(nameof(Couleur), typeof(Color), typeof(IndicateurVue), default(Color));

    /// <summary>
    /// Obtient ou d�finit la couleur de l'indicateur.
    /// </summary>
    public Color Couleur
	{
        get => (Color)GetValue(CouleurProperty);

        set => SetValue(CouleurProperty, value);
    }

    /// <summary>
    /// Constructeur de la vue de l'indicateur.
    /// Initialise les composants de la vue et lie le contexte de liaison � cette instance.
    /// </summary>
    public IndicateurVue()
	{
		InitializeComponent();
        BindingContext = this;
	}

    /// <summary>
    /// M�thode d�clench�e lorsque la taille de l'indicateur est modifi�e.
    /// R�ajuste la taille du carr� repr�sentant l'indicateur pour qu'il soit toujours carr�.
    /// </summary>
    /// <param name="sender">L'objet qui a d�clench� l'�v�nement ; ici, le carr� repr�sentant l'indicateur.</param>
    /// <param name="e">Les arguments de l'�v�nement.</param>
    private void QuandTailleChangee(object sender, EventArgs e)
    {
        double taille = Math.Min(Grid.Width, Grid.Height) / 2;
        Carre.WidthRequest = Carre.HeightRequest = taille;
    }
}