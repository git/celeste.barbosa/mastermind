using CoreLibrary;
using MauiSpark.Pages;
using System.ComponentModel;

namespace MauiSpark.Vues;

/// <summary>
/// Vue affichant les d�tails d'une partie en cours.
/// </summary>
public partial class PartieCommenceeVue : ContentView, INotifyPropertyChanged
{
    /// <summary>
    /// Propri�t� de d�pendance pour la partie en cours.
    /// </summary>
    public static readonly BindableProperty PartieProperty = BindableProperty.Create(nameof(Partie), typeof(Partie), typeof(PartieCommenceeVue), null, propertyChanged: QuandPartieChangee);

    /// <summary>
    /// La partie en cours.
    /// </summary>
    public Partie Partie
    {
        get => (Partie)GetValue(PartieProperty);
        set => SetValue(PartieProperty, value);
    }

    /// <summary>
    /// Le nom des r�gles de la partie.
    /// </summary>
    public string NomRegles
    {
        get => Partie != null ? Partie.Regles.Nom : "";
    }

    /// <summary>
    /// Le tour actuel de la partie.
    /// </summary>
    public string TourActuel
    {
        get => $"Tour : {(Partie != null ? Partie.Tour : 0)} / {(Partie != null ? Partie.Regles.NbTour : 0)}";
    }

    /// <summary>
    /// Les joueurs participant � la partie.
    /// </summary>
    public string[] Joueurs
    {
        get => Partie != null ? Partie.Joueurs.ToArray() : [];
    }

    /// <summary>
    /// Le nombre de joueurs participant � la partie.
    /// </summary>
    public string NombreJoueurs => $"Joueurs : {Joueurs.Length} / {(Partie != null ? Partie.Regles.NbJoueurs : 0)}";

    /// <summary>
    /// M�thode d�clench�e lorsqu'une propri�t� de la partie change.
    /// </summary>
    private static void QuandPartieChangee(BindableObject bindable, object ancienneValeur, object nouvelleValeur)
    {
        ((PartieCommenceeVue)bindable).OnPropertyChanged(nameof(NomRegles));
        ((PartieCommenceeVue)bindable).OnPropertyChanged(nameof(TourActuel));
        ((PartieCommenceeVue)bindable).OnPropertyChanged(nameof(Joueurs));
        ((PartieCommenceeVue)bindable).OnPropertyChanged(nameof(NombreJoueurs));
    }

    /// <summary>
    /// M�thode d�clench�e lorsqu'un utilisateur appuie sur la vue de la partie.
    /// </summary>
    private void PartiePressee(object? sender, EventArgs e)
    {
        if (Partie == null)
            return;

        Partie partie = MauiProgram.Manageur.ChargerPartie(Partie);

        partie.PartieDemanderJoueur += new ConnexionPage().QuandDemanderNom;
        partie.PartiePartieTerminee += new VictoirePage().QuandPartieTerminee;
        partie.PartieNouveauTour += new PlateauPage().QuandNouveauTour;

        partie.Jouer();
    }

    /// <summary>
    /// Constructeur par d�faut de la vue de la partie en cours.
    /// </summary>
    public PartieCommenceeVue()
	{
		InitializeComponent();
	}
}