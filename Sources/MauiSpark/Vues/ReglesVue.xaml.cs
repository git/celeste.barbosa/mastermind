namespace MauiSpark.Vues;

/// <summary>
/// Vue pour afficher le titre et la description des r�gles.
/// </summary>
public partial class ReglesVue : ContentView
{
    /// <summary>
    /// Identifie la propri�t� de d�pendance pour le Nom des r�gles.
    /// </summary>
    public static readonly BindableProperty NomProperty = BindableProperty.Create(nameof(Nom), typeof(string), typeof(ReglesVue), default(string));

    /// <summary>
    /// Identifie la propri�t� de d�pendance pour la description des r�gles.
    /// </summary>
    public static readonly BindableProperty DescriptionProperty = BindableProperty.Create(nameof(Description), typeof(string), typeof(ReglesVue), default(string));

    /// <summary>
    /// Obtient ou d�finit le titre des r�gles.
    /// </summary>
    public string Nom
    {
        get => (string)GetValue(NomProperty);
        set => SetValue(NomProperty, value);
    }

    /// <summary>
    /// Obtient ou d�finit la description des r�gles.
    /// </summary>
    public string Description
    {
        get => (string)GetValue(DescriptionProperty);
        set => SetValue(DescriptionProperty, value);
    }

    /// <summary>
    /// Initialise une nouvelle instance de la classe <see cref="ReglesVue"/>.
    /// </summary>
    public ReglesVue()
	{
		InitializeComponent();

		BindingContext = this;
	}
}