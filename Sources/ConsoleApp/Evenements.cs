﻿using CoreLibrary.Evenements;
using CoreLibrary.Joueurs;
using CoreLibrary.Core;
using System.Diagnostics.CodeAnalysis;

namespace ConsoleApp
{
    /// <summary>
    /// Fournit un ensemble de méthodes écoutant les événements liés au déroulement de la partie.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public static class Evenements
    {
        /// <summary>
        /// Ecoute l'événement en rapport avec la demande du nom d'un joueur.
        /// <param name="sender">La classe qui appelle l'événement; ici Partie.</param>
        /// <param name="e">L'instance de l'événement DemanderNomEventArgs créée par Partie.</param>
        /// <returns>Le nom du joueur.</returns>
        /// </summary>
        public static void DemanderNom(object? sender, PartieDemanderJoueurEventArgs e)
        {
            Console.WriteLine($"Joueur {e.Indice}");
            Console.Write(">>> ");

            string? nom = Console.ReadLine();

            Console.WriteLine();

            Joueur joueur = !string.IsNullOrEmpty(nom) ? Program.Manageur.DemanderJoueur(nom) : new Robot();

            e.JoueurDemande.SeConnecter(joueur);
        }


        /// <summary>
        /// Ecoute l'événement en rapport avec le début de la partie.
        /// <param name="sender">La classe qui appelle l'événement; ici Partie.</param>
        /// <param name="e">L'instance de l'événement DebutPartieEventArgs créée par Partie.</param>
        /// </summary>
        public static void CommencerLaPartie(object? sender, PartieDebutPartieEventArgs e)
        {
            Utils.DessinerSeparateur();

            Console.WriteLine("La partie commence, bonne chance à tous !\n");
        }

        public static void NouveauTour(object? sender, PartieNouveauTourEventArgs e)
        {
            Utils.DessinerSeparateur();

            Console.WriteLine($"{e.Nom} - Tour {e.Tour}\n");

            Utils.DessinerPlateau(e.Plateau);

            foreach(Jeton jeton in e.Code.Jetons)
                Utils.DessinerPion(jeton.Couleur);

            while (!e.Code.Complet)
            {
                Jeton? jeton = Utils.SaisirJeton(e.Code.Taille);

                if (jeton.HasValue)
                {
                    e.Code.AjouterJeton(jeton.Value);
                    Utils.DessinerPion(jeton.Value.Couleur);
                }
                else
                {
                    e.Code.SupprimerDernierJeton();
                    Utils.SupprimerDernierJeton();
                }
            }

            Console.WriteLine("\n");

            if (!e.EstJoueur)
            {
                Task t = Task.Delay(3000);
                t.GetAwaiter().GetResult();
                
            }

            e.Plateau.AjouterCode(e.Code);
        }

        /// <summary>
        /// Ecoute l'événement en rapport avec l'ajout d'un code dans le plateau.
        /// <param name="sender">La classe qui appelle l'événement; ici Partie.</param>
        /// <param name="e">L'instance de l'événement AjouterCodeEventArgs créée par Partie.</param>
        /// </summary>
        public static void AjouterCode(object? sender, PartiePasserLaMainEventArgs e)
        {
            Utils.DessinerSeparateur();
        }

        /// <summary>
        /// Ecoute l'événement en rapport avec la fin de la partie.
        /// <param name="sender">La classe qui appelle l'événement; ici Partie.</param>
        /// <param name="e">L'instance de l'événement PartieTermineeEventArgs créée par Partie.</param>
        /// </summary>
        public static void PartieTerminee(Object? sender, PartiePartieTermineeEventArgs e)
        {
            string[] gagnants = e.Gagnants.ToArray();

            if (gagnants.Length > 1)
            {
                Console.WriteLine("C'est une égalité !");
            }
            else if (gagnants.Length == 1)
            {
                Console.WriteLine($"C'est une victoire de {gagnants[0]}.");
            }
            else
            {
                Console.WriteLine("C'est une défaite des deux joueurs...");
            }
        }
    }
}
