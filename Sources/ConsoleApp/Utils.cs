﻿using CoreLibrary.Core;
using System.Diagnostics.CodeAnalysis;

namespace ConsoleApp
{
    /// <summary>
    /// Classe utilitaire contenant des méthodes pour dessiner des éléments dans la console.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public static class Utils
    {
        // Dictionnaires associant les valeurs des énumérations avec les couleurs de la console
        private readonly static Dictionary<Couleur, ConsoleColor> couleursTerminal = new Dictionary<Couleur, ConsoleColor>()
        {
            {Couleur.Noir, ConsoleColor.Black },
            {Couleur.Blanc, ConsoleColor.White },
            {Couleur.Rouge, ConsoleColor.Red },
            {Couleur.Vert, ConsoleColor.Green },
            {Couleur.Bleu, ConsoleColor.Blue },
            {Couleur.Jaune, ConsoleColor.DarkYellow }
        };

        private readonly static Dictionary<Indicateur, ConsoleColor> indicateursTerminal = new Dictionary<Indicateur, ConsoleColor>()
        {
            {Indicateur.BonnePlace, ConsoleColor.Black },
            {Indicateur.BonneCouleur, ConsoleColor.White }
        };

        /// <summary>
        /// Dessine le titre du jeu dans la console.
        /// </summary>
        public static void DessinerTitre()
        {
            Console.WriteLine(@"
 __  __            _                     _           _
|  \/  | __ _  ___| |_  ___  _ _  _ __  (_) _ _   __| |
| |\/| |/ _` |(_-<|  _|/ -_)| '_|| '  \ | || ' \ / _` |
|_|  |_|\__,_|/__/ \__|\___||_|  |_|_|_||_||_||_|\__,_|
");
            DessinerSeparateur();
        }

        /// <summary>
        /// Dessine un séparateur dans la console.
        /// </summary>
        public static void DessinerSeparateur()
        {
            Console.WriteLine(@"

───────────────────────────────────────────────────────


            ");
        }

        /// <summary>
        /// Dessine un pion dans la console.
        /// </summary>
        /// <param name="pion">Le pion à dessiner (une couleur ou un indicateur).</param>
        public static void DessinerPion(Enum pion)
        {
            Console.Write(" ");

            // Sélectionne la couleur appropriée en fonction du type de pion (une couleur ou un indicateur)
            // Si Couleur.ROUGE alors le fond sera ConsoleColor.Red
            // Si Indicateur.BONNEPLACE alors le fond sera ConsoleColor.Black
            Console.ForegroundColor = pion.GetType().Equals(typeof(Couleur)) ?
                couleursTerminal.GetValueOrDefault((Couleur)pion) :
                indicateursTerminal.GetValueOrDefault((Indicateur)pion);

            // Si la couleur du pion est noir, alors le fond devient blanc, sinon il reste noir
            Console.BackgroundColor = Console.ForegroundColor.Equals(ConsoleColor.Black) ? ConsoleColor.White : ConsoleColor.Black;

            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.Write('O');

            Console.ResetColor();

            Console.Write(" ");
        }

        /// <summary>
        /// Dessine une ligne de pions sur la console.
        /// </summary>
        /// <param name="ligne">Un tableau d'énumérations représentant les pions à dessiner.</param>
        private static void DessinerLigne<T>(int taille, IEnumerable<T> ligne) where T : Enum
        {
            Console.Write(" ");

            foreach (Enum pion in ligne)
                DessinerPion(pion);

            // Ajoute des espaces s'il y a moins de taille pions à dessiner
            Console.Write("".PadLeft((taille - ligne.Count()) * 3 + 1));
        }

        /// <summary>
        /// Dessine un plateau de jeu dans la console, affichant les codes et leurs indicateurs.
        /// </summary>
        /// <param name="plateau">Le plateau de jeu.</param>
        public static void DessinerPlateau(Plateau plateau)
        {
            (IEnumerable<IEnumerable<Jeton>> codes, IEnumerable<IEnumerable<Indicateur>> indicateurs) = plateau.Grille;

            int tailleGrille = 2 + plateau.TailleMaxCode * 3;

            Console.WriteLine($"─{new string('─', tailleGrille)}─   ─{new string('─', tailleGrille)}─");
            Console.WriteLine($"│{new string(' ', tailleGrille)}│   │{new string(' ', tailleGrille)}│");

            for (int i = 0; i < plateau.TailleMax; ++i)
            {
                if(i >= codes.Count())
                {
                    Console.WriteLine($"│{new string(' ', tailleGrille)}│   │{new string(' ', tailleGrille)}│");
                    continue;
                }

                IEnumerable<Jeton> ligneCodes = codes.ElementAt(i);
                IEnumerable<Indicateur> ligneIndicateurs = indicateurs.ElementAt(i);

                Console.Write("│");

                DessinerLigne(plateau.TailleMaxCode, ligneCodes.Select(jeton => jeton.Couleur));

                Console.Write("│   │");

                DessinerLigne(plateau.TailleMaxCode, ligneIndicateurs);

                Console.WriteLine("│");
            }
            
            Console.WriteLine($"│{new string(' ', tailleGrille)}│   │{new string(' ', tailleGrille)}│");
            Console.WriteLine($"─{new string('─', tailleGrille)}─   ─{new string('─', tailleGrille)}─");
        }

        /// <summary>
        /// Permet à l'utilisateur de choisir un jeton
        /// <param name="indiceCode">L'indice du jeton à ajouter dans le code.</param>
        /// <returns>Le jeton choisi par le joueur.</returns>
        /// </summary>
        public static Jeton? SaisirJeton(int indiceCode)
        {
            Console.TreatControlCAsInput = true;
            Console.CursorVisible = false;

            bool aChoisi = false;
            int indice = 0;
            Couleur[] couleurs = (Couleur[])Enum.GetValues(typeof(Couleur));

            while (!aChoisi)
            {
                DessinerPion(couleurs[indice]);
                Console.Write("\b\b\b");

                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.Enter:
                        aChoisi = true;
                        break;

                    case ConsoleKey.LeftArrow:
                        --indice;
                        break;

                    case ConsoleKey.RightArrow:
                        ++indice;
                        break;

                    case ConsoleKey.Escape:
                        if (indiceCode != 0)
                        {
                            aChoisi = true;
                            indice = -2;
                        }
                        break;

                    default:
                        break;
                }

                if (indice == -1)
                    indice = couleurs.Length - 1;
                else if (indice == couleurs.Length)
                    indice = 0;
            }

            Console.TreatControlCAsInput = false;
            Console.CursorVisible = true;

            return indice != -2 ? new Jeton(couleurs[indice]) : null;
        }


        /// <summary>
        /// Permet de supprimer le dernier jeton saisi
        /// </summary>
        public static void SupprimerDernierJeton()
        {
            Console.Write("\b\b\b      \b\b\b\b\b\b");
        }
    }
}
