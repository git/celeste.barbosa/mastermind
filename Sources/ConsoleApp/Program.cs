using CoreLibrary.Manageurs;
using CoreLibrary.Regles;
using CoreLibrary;
using System.Diagnostics.CodeAnalysis;
using Persistance.Persistance;

namespace ConsoleApp
{
    /// <summary>
    /// Permet de jouer une partie de mastermind, avec les regles classiques
    /// (2 joueurs, 12 tours, un code a code couleurs parmi 6) 
    /// </summary>
    [ExcludeFromCodeCoverage]
    public static class Program
    {
        public static Manageur Manageur { get; private set; } = new Manageur(
            new PersistanceJson(
                Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Mastermind")
            )
        );

        public static void Main()
        {
            Utils.DessinerTitre();

            Partie maPartie = Manageur.NouvellePartie(new ReglesClassiques());

            maPartie.PartieDemanderJoueur += Evenements.DemanderNom;
            maPartie.PartieDebutPartie += Evenements.CommencerLaPartie;
            maPartie.PartieNouveauTour += Evenements.NouveauTour;
            maPartie.PartiePasserLaMain += Evenements.AjouterCode;
            maPartie.PartiePartieTerminee += Evenements.PartieTerminee;

            maPartie.Jouer();
        }
    }
}

