[![Drone](https://codefirst.iut.uca.fr/api/badges/nicolas.barbosa/mastermind/status.svg)](https://codefirst.iut.uca.fr/nicolas.barbosa/mastermind)

[![Sonar](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=celestebarbosa-mastermind&metric=alert_status&token=849a6b4d77bc19046c81e8fc8f6be8ec0b8c5f3a)](https://codefirst.iut.uca.fr/sonar/dashboard?id=celestebarbosa-mastermind)

[![Bugs](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=celestebarbosa-mastermind&metric=bugs&token=849a6b4d77bc19046c81e8fc8f6be8ec0b8c5f3a)](https://codefirst.iut.uca.fr/sonar/dashboard?id=celestebarbosa-mastermind)
[![Fiabilité](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=celestebarbosa-mastermind&metric=reliability_rating&token=849a6b4d77bc19046c81e8fc8f6be8ec0b8c5f3a)](https://codefirst.iut.uca.fr/sonar/dashboard?id=celestebarbosa-mastermind)

[![Vulnérabilités](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=celestebarbosa-mastermind&metric=vulnerabilities&token=849a6b4d77bc19046c81e8fc8f6be8ec0b8c5f3a)](https://codefirst.iut.uca.fr/sonar/dashboard?id=celestebarbosa-mastermind)
[![Securité](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=celestebarbosa-mastermind&metric=security_rating&token=849a6b4d77bc19046c81e8fc8f6be8ec0b8c5f3a)](https://codefirst.iut.uca.fr/sonar/dashboard?id=celestebarbosa-mastermind)

[![Risques](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=celestebarbosa-mastermind&metric=security_hotspots&token=849a6b4d77bc19046c81e8fc8f6be8ec0b8c5f3a)](https://codefirst.iut.uca.fr/sonar/dashboard?id=celestebarbosa-mastermind)

[![Dette technique](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=celestebarbosa-mastermind&metric=sqale_index&token=849a6b4d77bc19046c81e8fc8f6be8ec0b8c5f3a)](https://codefirst.iut.uca.fr/sonar/dashboard?id=celestebarbosa-mastermind)
[![Code Smells](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=celestebarbosa-mastermind&metric=code_smells&token=849a6b4d77bc19046c81e8fc8f6be8ec0b8c5f3a)](https://codefirst.iut.uca.fr/sonar/dashboard?id=celestebarbosa-mastermind)
[![Maintenabilité](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=celestebarbosa-mastermind&metric=sqale_rating&token=849a6b4d77bc19046c81e8fc8f6be8ec0b8c5f3a)](https://codefirst.iut.uca.fr/sonar/dashboard?id=celestebarbosa-mastermind)

[![Tests](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=celestebarbosa-mastermind&metric=coverage&token=849a6b4d77bc19046c81e8fc8f6be8ec0b8c5f3a)](https://codefirst.iut.uca.fr/sonar/dashboard?id=celestebarbosa-mastermind)
[![Lignes dupliquées (%)](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=celestebarbosa-mastermind&metric=duplicated_lines_density&token=849a6b4d77bc19046c81e8fc8f6be8ec0b8c5f3a)](https://codefirst.iut.uca.fr/sonar/dashboard?id=celestebarbosa-mastermind)
[![Nombre de lignes](https://codefirst.iut.uca.fr/sonar/api/project_badges/measure?project=celestebarbosa-mastermind&metric=ncloc&token=849a6b4d77bc19046c81e8fc8f6be8ec0b8c5f3a)](https://codefirst.iut.uca.fr/sonar/dashboard?id=celestebarbosa-mastermind)


# Mastermind - Projet IHM

**Céleste BARBOSA**\
**Pauline PRADY**\
**Camille TURPIN-ETIENNE**

---


## Réalisation d'un jeu vidéo en .NET, C# & MAUI

## Vidéo de présentation de notre application Mastermind

[Voir la vidéo de présentation](https://opencast.dsi.uca.fr/paella/ui/watch.html?id=b07dfc03-3f8e-4e97-8434-0282355a2f87)


#### Wiki
Un wiki du projet est disponible [ici](https://codefirst.iut.uca.fr/git/nicolas.barbosa/mastermind/wiki) et permet d'obtenir diverses informations sur les étapes de réalisation de ce jeu vidéo comme ses diagrammes de conception ou son étape de contextualisation.


#### Release
Vous trouverez en cliquant sur le lien ci-dessous, la dernière version de notre application, avec un installateur pour Windows.

Sinon, le code source est disponible sur la branche master, il contient également la dernière version du projet.

[Mastermind - Téléchargement](https://codefirst.iut.uca.fr/git/nicolas.barbosa/mastermind/releases/tag/release1.5)


#### Documentation
Pour accéder à la documentation doxygen de notre code, vous pouvez cliquer sur le lien ci-dessous.

[Documentation](https://codefirst.iut.uca.fr/documentation/nicolas.barbosa/doxygen/mastermind/html/)


#### Ce qui fonctionne
Dans notre application Mastermind, nous disposons de deux modes de jeu : Classique et Difficile. Le jeu est jouable à deux joueurs humains ou bien contre des robots.

La persistance est bien implémentée, vous pouvez conserver votre profil. Vous pouvez également reprendre une partie en cours.

Le classement permet de classer les joueurs selon les statistiques de jeu (parties gagnées, parties perdues, parties égalisées, coût moyen).
Nous classons les joueurs selon le mode de jeu choisi.


#### Ce qui ne fonctionne pas et ce qui ne fonctionne pas tout à fait
Selon nous, nous ne pensons pas qu'il y ait un quelconque problème dans notre code.

Nous n'avons pas trouvé de bug ou de fonctionnalité qui ne fonctionnait pas.

#### Mode d'emploi pour jouer

Pour jouer, il vous suffit de lancer le projet MauiSpark. Une fois lancé, vous arriverez sur la page d'accueil. Vous pouvez lancer une nouvelle partie ou reprendre une ancienne. Ensuite, il vous suffit de rentrer le nom des joueurs humains ou de choisir l'option Robot pour jouer contre un bot.

Ensuite, vous choisissez votre code en cliquant sur une couleur. Une fois que vous avez choisi toutes vos couleurs, vous cliquez sur valider le code. Vous avez aussi la possibilité de supprimer les jetons de votre code en cours.

Des indicateurs sont affichés au cours de la partie : un indicateur blanc pour une bonne couleur et un indicateur noir pour une bonne place.

La partie se termine sous trois conditions : un joueur trouve le bon code, la partie arrive au dernier tour et les deux joueurs ont perdu, ou les deux joueurs ont trouvé le code secret.


#### Sonar
Pour accéder à sonar, il vous suffit de cliquer sur le lien ci-dessous.

[Sonar de notre projet](https://codefirst.iut.uca.fr/sonar/dashboard?id=celestebarbosa-mastermind)


### Technologies utilisées

- **.NET** - Une plateforme de développement gratuite, multiplateforme et open-source pour créer de nombreux types d'applications.
- **C#** - Le langage de programmation principal utilisé.
- **MAUI** - .NET Multi-platform App UI pour créer des applications multiplateformes.
- **SonarQube** - Un outil pour l'inspection continue de la qualité du code.